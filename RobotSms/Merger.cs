﻿using System;
using System.Collections.Generic;
using System.Threading;
using Robot.Configuration;
using Robot.Logging;
using StockSharp.Logging;

namespace Robot.Sms {
    using WaitGroup = Dictionary<string, ManualResetEventSlim>;
    public class MergerMessage : SmsMessage {
        public override string ToText() {
            return Text;
        }
    }
    /// <summary>
    /// Класс реализующий объединение сообщений в группы
    /// </summary>
    sealed class Merger : ISmsable {
        public event Action<SmsMessage> Sms;
        /// <summary>
        /// Группа отправителей и события с ними связанных
        /// </summary>
        WaitGroup _waitGroup = new WaitGroup();
        /// <summary>
        /// Дескрипторы событий
        /// </summary>
        WaitHandle[] _handles;
        /// <summary>
        /// Событие ожидания новой смс
        /// </summary>
        ManualResetEventSlim guard_ = new ManualResetEventSlim(false);
        /// <summary>
        /// Тайм-аут ожидания всех смс группы
        /// </summary>
        TimeSpan _timeOut;
        MergeGroup _group;
        ISmsMerger _smsMerger;
        CancellationTokenSource _tokenSource = new CancellationTokenSource();
        /// <summary>
        /// Признак завершения работы
        /// </summary>
        volatile bool _workStop = false;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="group">Группа для объединения</param>
        public Merger(MergeGroup group) {
            _group = group;

            createSmsMerger(group.SmsMerger);

            _handles = new WaitHandle[_group.Count];

            for (int i = 0; i < _group.Count; ++i) {
                ManualResetEventSlim e = new ManualResetEventSlim(false);
                _handles[i] = e.WaitHandle;
                _waitGroup.Add(_group.GetItemAt(i).Name, e);
            }
            _timeOut = TimeSpan.FromSeconds(group.WaitSeconds);
        }  // Ctor

        void createSmsMerger(string name) {
            Type t = Type.GetType(name);
            if (t == null) {
                RobotLogger.Instance.AddErrorLog(
                    String.Format("Не найден тип {0} для склеивания sms. Используется по умолчанию",
                    name));
                _smsMerger = new TextSmsMerger();
                return;
            }

            _smsMerger = Activator.CreateInstance(t) as ISmsMerger;
        }
        
        public void Stop() {
            _workStop = true;

            // Выставляем все хэндлы в set, чтобы завершить поток
            foreach (var e in _waitGroup) {
                e.Value.Set();
            }
            _tokenSource.Cancel();
        }
        /// <summary>
        /// Обработчик приема нового сообщения
        /// </summary>
        /// <param name="sms">Сообщение</param>
        /// <returns></returns>
        public bool NewSms(SmsMessage sms) {
            lock (this) {
                // Убедимся, что это наше событие
                if (_waitGroup.ContainsKey(sms.Sender)) {
                    _waitGroup[sms.Sender].Set();
                    if (!guard_.IsSet)
                        guard_.Set();

                    _smsMerger.AddNewMessage(sms);

                    return (true);
                }
            }
            return (false);
        }  // NewSms
        /// <summary>
        /// Поток объединения сообщений
        /// </summary>
        public void Merge() {
            try {
                RobotLogger.Instance.AddDebugLog("SmsEnvetsMerger {0} started", _group.Name);
                for (; ; ) {
                    guard_.Wait(_tokenSource.Token);
                    
                    bool waitResult = WaitHandle.WaitAll(_handles, _timeOut);
                    if (_workStop) break;
                    lock (this) {
                        // сбрасываем события
                        foreach (var val in _waitGroup) {
                            if (!val.Value.IsSet) {
                                RobotLogger.Instance.AddDebugLog(
                                    "Группа {0}  не пришло событие {1}",
                                    _group.Name, val.Key);
                            }
                            val.Value.Reset();
                        }
                        guard_.Reset();
                    }

                    if (waitResult == false) {
                        if (_group.SendAction == SendAction.WaitSuccess) {
                            RobotLogger.Instance.AddDebugLog(
                                "Сообщение {0} ({1}) не отправляется(wait result({2}), action({3}))",
                                _smsMerger.TextToSend(), _group.Name, waitResult, _group.SendAction);
                            lock (this) {
                                _smsMerger.Clear();
                            }
                            continue;
                        }
                    } else if (waitResult == true) {
                        if (_group.SendAction == SendAction.WaitFailure) {
                            RobotLogger.Instance.AddDebugLog(
                                "Сообщение {0} ({1}) не отправляется(wait result({2}), action({3}))",
                                _smsMerger.TextToSend(), _group.Name, waitResult, _group.SendAction);
                            lock (this) {
                                _smsMerger.Clear();
                            }
                            continue;
                        }
                    }
                    
                    lock (this) {
                        // Оправляем что получилось
                        //Console.WriteLine("Merged: {0}", _mergedSms);
                        Sms.Invoke(new MergerMessage() {
                            Text = _smsMerger.TextToSend()
                        });
                        _smsMerger.Clear();
                        
                    }  // unlock
                }
            } catch (OperationCanceledException e) {
                RobotLogger.Instance.AddDebugLog("SmsEventsMerger {0} stoped: {1}",
                    _group.Name, e.Message);
            }
        }  // Merge
    }
}