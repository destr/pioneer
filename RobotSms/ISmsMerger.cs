﻿
namespace Robot.Sms {
    /// <summary>
    /// Интерфейс объединения сообщений
    /// </summary>
    interface ISmsMerger {
        /// <summary>
        /// Добавление нового сообщения
        /// </summary>
        /// <param name="sms">Сообщение</param>
        void AddNewMessage(SmsMessage sms);

        /// <summary>
        /// Получить текст для отправки
        /// </summary>
        /// <returns></returns>
        string TextToSend();
        /// <summary>
        /// Очистить предыдущие сообщения
        /// </summary>
        void Clear();
    }
}
