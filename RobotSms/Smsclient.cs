﻿using System;
using System.Threading;
using Robot.Configuration;
using Robot.Logging;
using StockSharp.Logging;

namespace Robot.Sms {
    using ThirdParty;    
    public class SmsClient {
        public event Action<string> SendSms;
        public SmsClient() {
            smsc_ = new SMSC();

            loadConfig();
            SendSms += new Action<string>(SmsClient_SendSms);
            thread_ = new Thread(this.work);
        }

        void SmsClient_SendSmsMessage(SmsMessage obj) {
            SmsClient_SendSms(obj.ToText());
        }
        public void send(string text) {
            SendSms(text);
        }  // send
        public void send(SmsMessage message) {
            SendSms(message.ToText());
        }
        public void send(string format, params object[] args) {
            SendSms(String.Format(format, args));
        }  // send
        /// <summary>
        /// Блокирующая отправка сообщения
        /// </summary>
        /// <param name="text"></param>
        public void syncSend(string text) {
            SmsClient_SendSms(text);
        }  // syncSend
        public void start(){
            if (thread_.ThreadState == ThreadState.Running) return;
            thread_.Start();
        }  // start
        public void stop() {
            stop_event_.Set();
            thread_.Join();
        }  // stop
        void work () {
            stop_event_.WaitOne();
        }  // work

        void SmsClient_SendSms(string text) {
            try {
                string smslog = "SMS";
                //bool maySend = WorkingCalendar.isTradingTime(DateTime.Now.TimeOfDay);

                if (!dummy_) {
                    
                    string [] res  = smsc_.send_sms(phones_, text, query: smsValid());
                    if (Convert.ToInt32(res[1]) <= 0) {
                        throw new Exception(
                            String.Format("Ошибка № {0}",
                            res[1].Substring(1, 1) + (res[0] != "0" ? ", ID: " + res[0] : "")));
                    }
                } else {
                    smslog += " dummy";
                }
                RobotLogger.Instance.AddDebugLog("{0}: {1}", smslog, text);
            } catch (Exception e) {
                RobotLogger.Instance.AddErrorLog(
                    "Не удалось отправить sms \"{0}\": {1}", text, e.Message);
            }
        }
        void loadConfig() {
            Smsc cfg = UserConf.Instance.SmscConfiguration();
            smsc_.SMSC_LOGIN = cfg.login;
            smsc_.SMSC_PASSWORD = cfg.password;
            smsc_.SMSC_HTTPS = true;
            _smsAlive = cfg.smsalive;
            Dummy = cfg.dummy;

            for (int i = 0; i <cfg.Count; ++i) {
                if (i != 0) {
                    phones_ += ",";
                }
                phones_ += cfg.GetItemAt(i).phone;
            }
            
        }  // loadConfig
        /// <summary>
        /// Признак режима вывода текста смс в лог вместо отправки
        /// </summary>
        public bool Dummy {
            get {
                return dummy_;
            }
            set {
                dummy_ = value;
                if (dummy_ == false) {
                    RobotLogger.Instance.AddInfoLog(
                        "SMS client mode dummy OFF");
                } else {
                    RobotLogger.Instance.AddInfoLog(
                        "SMS client mode dummy ON");
                }
            }
        }
        string smsValid() {
            return "valid=" + _smsAlive.ToString("hh\\:mm");
        }

        private AutoResetEvent stop_event_ = new AutoResetEvent(false);
        private Thread thread_;
        private SMSC   smsc_;
        private string phones_;
        private bool dummy_;
        /// <summary>
        /// Время жизни смс
        /// </summary>
        private TimeSpan _smsAlive;
        

    } // SmsClient
}  // namespace RobotCommon