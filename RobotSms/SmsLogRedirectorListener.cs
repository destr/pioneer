﻿using System;
using StockSharp.Logging;

namespace Robot.Sms {
    public class LogSmsMessage : SmsMessage {
        public LogLevels Level {
            get {
                return _level;
            }
            set {
                _level = value;
                switch (_level) {
                    case LogLevels.Warning:
                        base.Sender = "W";
                        break;
                    case LogLevels.Error:
                        base.Sender = "E";
                        break;
                }
                
            }

        }
        public override string ToText() {
            return base.Sender;
        }

        private LogLevels _level;
    }
    public class SmsLogRedirectorListener : LogListener, ISmsable {
        public event Action<SmsMessage> Sms;
        protected override void OnWriteMessage(LogMessage message) {

            if (message.Level == LogLevels.Error ||
                message.Level == LogLevels.Warning) {
                    Sms.Invoke(new LogSmsMessage() {
                        Level = message.Level
                    });
            }
        }
    }
}
