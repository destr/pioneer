﻿
namespace Robot.Sms {
    class TextSmsMerger : ISmsMerger {
        public void AddNewMessage(SmsMessage sms) {
            
            // склиеваем sms
            if (_text.Length != 0)
                _text += '\n';
            _text += sms.ToText();
        }
        public string TextToSend() {
                return _text;
        }

        public void Clear() {
            _text = "";
        }

        string _text = string.Empty;
    }
}
