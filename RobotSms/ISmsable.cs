﻿using System;
namespace Robot.Sms {
    /// <summary>
    /// Интерфейс осуществляющий отправку смс
    /// </summary>
    public interface ISmsable {
        /// <summary>
        /// Событие отправки смс
        /// </summary>
        event Action<SmsMessage> Sms; 
    }
}  // namespace RobotCommon.Sms