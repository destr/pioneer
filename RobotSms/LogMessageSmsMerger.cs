﻿using System;
using System.Collections.Generic;
using StockSharp.Logging;

namespace Robot.Sms {
    /// <summary>
    /// Объединение в смс сообщений лога
    /// </summary>
    class LogMessageSmsMerger : ISmsMerger {
        public void AddNewMessage(SmsMessage sms) {
            LogSmsMessage msg = sms as LogSmsMessage;
            if (!_ew.ContainsKey(msg.Level)) {
                _ew.Add(msg.Level, 0);
            }
            _ew[msg.Level]++;
        }
        public string TextToSend() {
            string ret = "";
            bool first = true;
            foreach (var e in _ew) {
                if (first) { first = false; } else { ret += ", "; }
                ret += String.Format("{0}: {1}", e.Key, e.Value);
            }
            return ret;
        }

        public void Clear() {
            _ew.Clear();
        }

        /// <summary>
        /// Счётчик ошибок и варнингов
        /// </summary>
        Dictionary<LogLevels, int> _ew = new Dictionary<LogLevels, int>();
    }
}
