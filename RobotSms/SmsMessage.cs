﻿using System;
namespace Robot.Sms {
    /// <summary>
    /// Базовый класс смс-сообщения
    /// </summary>
    public class SmsMessage {
        /// <summary>
        /// Отправитель сообщения
        /// </summary>
        public string Sender;
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text;
        /// <summary>
        /// Конвертация производного сообщения в текст
        /// </summary>
        /// <returns></returns>
        public virtual string ToText() {
            return String.Format("{0}: {1}", Sender, Text);
        }
    }
}
