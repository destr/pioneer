using System.Collections.Generic;
using System.Threading;
using Robot.Configuration;
using Robot.Logging;
using StockSharp.Logging;

namespace Robot.Sms {
    using MergeThreadMap = Dictionary<Merger, Thread>;

    public class SmsEventsMerger {

        MergeThreadMap _mergeThreadMap = new MergeThreadMap();
        SmsMerger _cfg;
        SmsClient _smsClient;
        public bool Dummy {
            get {
                return _smsClient.Dummy;
            }
            set {
                _smsClient.Dummy = value;
            }
        }
        
        public SmsEventsMerger() {
            _cfg = UserConf.Instance.MergeGroupsConfiguration();
            _smsClient = new SmsClient();
            _smsClient.start();
            // TODO: �������� ��������
            // �������� ����� ������ ������

            for (int i = 0; i < _cfg.MergeGroups.Count; ++i ) {
                Merger merger = new Merger(_cfg.MergeGroups.GetItemAt(i));
                merger.Sms += _smsClient.send;
                Thread thread = new Thread(merger.Merge);
                thread.Start();
                _mergeThreadMap.Add(merger, thread);
            }

        }
        public void stop() {
            _smsClient.stop();
            foreach (var val in _mergeThreadMap) {
                val.Key.Stop();
                val.Value.Join();
            }

        }  // stop
        /// <summary>
        /// ���������� ��������� � ���������
        /// </summary>
        /// <param name="info"></param>
        public void SmsMerge(SmsMessage message) {
            int smsFromGroup = 0;
            foreach (var val in _mergeThreadMap) {
                if (val.Key.NewSms(message)) {
                    ++smsFromGroup;
                }
            }

            if (smsFromGroup == 0) {
                RobotLogger.Instance.AddDebugLog(
                    "��� �� ����������� �� ����� ������ : {0}", message.ToText());
                _smsClient.send(message);
            }

        }  // SmsMerge

    }
}  // namespace RobotCommon.Sms