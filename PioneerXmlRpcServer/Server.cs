﻿using System;
using System.Net;
using System.Threading;
using System.IO;
using System.Text;
using CookComputing.XmlRpc;
using Robot.Logging;
using StockSharp.Logging;

namespace PioneerRobot.XmlRpc {
    /// <summary>
    /// Реализация xmlrpc сервера
    /// </summary>
    public class Server {
        public Server(XmlRpcListenerService service, string net, Int32 port, string accessToken) {
            _service = service;
            _listener.Prefixes.Add(String.Format("http://{0}:{1}/", net, port));
            _listener.AuthenticationSchemes = AuthenticationSchemes.Basic;
            _accessToken = accessToken;
        }
        public void Start() {
            _thread = new Thread(run);
            RobotLogger.Instance.AddInfoLog("Запуск xmlrpc сервера ...");

            try {
                _listener.Start();
                _thread.Start();
            } catch (HttpListenerException e) {
                RobotLogger.Instance.AddErrorLog(e);
                //netsh http add urlacl url=http://myip1:8080/app user=domain\user
            }
            
        }  // Stop

        public void Stop() {
            RobotLogger.Instance.AddInfoLog("Останов xmlrpc-сервера ...");
            _stop = true;
            _listener.Stop();
        }  // Stop

        private void run() {
            while (!_stop) {
                try {
                    HttpListenerContext context = _listener.GetContext();
                    RobotLogger.Instance.AddDebugLog("Получен новый запрос от: ", context.Request.UserHostAddress);
                    HttpListenerBasicIdentity identity = (HttpListenerBasicIdentity)context.User.Identity;
                    if (identity != null) {
                        Console.WriteLine("User {0} Password {1}", identity.Name, identity.Password);
                    }
                    if (identity == null || (identity.Password != _accessToken)) {
                        accessDenied(context);
                        continue;
                    }

                    
                    _service.ProcessRequest(context);
                    
                } catch (Exception e) {
                    if (!_stop) {
                        RobotLogger.Instance.AddErrorLog(e.Message);
                    }
                }
            }
        }  // run
        void accessDenied(HttpListenerContext context) {
            //Создаем ответ
            string requestBody;
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;
            
            Stream inputStream = request.InputStream;
            Encoding encoding = request.ContentEncoding;
            StreamReader reader = new StreamReader(inputStream, encoding);
            requestBody = reader.ReadToEnd();

            response.StatusCode = (int)HttpStatusCode.Forbidden;

            //Возвращаем ответ
            using (Stream stream = response.OutputStream) { }
        }

        /// <summary>
        /// Поток выполнения сервреа
        /// </summary>
        Thread _thread;
        /// <summary>
        /// Реализация сервиса xmlrpc
        /// </summary>
        XmlRpcListenerService _service;
        /// <summary>
        /// http сервер
        /// </summary>
        HttpListener _listener = new HttpListener();
        /// <summary>
        /// Токен доступа
        /// </summary>
        string _accessToken = null;
        /// <summary>
        /// Признак останова сервиса
        /// </summary>
        volatile bool _stop = false;
    }
}
