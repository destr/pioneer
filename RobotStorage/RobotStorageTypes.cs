﻿using System;

namespace Robot.Storage {
    /// <summary>
    /// Значения H, L цены за день
    /// </summary>
    public sealed class HLOfDayValue {
        public HLOfDayValue() {
            this.High = 0;
            this.Low = decimal.MaxValue;
            this.Date = DateTime.Now;
        }
        /// <summary>
        /// Максимальная цена дня
        /// </summary>
        public decimal High { get; set; }
        /// <summary>
        /// Минимальная цена дня
        /// </summary>
        public decimal Low { get; set; }
        /// <summary>
        /// Дата дня для которого определены цены
        /// </summary>
        public DateTime Date { get; set; }
    }
}
