﻿using System;
using Robot.Common.Cli;
using global::Robot.Configuration;

namespace Robot.Storage.Finam {
    public class FinamHistoryDownloaderCli : IDoCliCommand {
        
        public FinamHistoryDownloaderCli() {
        }  // Ctor

        public bool DoCliCommand(CommandDescription command) {
            return Helper.InvokeCommand(this, command);
        }

        public void FinamUpdateCommand(string[] args) {
            Console.WriteLine("Обновление информации об инструментах");
            this._downloader.Update();
        }  // FinamUpdateCommand

        public void FinamLoadCommand(string[] args) {
            // (securityConde, dateBegin, dateEnd, timeframe)
            // (securityCode, days, timeframe)

            if (args.Length < 3) {
                throw new ArgumentException("Не правильное количество параметров команды");
            }
            try {
                string securityCode = args[0].Trim();
                if (args.Length == 4) {

                    // Разбор и конвертация аргументов
                    DateTime begin = DateTime.Parse(args[1]);
                    DateTime end = DateTime.Parse(args[2]);
                    TimeSpan timeframe = TimeSpan.FromMinutes(double.Parse(args[3]));
                    this._downloader.LoadCandles(securityCode, begin, end, timeframe);

                    return;
                }
                if (args.Length == 3) {
                    
                    int daysBefore = int.Parse(args[1]);
                    TimeSpan timeframe = TimeSpan.FromMinutes(double.Parse(args[2]));
                    this._downloader.LoadCandles(securityCode, daysBefore, timeframe);
                    return;
                }
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }  // FinamLoadCommand
        public void FinamLoadAsCommand(string[] args) {
            // (securityCode, saveAsSecurityCode, dateBegin, dateEnd, timeframe)
            // (securityCode, saveAsSecurityCode, days, timeframe)
            if (args.Length < 4) {
                throw new ArgumentException("Не правильное количество параметров команды");
            }
            string securityCode = args[0].Trim();
            string saveAsSecurityCode = args[1].Trim();
            try {
                if (args.Length == 5) {

                    DateTime begin = DateTime.Parse(args[2]);
                    DateTime end = DateTime.Parse(args[3]);
                    TimeSpan timeframe = TimeSpan.FromMinutes(double.Parse(args[4]));
                    this._downloader.LoadCandles(securityCode, begin, end, timeframe, saveAsSecurityCode);
                }
                if (args.Length == 4) {
                    int daysBefore = int.Parse(args[2]);
                    TimeSpan timeframe = TimeSpan.FromMinutes(double.Parse(args[3]));
                    this._downloader.LoadCandles(securityCode, daysBefore, timeframe, saveAsSecurityCode);
                }
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }  // FinamLoadAsCommand

        public void FinamSearchCommand(string[] args) {
            if (args.Length != 1) {
                throw new ArgumentException("Недопустимое количество параметров");
            }
            this._downloader.Search(args[0]);
        }  // FinamSearchCommand

        /// <summary>
        /// Показать зарегистрированные алиасы
        /// </summary>
        /// <param name="args"></param>
        public void FinamAliasCommand(string[] args) {
            var aliases = UserConf.Instance.StorageConfiguration().SecurityAliases;
            foreach (AliasValue alias in aliases) {
                Console.WriteLine("{0} -> {1}", alias.name, alias.value);
            }
        }

        private FinamHistoryDownloader _downloader = new FinamHistoryDownloader();
    }
}
