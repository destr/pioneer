﻿using System;
using StockSharp.Algo.History.Finam;

namespace Robot.Storage.Finam {
    /// <summary>
    /// Расширенная информация об иструмента Финам
    /// </summary>
    class FinamSecurityInfoEx : StockSharp.Algo.History.Finam.FinamSecurityInfo {
        public FinamSecurityInfoEx(FinamSecurityInfo info) {
            Alias           = null;
            Code            = info.Code;
            Decimals        = info.Decimals;
            FinamMarketId   = info.FinamMarketId;
            FinamSecurityId = info.FinamSecurityId;
            Name            = info.Name;
        }
        /// <summary>
        /// Алиас инструмента
        /// </summary>
        public string Alias { get; set; }
    }
}
