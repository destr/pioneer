﻿namespace Robot.Storage.Finam {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;
    using Configuration;
    using StockSharp.Algo.Candles;
    using StockSharp.Algo.History.Finam;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    using Robot.Logging;
    using global::Robot.Configuration;

    using FinamSecurityCache = System.Collections.Generic.Dictionary<string, StockSharp.Algo.History.Finam.FinamSecurityInfo>;
    static class Exctensions {
        static public string Serialize(this FinamSecurityInfo info) {
            return string.Format(
                "{0};{1};{2};{3}", info.Code, info.FinamMarketId, info.FinamSecurityId, info.Name);
        }
        static public void Deserialize(this FinamSecurityInfo info, string str) {
            string[] vals = str.Split(new char[] { ';' }, 4);
            info.Code = vals[0];
            info.FinamMarketId = long.Parse(vals[1]);
            info.FinamSecurityId = long.Parse(vals[2]);
            info.Name = vals[3];
        }
    }
    /// <summary>
    /// Загрузка данных с finam
    /// </summary>
    public class FinamHistoryDownloader {
        /// <summary>
        /// Конструктор
        /// </summary>
        public FinamHistoryDownloader() {
            this._cachePath = Path.Combine(GlobalConf.FinamDir, this._cacheFileName);
            this._finamSource = new FinamHistorySource(new FinamSecurityStorage()) {
                DumpFolder = GlobalConf.FinamDir
            };
        }  // Ctor
        /// <summary>
        /// Обновить кеш инструментов
        /// </summary>
        public void Update() {
            string last = this.lastCacheUpdate();
            if (last != null) {
                Console.WriteLine("Последнее обновление: {0}", last);
            }

            string tmpcachepath = this._cachePath + ".tmp";
            if (File.Exists(tmpcachepath)) {
                File.Delete(tmpcachepath);
            }

            FinamSecurityInfo[] fsi = FinamHistorySource.DownloadSecurityInfo();
            //// Code: SRU3 Name: SBRF-9.13, FinammId 14, FinamSId 152388
            using (StreamWriter file = new StreamWriter(tmpcachepath)) {
                // Дата создания кеша
                file.WriteLine(DateTime.Now);
                foreach (FinamSecurityInfo info in fsi) {
                    file.WriteLine(info.Serialize());
                }
            }
            try {
                if (File.Exists(this._cachePath))
                {
                    File.Replace(tmpcachepath, this._cachePath, null);
                }
                else 
                {
                    File.Move(tmpcachepath, this._cachePath); 
                }
                Console.WriteLine("Информация обновлена");

            } catch (Exception e) {
                Console.WriteLine("Ошибка обновления данных финам");
                Console.WriteLine(e);
            }
        }  // Update
        /// <summary>
        /// Поиск кода инструмента
        /// </summary>
        /// <param name="pattern"></param>
        public IEnumerable<FinamSecurityInfo> Search(string pattern) {
            if (this._cache.Count == 0) {
                // кеш не загружен, необходимо загрузить
                this.loadCache();
            }

            IEnumerable<AliasValue> aliases = searchAliases(pattern);

            // Ищем иструменты по шаблону заднному пользователем
            Regex re = new Regex(pattern, RegexOptions.IgnoreCase);
            IList<FinamSecurityInfo> securityList = this._cache.Where(o => re.IsMatch(o.Key))
                .Select(v => v.Value).ToList();

            // ищем все инструменты, которые попали в поиск по алиасам
            foreach (AliasValue alias in aliases) {
                Console.WriteLine("Found alias: {0}", alias.name);
                bool aliasFound = false;
                foreach (var cacheLine in this._cache) {
                    if (cacheLine.Key == alias.value) {
                        FinamSecurityInfoEx siex = new FinamSecurityInfoEx(cacheLine.Value) {
                            Alias = alias.name
                        };
                        Console.WriteLine("Found: {0}", alias.value);
                        securityList.Add(siex);
                        aliasFound = true;
                        break;
                    } 
                }
                if (!aliasFound) {
                    RobotLogger.Instance.AddWarningLog(
                        "Не найден инструмент {0} для алиаса {1}", alias.value, alias.name);
                }
            }

            if (securityList.Count() == 0) {
                throw new Exception(
                    string.Format("Не найдено ни одного инструмента с кодом `{0}'", pattern));
            }

            Console.WriteLine("Found: ");
            foreach (FinamSecurityInfo val in securityList) {
                Console.WriteLine("\t{0}", val.Code);
            }
            return securityList.AsEnumerable();
        }  // Search

        /// <summary>
        /// Загрузить свечи
        /// </summary>
        /// <param name="seccode">Код инструмента</param>
        /// <param name="begin">Начало диапазона</param>
        /// <param name="end">Конец диапазона</param>
        public void LoadCandles(
            string seccode, DateTime begin, DateTime end, TimeSpan timeframe, string saveAsSeccode = null) {
            IEnumerable<FinamSecurityInfo> fis = this.Search(seccode);

            this.internallLoadCandles(fis, begin, end, timeframe, saveAsSeccode);
        } // LoadCandles
        
        /// <summary>
        /// Загрузить свечи по инструменту
        /// </summary>
        /// <param name="seccode">Код инструмента</param>
        /// <param name="daysBefore">Количество дней за которое надо загрузить свечи</param>
        /// <param name="timeframe">Интервал</param>
        public void LoadCandles(
            string seccode, int daysBefore, TimeSpan timeframe, string saveAsSeccode = null) {
            IEnumerable<FinamSecurityInfo> fis = this.Search(seccode);
            DateTime end = DateTime.Now;
            DateTime begin = end.AddDays(-daysBefore);

            this.internallLoadCandles(fis, begin, end, timeframe, saveAsSeccode);
        }  // LoadCandles

        private IEnumerable<Candle> internallLoadCandles(
            IEnumerable<FinamSecurityInfo> fis, DateTime begin, DateTime end, TimeSpan timeframe, string saveAsSeccode) 
        {

            Console.WriteLine(
                "Загрузка свечей для инструментов: {0}",
                string.Join(", ", fis.Select(i => i.Code)));

            foreach (FinamSecurityInfo info in fis) 
            {
                Security security = new Security();
                security.Code = info.Code;
                security.ExtensionInfo = new Dictionary<object, object>();
                security.ExtensionInfo[FinamHistorySource.FinamMarketIdField] = info.FinamMarketId;
                security.ExtensionInfo[FinamHistorySource.FinamSecurityIdField] = info.FinamSecurityId;

                Console.WriteLine(
                    "Загрузка свечей интервал {3}  по инструменту {0} с {1} по {2} ...", 
                    security.Code,
                    begin.Date,
                    end.Date,
                    timeframe.TotalMinutes.ToString());

                IEnumerable<Candle> candles = this._finamSource.GetCandles(security, begin, end, timeframe);
                if (saveAsSeccode == null) {
                    // Если надо сохранять под другим кодом
                    saveAsSeccode = fromAlias(info);
                }
                if (saveAsSeccode != null) {
                    Console.WriteLine("Сохранение инструмена {0} под кодом {1}", security.Code, saveAsSeccode);
                    security.Code = saveAsSeccode;
                }

                global::Robot.Storage.Storage.Instance.SaveCandles(
                    security,
                    candles.Cast<TimeFrameCandle>(),
                    timeframe,
                    CandleStorageType.Min);

            }
            Console.WriteLine("Все инструменты загружены!");
            return null;
        }  // internalLoadCandles
        
        /// <summary>
        /// Загрузк кеша инструментов
        /// </summary>
        private void loadCache() {
            using (StreamReader file = new StreamReader(this._cachePath)) {
                if (!file.EndOfStream) {
                    // пропускаем дату
                    file.ReadLine();
                }
                while (!file.EndOfStream) {
                    string line = file.ReadLine();
                    FinamSecurityInfo info = new FinamSecurityInfo();
                    info.Deserialize(line);
                    try {
                        this._cache.Add(info.Code, info);
                    } catch (Exception e) {
                        Console.WriteLine(string.Format("{0}: {1}", e.Message, info.Code));
                    }
                }
            }
        }  // loadCache

        private IEnumerable<AliasValue> searchAliases(string name) {
            Aliases aliases = UserConf.Instance.StorageConfiguration().SecurityAliases;
            Console.WriteLine("Translate name {0}", name);

            IList<AliasValue> result = new List<AliasValue>();
            Regex re = new Regex(name, RegexOptions.IgnoreCase);
            foreach (AliasValue alias in aliases) {
                Console.WriteLine(alias.name);
                if (re.IsMatch(alias.name)) {
                    result.Add(alias);
                    Console.WriteLine("Translated alias from {0} to {1}", alias.name, alias.value);
                }
            }

            return result;
        }  // tranlateAlias

        private string fromAlias(FinamSecurityInfo info) {
            if (info is FinamSecurityInfoEx) {
                return ((FinamSecurityInfoEx)info).Alias;
            }
            return null;
        }
        /// <summary>
        /// Загрузить дату последнего обновления кеша
        /// </summary>
        /// <returns></returns>
        private string lastCacheUpdate() {
            if (File.Exists(this._cachePath)) {
                using (StreamReader file = new StreamReader(this._cachePath)) {
                    if (!file.EndOfStream) {
                        // Первая строка это дата обновления
                        string updateDate = file.ReadLine();
                        return updateDate;
                    }
                }
            }
            return null;
        }  // lastCacheUpdate
        /// <summary>
        /// Название кеш-файла
        /// </summary>
        private readonly string _cacheFileName = "finam.security.cache.txt";
        /// <summary>
        /// Путь к кешу инструментов
        /// </summary>
        private string _cachePath;
        /// <summary>
        ///  Кеш инструментов с финам
        /// </summary>
        private FinamSecurityCache _cache = new FinamSecurityCache();
        /// <summary>
        /// Источник данных финам
        /// </summary>
        private FinamHistorySource _finamSource;
       

    }
}
