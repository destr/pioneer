﻿namespace Robot.Storage {
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Configuration;
    using Ecng.ComponentModel;
    using Logging;
    using StockSharp.Algo.Candles;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    using TimeFrameCandleStorage = System.Collections.Generic.Dictionary<System.DateTime,
            StockSharp.Algo.Candles.TimeFrameCandle>;
    /// <summary>
    /// Тип свечей хранимых в хранилище
    /// </summary>
    public enum CandleStorageType {
        /// <summary>
        /// Минутные свечи
        /// </summary>
        Min,
        /// <summary>
        /// Секундные свечи
        /// </summary>
        Sec,
        /// <summary>
        /// Тиковые свечи
        /// </summary>
        Tick

    }
    public class Storage
    {

        public void SaveCandles(
            Security security,
            IEnumerable<TimeFrameCandle> candles,
            TimeSpan timeFrame,
            CandleStorageType type)
        {

            string path = this.candlesPath(security, timeFrame, type);
            TimeFrameCandleStorage storage = new TimeFrameCandleStorage();
            try {
                // загружаем текущее хранилище
                storage = this.internalLoadTimeFrameCandles(security, timeFrame, type);
            } catch (StorageNotExistsException) {
                // нет хранилища и ладно, мы его создаём
            }
            string storageName = this.storageName(security, timeFrame, type);
            foreach (TimeFrameCandle candle in candles) {
                RobotLogger.Instance.AddDebugLog(
                    "Save candle {0} with time {1}", candle.Security.Code, candle.OpenTime);

                DateTime key = candle.OpenTime;


                if (storage.ContainsKey(key)) {
                    if (!this.isCandlesEq(storage[key], candle)) {
                        RobotLogger.Instance.AddDebugLog(
                            "Свеча с датой {0} уже содержится в хранилище {1}, но имеет другие"
                            + " значения. Заменяем",
                            key,
                            storageName);

                        storage[key] = candle;
                    }
                    continue;
                }
                storage.Add(key, candle);
            }


            // Сохраняем в файл
            using (StreamWriter file = new StreamWriter(path)) {
                foreach (var v in storage) {
                    file.WriteLine(this.candleToString(v.Value));
                }
            }
        }

        private bool isCandlesEq(TimeFrameCandle inStorage, TimeFrameCandle toSave) {
            if (inStorage.OpenTime != toSave.OpenTime) return false; 

            if (inStorage.OpenPrice != toSave.OpenPrice) return false;

            if (inStorage.HighPrice != toSave.HighPrice) return false;

            if (inStorage.LowPrice != toSave.LowPrice) return false;

            if (inStorage.ClosePrice != toSave.ClosePrice) return false;
            if (inStorage.TotalVolume != toSave.TotalVolume) return false;


            return true;
        }

        private string candleToString(TimeFrameCandle candle) {
            // Date,Time,Open,High,Low,Close,Vol
            string value;
            value = candle.OpenTime.Date.ToString(this._candleDateFieldFormat);
            value += this._candleFieldsDelimeter;

            value += candle.OpenTime.TimeOfDay.ToString(this._candleTimeFieldFormat);
            value += this._candleFieldsDelimeter;

            value += candle.OpenPrice.ToString(this._candleDecimalFieldFormat, this._candleDecimalNfi);
            value += this._candleFieldsDelimeter;

            value += candle.HighPrice.ToString(this._candleDecimalFieldFormat, this._candleDecimalNfi);
            value += this._candleFieldsDelimeter;

            value += candle.LowPrice.ToString(this._candleDecimalFieldFormat, this._candleDecimalNfi);
            value += this._candleFieldsDelimeter;

            value += candle.ClosePrice.ToString(this._candleDecimalFieldFormat, this._candleDecimalNfi);
            value += this._candleFieldsDelimeter;

            value += candle.TotalVolume.ToString();
            return value;
        }

        private string storageName(Security security, TimeSpan timeFrame, CandleStorageType type) {
            return string.Format("{0}/{1}/{2}", security.Code, type, timeFrame.TotalMinutes);
        }

        /// <summary>
        /// Загрузка свечей по инструменту
        /// </summary>
        /// <typeparam name="CandleType">Тип свечи</typeparam>
        /// <param name="security">Инструмент для которого загружаются свечи</param>
        /// <param name="timeFrame">Интервал</param>
        /// <param name="dateFrom">Начало диапазона</param>
        /// <param name="dateTo">Конеч диапазона</param>
        /// <returns></returns>
        public IEnumerable<TimeFrameCandle> LoadTimeFrameCandles(
            Security security, 
            TimeSpan timeFrame, 
            DateTime dateFrom,
            DateTime dateTo) 
        {

            List<TimeFrameCandle> candles = new List<TimeFrameCandle>();
            TimeFrameCandleStorage storage =
                this.internalLoadTimeFrameCandles(security, timeFrame, CandleStorageType.Min);
            // выбираем нужные свечи
            for (DateTime dt = dateFrom; dt <= dateTo; dt = dt + timeFrame) {
                RobotLogger.Instance.AddDebugLog(
                    "Загрузка свечи {0} со временем {1}", security.Code, dt);
                if (!storage.ContainsKey(dt)) {
                    RobotLogger.Instance.AddDebugLog(
                        "Свечи {0} со временем {1} нет в хранилище", security.Code, dt);
                    continue;
                }
                candles.Add(storage[dt]);
            }
            return candles.AsEnumerable();
        }  // LoadTimeFrameCandles
        /// <summary>
        /// Загрузить свечу
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <param name="timeFrame">Интервал</param>
        /// <param name="date">Дата свечи</param>
        /// <returns>Свеча</returns>
        public TimeFrameCandle LoadTimeFrameCandle(
            Security security, TimeSpan timeFrame, DateTime date)
        {
            TimeFrameCandleStorage storage =
                this.internalLoadTimeFrameCandles(security, timeFrame, CandleStorageType.Min);

            if (storage.ContainsKey(date)) 
            {
                return storage[date];
            }
            return null;
        }

        /// <summary>
        /// Загрузка свечей по инструменту начиная с указанной даны в количестве count
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <param name="timeFrame">Интервал</param>
        /// <param name="dateFrom">Начальная дата</param>
        /// <param name="count">Количество</param>
        /// <param name="timeRange">Временной диапазон свечи</param>
        /// <returns></returns>
        public IEnumerable<TimeFrameCandle> LoadTimeFrameCandles(
            Security security, TimeSpan timeFrame, DateTime dateFrom, int count, Range<TimeSpan> timeRange)
        {
           
            if (timeRange == null) 
            {
                timeRange = new Range<TimeSpan>(TimeSpan.MinValue, TimeSpan.MaxValue);
            }
            TimeFrameCandleStorage storage =
                this.internalLoadTimeFrameCandles(security, timeFrame, CandleStorageType.Min);

            TimeFrameCandleStorage storageSlice = storage
                .Where(p => p.Key < dateFrom && timeRange.Contains(p.Key.TimeOfDay))
                .ToDictionary(p => p.Key,  p => p.Value);

            return storageSlice.Skip(storageSlice.Count - count).Take(count).Select(x => x.Value)
                .AsEnumerable(); 
        }  // LoadTimeFrameCandles

        /// <summary>
        /// Загрузить все свечи из хранилища
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <param name="timeFrame">Интервал</param>
        /// <param name="type">Тип хранилиза</param>
        /// <returns>Загруженное хранилище</returns>
        private TimeFrameCandleStorage internalLoadTimeFrameCandles(
            Security security, TimeSpan timeFrame, CandleStorageType type) 
        {
            
            string path = this.candlesPath(security, timeFrame, type);
            if (!File.Exists(path)) 
            {
                RobotLogger.Instance.AddInfoLog(
                    "Хранилище свеч {0} не существует", path);
                throw new StorageNotExistsException(path);
            }
            TimeFrameCandleStorage storage = new TimeFrameCandleStorage();
            using (StreamReader file = new StreamReader(path)) 
            {
                if (file.EndOfStream) 
                {
                    RobotLogger.Instance.AddInfoLog(
                        "Хранилище свеч для иструмента {0} больше не содержит свечей", security.Code);
                    return storage;
                }

                int line = 0;
                while (!file.EndOfStream) {
                    string str = file.ReadLine();
                    ++line;
                    string[] vals = str.Split(this._candleFieldsDelimeter);
                    if (vals.Length < this._candleFieldsCount) {
                        RobotLogger.Instance.AddDebugLog(
                            "В строке {1} хранилищаа {2} некорректная строка {0}",
                            str, 
                            line, 
                            this.storageName(security, timeFrame, type));
                        continue;
                    }
                    DateTime date = DateTime.ParseExact(vals[0], this._candleDateFieldFormat, null);
                    TimeSpan time = TimeSpan.ParseExact(vals[1], this._candleTimeFieldFormat, null);
                    date = date.Add(time);

                    TimeFrameCandle candle = new TimeFrameCandle();
                    // вроде всё ок, поехали
                    candle.OpenTime = date;
                    candle.OpenPrice = decimal.Parse(vals[2], this._candleDecimalNfi);
                    candle.HighPrice = decimal.Parse(vals[3], this._candleDecimalNfi);
                    candle.LowPrice = decimal.Parse(vals[4], this._candleDecimalNfi);
                    candle.ClosePrice = decimal.Parse(vals[5], this._candleDecimalNfi);
                    candle.TotalVolume = decimal.Parse(vals[6], this._candleDecimalNfi);
                    candle.Security = security;
                    candle.TimeFrame = timeFrame;

                    if (!storage.ContainsKey(date)) {
                        storage.Add(date, candle);
                    }
                    else {
                        RobotLogger.Instance.AddDebugLog(
                            "Дубликат времени {0} свечи  в строке {1}", date, line);
                    }
                    
                }
            }
            return storage;
        }

        /// <summary>
        /// Сформировать путь к файлу свечей
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <returns></returns>
        string candlesPath(Security security, TimeSpan timeFrame, CandleStorageType type) {
            string dirPath = Path.Combine(
                GlobalConf.StorageDir,
                security.Code,
                type.ToString(), 
                timeFrame.TotalMinutes.ToString());

            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            return Path.Combine(dirPath, "candles.txt");
        }  // candlesPath

        private readonly char _candleFieldsDelimeter = ';';
        private readonly string _candleDateFieldFormat = "yyyyMMdd";
        private readonly string _candleTimeFieldFormat = "hhmmss";
        private readonly string _candleDecimalFieldFormat = "F";
        private readonly NumberFormatInfo _candleDecimalNfi = new NumberFormatInfo() {
            NumberDecimalSeparator = ".",
            NumberDecimalDigits = 4
        };
        /// <summary>
        /// Количество полей в сериализованной свече
        /// </summary>
        private readonly int _candleFieldsCount = 7;

        /// <summary>
        /// Конструктор
        /// </summary>
        private Storage() {
            
        }
        private static volatile Storage _instance;
        private static object _syncObject = new object();

        public static Storage Instance {
            get {
                if (_instance == null) {
                    lock (_syncObject) {
                        if (_instance == null) {
                            _instance = new Storage();
                        }
                    }
                }
                return _instance;
            }
        }
    }
}
