﻿// <copyright file="Exceptions.cs" company="Multiple Market Systems"> 
// Multiple Market Systems
// </copyright>
namespace Robot.Storage {
    public class StorageNotExistsException : System.Exception {
        public StorageNotExistsException(string storage) :
            base(string.Format("Хранилище свеч {0} не существует", storage)) {
        }
    }
}
