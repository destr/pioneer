﻿namespace Robot.Common.Exchange.Rts {
    using System;
    using System.Collections.Generic;

    using Robot.Configuration;
    /// <summary>
    /// Рабочий календарь биржи
    /// </summary>
    public class WorkingCalendar {
        /// <summary>
        /// Время начала вечерней торговой сессии в дату истечения опционов
        /// </summary>
        static public TimeSpan ExpirationDaySessionTime {
            get {
                return UserConf.Instance.RtsCalendarConfiguration()
                    .ExpirationDates.ExpirationDaySesionTime;
            }
        }

        /// <summary>
        /// Проверка является ли дата торговым днём
        /// </summary>
        /// <param name="date">Дата</param>
        /// <returns>truе если день торговый</returns>
        public static bool isTradingDate(DateTime date) {
            if (date.DayOfWeek == DayOfWeek.Saturday
                || date.DayOfWeek == DayOfWeek.Sunday) {
                // выходные
                // проверяем, может быть этот день перенесли
                return isAdditionalWorkingDate(date.Date);
            }
            
            // рабочие дни проверяем не являются ли они праздниками
            return !isAdditionalHolidayDate(date.Date);
        }  // isTradingDate
        private static bool isAdditionalWorkingDate(DateTime dt) {
            WorkingDates wd = 
                UserConf.Instance.RtsCalendarConfiguration().AdditionalWorkingDates;
            return checkDate(dt, wd);
            
        }
        private static bool checkDate(DateTime dt, ICollection<Date> values) {
            foreach (Date d in values) {
                if (d.value.Date == dt) {
                    return (true);
                }
            }
            return (false);
        }
        private static bool isAdditionalHolidayDate(DateTime dt) {
            HolidaysDates hd =
                UserConf.Instance.RtsCalendarConfiguration().AdditionalHolidayDates;
            return checkDate(dt, hd);
        }
        /// <summary>
        /// Определение предыдущего торгового дня относительно DateTime.Now
        /// </summary>
        /// <returns></returns>
        public static DateTime previousTradingDate(DateTime now = new DateTime()) {
            if (now.Equals(DateTime.MinValue)) {
                now = DateTime.Now.Date;
            }                        
            DateTime yesterday = now - TimeSpan.FromDays(1);
            for (;;yesterday -= TimeSpan.FromDays(1)) {
                if (isTradingDate(yesterday)) {
                        return yesterday;
                }
            }
        }  // previousTradingDate
        /// <summary>
        /// Определение следующего торгового дня относительно DateTime.Now
        /// </summary>
        /// <returns></returns>
        public static DateTime nextTradingDate(DateTime now = new DateTime()) {
            if (now.Equals(DateTime.MinValue)) {
                now = DateTime.Now.Date;
            }
            DateTime tomorrow = now + TimeSpan.FromDays(1);
            for (;; tomorrow += TimeSpan.FromDays(1)) {
                if (isTradingDate(tomorrow)) {
                    return tomorrow;
                }
            }
        }  // nextTradingDate
        /// <summary>
        /// Проверка вхождения времени в диапазон времени торгов
        /// </summary>
        /// <param name="time"></param>
        /// <returns>true если время торговое</returns>
        public static bool isTradingTime(TimeSpan time) {

            foreach (var wt in StockSharp.BusinessEntities.ExchangeBoard.Forts.WorkingTime.Times) {
                if (time >= wt.Min && time < wt.Max) return true;
            }

            return false;
                                
        }  // isTradingTime
        /// <summary>
        /// Проверка даты итечения опцинов
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static bool isExpirationDate(DateTime date) {
            ExpirationDates ed =
                UserConf.Instance.RtsCalendarConfiguration().ExpirationDates;
            return checkDate(date.Date, ed);
        }
        public static bool isEveningSession(TimeSpan time) {
            StockSharp.BusinessEntities.WorkingTime wt =
                StockSharp.BusinessEntities.ExchangeBoard.Forts.WorkingTime;
            return time >= wt.Times[2].Min && time < wt.Times[2].Max;
        }
    }  // class WorkingCalendar
} // namespace RobotCommon.Exchange.Rts