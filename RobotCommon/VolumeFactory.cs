﻿using System;
using Robot.Common.Types;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace Robot.Common.Equity {
    public class VolumeFactory {
        /// <summary>
        /// Создание менеджера объёма
        /// </summary>
        /// <param name="portfolio">Портфель</param>
        /// <param name="security">Инструмент</param>
        /// <param name="logger">Объект для вывода лог сообщений</param>
        /// <param name="unit">Единицы измерения объёма</param>
        /// <param name="volumeValue">Значение объёма</param>
        /// <returns></returns>
        static public IVolumeManager create(Portfolio portfolio,
            Security security, ILogReceiver logger, VolumeUnit unit, decimal volumeValue) {
                if (volumeValue == 0) {
                    throw new Exception("Не указан объём входа в позицию");
                }
                switch (unit) {
                    case VolumeUnit.Volume:
                        LoggingHelper.AddInfoLog(logger,
                            "Используем постоянное значение объёма: {0}",
                            volumeValue);
                        return new DirectVolumeManager(volumeValue) {
                                security = security,
                                portfolio = portfolio,
                                logger = logger
                            };
                        
                    case VolumeUnit.RiskPercent:
                        LoggingHelper.AddInfoLog(logger,
                            "Объём определяется риск менеджером как процент от счёта {0}%",
                            volumeValue);
                        return new RiskVolumeManager(volumeValue) {
                            security = security,
                            portfolio = portfolio,
                            logger = logger
                        };
                    case VolumeUnit.EquityPercent:
                        LoggingHelper.AddInfoLog(logger,
                            "Объём определяется процентом от счёта {0}%",
                            volumeValue);
                        return new EquityVolumeManager(volumeValue) {
                            security = security,
                            portfolio = portfolio,
                            logger = logger
                        };
                    default:
                        throw new Exception("Не указан метод определения объёма для входа в позицию");
                }

        }
    }
}
