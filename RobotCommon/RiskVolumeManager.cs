﻿using System;
using StockSharp.Logging;

namespace Robot.Common.Equity {
    /// <summary>
    /// Вычисление объёма сделки на основе риска
    /// </summary>
    public class RiskVolumeManager: IVolumeManager {

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="stoppoint">Риск в процентах</param>
        public RiskVolumeManager(decimal percentequityrisk) {
            percent_equity_risk_ = percentequityrisk;
            
        }  // Ctor

        /// <summary>
        /// Рассчитать максимально возможный объём сделки
        /// </summary>
        /// <param name="open_price">Цена открытия свечи</param>
        /// <param name="stop_price">Стоп цена</param>
        /// <returns>Объём</returns>
        public override decimal maxVolume(decimal open_price, decimal stop_price) {
            decimal vol = maxVolumeByEquity(open_price, stop_price,
                portfolio.BeginValue);

            decimal max_portfolio_vol = maxVolumeByPortfolio(open_price, stop_price,
                portfolio.BeginValue);
            return selectMinVolume(max_portfolio_vol, vol);
        }  // maxVolume
        /// <summary>
        /// Расчёт максимального объёма контрактов после срабатывания стопов
        /// </summary>
        /// <param name="openprice"></param>
        /// <param name="stopprice"></param>
        /// <param name="prevvolume"></param>
        /// <param name="newstopprice"></param>
        /// <returns></returns>
        public override decimal maxVolumeAfterStop(decimal openprice,
            decimal stopprice, decimal prevvolume, decimal newstopprice) {
            
            // Рассчитываем старый риск с которым мы уже вошли
            decimal totaloldrisk = Math.Abs(openprice - stopprice);
            logger.AddDebugLog("Риск в пунктах по исполненной сделке" + 
                " totaloldrisk({0}) = Math.Abs(openprice({1}) - stopprice({2}))",
                totaloldrisk, openprice, stopprice);
            totaloldrisk *= (security.MinStepPrice / security.MinStepSize);
            logger.AddDebugLog("Потеря при выходе по стопу в деньгах на контракт:" +
                " totaloldrisk({0}) *= (security.MinStepPrice({1}) / security.MinStepSize({2}))",
                totaloldrisk, security.MinStepPrice, security.MinStepSize);
            // потеряем денег, когда отстопимся
            totaloldrisk *= prevvolume;
            logger.AddDebugLog("Потеря в деньгах по всему объёму: " +
                "totaloldrisk({0}) *= prevvolume({1})", 
                totaloldrisk, prevvolume);

            // Доступно средств для разворота
            decimal avail = portfolio.BeginValue - totaloldrisk;
            logger.AddDebugLog("Доступно средств после активации стопа: " +
                "avail({0}) = portfolio.BeginValue({1}) - totaloldrisk({2})", 
                avail, portfolio.BeginValue, totaloldrisk);

            decimal max_portfolio_vol = maxVolumeByPortfolio(openprice, stopprice, avail);
            decimal vol = maxVolumeByEquity(stopprice, newstopprice, avail);
            return selectMinVolume(max_portfolio_vol, vol);

        }  // maxVolumeAfterStop
        decimal selectMinVolume(decimal max_portfolio_vol, decimal vol) {
            if (max_portfolio_vol <= 0) {
                throw new Exception(
                    String.Format("Вход по риску, объём не положительный {0}", max_portfolio_vol));
            }
            if (vol <= 0) {
                throw new Exception(
                    String.Format("Вход по риску, объём не положительный {0}", vol));
            }
            if (vol > max_portfolio_vol) {
                logger.AddInfoLog(
                    "Объём({0}) рассчитаный по риску на контракт, больше({1}), чем можно купить" +
                    " на средства портфеля. Используется меньший: {1}!", vol, max_portfolio_vol);
                return max_portfolio_vol;
            }
            return vol;
        }  // selectMinVolume
        /// <summary>
        /// Максимальный объём контрактов который можно купить на портфель
        /// </summary>
        /// <param name="openprice">Цена открытия (в пунктах)</param>
        /// <param name="stop_price">Стоп цена (в пунктах)</param>
        /// <returns></returns>
        decimal maxVolumeByPortfolio(decimal openprice, decimal stopprice,
            decimal AvailableAmount) {

            decimal stopitems = Math.Abs(openprice - stopprice);
            logger.AddDebugLog(
                "Вычисление стопа в пунктах stopitems({0})" +
                "= Math.Abs(openprice({1}) - stopprice({2}))",
                stopitems, openprice, stopprice);

            decimal margin = marginValue(openprice, stopprice);
            decimal max_available_vol = Math.Floor(AvailableAmount / margin);
            logger.AddDebugLog(
                "Максимальное количество контрактов " +
                "max_available_vol({0}) = Floor(portfolio_.BeginAmount.Value({1}) / margin({2}))",
                max_available_vol, AvailableAmount, margin);

            decimal totalrisk = (stopitems * stop_k_ * max_available_vol) * (security.MinStepPrice / security.MinStepSize);
            logger.AddDebugLog(
                "Вычисление риска в рублях на макс. возможное число контрактов " +
                "totalrisk({0}) = (stopitems({1}) * stop_k_({2}) * max_available_vol({3})) * (sec_.MinStepPrice({4}) / sec_.MinStepSize({5}))",
                totalrisk, stopitems, stop_k_, max_available_vol, security.MinStepPrice, security.MinStepSize);
            
            decimal vol = (AvailableAmount - totalrisk) / margin;
            vol = Math.Floor(vol);
            logger.AddDebugLog(
                "Вычисляем максимальный объём: " +
                "vol({0}) = (AvailableAmount({1}) - totalrisk({2})) / margin({3})",
                vol, AvailableAmount, totalrisk, margin);

            return vol;
        }  // maxVolumeByPortfolio
        /// <summary>
        /// Расчёт максимального колиества контрактов 
        /// </summary>
        /// <param name="open_price"></param>
        /// <param name="stop_price"></param>
        /// <param name="AvailableAmount"></param>
        /// <returns></returns>
        decimal maxVolumeByEquity(decimal open_price, decimal stop_price, 
            decimal AvailableAmount) {
            decimal totalriskprice = Math.Abs(open_price - stop_price);

            logger.AddDebugLog(
                "Риск на один контракт на один стоп в пунктах: " +
                "totalriskprice({0}) = Abs(open_price({1}) - stop_price({2}))",
                totalriskprice, open_price, stop_price);

            totalriskprice *= (security.MinStepPrice / security.MinStepSize);
            logger.AddDebugLog(
                "Риск на один стоп в деньгах: " +
                "totalriskprice({0}) *= (sec_.MinStepPrice({1}) / sec_.MinStepSize({2}))",
                totalriskprice, security.MinStepPrice, security.MinStepSize);

            decimal equityrisk = AvailableAmount * percent_equity_risk_ / 100;
            logger.AddDebugLog(
                "Максимальный риск для портфеля: " +
                "equityrisk({0}) = AvailableAmount({1})" +
                " * percent_equity_risk_({2}) / 100",
                equityrisk, AvailableAmount, percent_equity_risk_);

            decimal vol = equityrisk / totalriskprice;
            logger.AddDebugLog(
                "Максимальное количество торгуемых контрактов по риску:" +
                " vol({0}) = equityrisk({1}) / totalriskprice({2})",
                vol, equityrisk, totalriskprice);

            vol = Math.Floor(vol);
            return vol;
        }  // maxVolumeBy

        
        /// <summary>
        /// Риск в процентах от текущего состояния счёта
        /// </summary>
        private decimal percent_equity_risk_;
        /// <summary>
        /// Коэффициент стоп уровня
        /// </summary>
        private decimal stop_k_ = 1.5m;
    }
}  // namespace