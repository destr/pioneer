﻿using System;
using StockSharp.Logging;
namespace Robot.Common.Equity {
    /// <summary>
    /// Рассчёт объма сделки на основне процента от капитала
    /// </summary>
    public class EquityVolumeManager : IVolumeManager {
        public EquityVolumeManager(decimal equitypercent) {
            equitypercent_ = equitypercent;
        }

        public override decimal maxVolume(decimal openprice, decimal stopprice) {
            decimal avail = portfolio.BeginValue * equitypercent_ / 100;
            logger.AddDebugLog("Доступные средства для занятия позиции " + 
                "avail({0}) = portfolio.BeginValue({1}) * equitypercent_({2}) / 100",
                avail, portfolio.BeginValue, equitypercent_);
            
            decimal margin = marginValue(openprice, stopprice);
            decimal vol = Math.Floor(avail / margin);
            logger.AddDebugLog("Расчёт максимального количества контрактов" +
                " vol({0}) = avail({1}) / margin({2})",
                vol, avail, margin);
            return vol;
        }  // maxVolume
        public override decimal maxVolumeAfterStop(decimal openprice,
            decimal stopprice, decimal prevvolume, decimal newstopprice) {

            return maxVolume(openprice, stopprice);
        }

        decimal equitypercent_;
    }
}  // namespace RobotCommon.Equity