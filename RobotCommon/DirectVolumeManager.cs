﻿namespace Robot.Common.Equity {
    /// <summary>
    /// Прямой менеджер объёма. Возращает заданное значение
    /// </summary>
    public class DirectVolumeManager : IVolumeManager {
        public DirectVolumeManager(decimal volume) {
            volume_ = volume;
        }  // Ctor

        public override decimal maxVolume(decimal openprice, decimal stopprice) {
            return volume_;
        }
        public override decimal maxVolumeAfterStop(decimal openprice,
            decimal stopprice, decimal prevvolume, decimal newstopprice) {
            return (volume_);
        }

        decimal volume_ = 0;
    }  // class DirectVolumeManager
}  // namespace RobotCommon.Equity