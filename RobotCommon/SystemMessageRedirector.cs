﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Robot.Common.Exchange.Rts;
using Robot.Logging;
using Robot.Sms;
using StockSharp.Logging;
using StockSharp.Quik;

namespace Robot.Common.Quik {
    /// <summary>
    /// Перенаправление системных сообщений quik содержащих ключевые
    /// слова sms-подписчикам
    /// </summary>
    public sealed class SystemMessageRedirector : ISmsable {

        public event Action<SmsMessage> Sms;
        public QuikTerminal Terminal { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        public SystemMessageRedirector() {
            messagePatterns = new Regex[] {
                new Regex(@"DDE.*ошибк",
                    RegexOptions.IgnoreCase | RegexOptions.Compiled),
                new Regex(@"сервер|проблемы",
                    RegexOptions.IgnoreCase | RegexOptions.Compiled)

            };
        }
        /// <summary>
        /// Запустить редиректор сообщений
        /// </summary>
        public void Start() {
            var list = Terminal.GetMessages();
            if (list != null) {
                _lastCount = list.Count();
            }

            _thread = new Thread(work);
            _work = true;
            _thread.Start();
        }  // Stop
        /// <summary>
        /// Остановить редиректор сообщений
        /// </summary>
        public void Stop() {
            _work = false;
            _timeOutEvent.Set();
            _thread.Join();
        }  // Stop
        /// <summary>
        /// Работа в потоке
        /// </summary>
        void work() {
            while (_work) {
                // ждём таймаута или нас будят
                try {
                    if (_timeOutEvent.WaitOne(_checkInterval)) {
                        // разбудили руками, значит завершают
                        return;
                    }
                    checkNewMessages();
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(e);
                    RobotLogger.Instance.AddWarningLog("Получение сообщений quik прервано");
                    break;
                }
            }
        }
        
        /// <summary>
        /// Проверка наличия новых сообщений
        /// </summary>
        void checkNewMessages() {
            IEnumerable<string> list = Terminal.GetMessages();
            int curCount = list.Count();
            if (curCount == 0) {
                return;
            }
            // ничего нового
            if (_lastCount == curCount) {
                return;
            }
            
            // Определяем количество новых сообщений
            int newMessageCount = 0;
            if (_lastCount < curCount) {
                newMessageCount = curCount - _lastCount;
            } else {
                // список сообщений был очищен
                newMessageCount = curCount;
            }

            List<string> newMessages = new List<string>();
            for (int i = 0; i < newMessageCount; ++i) {
                newMessages.Add(list.ElementAt(curCount - i - 1));
            }

            _lastCount = curCount;

            filterMessages(newMessages.AsEnumerable<string>());
        }  // checkNewMessages

        /// <summary>
        /// Выполнить фильтрацию сообщений
        /// </summary>
        /// <param name="message"></param>
        void filterMessages(IEnumerable<string> newMessages) {
            foreach (string message in newMessages.Reverse<string>()) {
                RobotLogger.Instance.AddInfoLog(
                    "Новое quik сообщение: `{0}'", message);
                //Console.WriteLine("!!!Valid Message: {0}", message);
                //msgparts[1] = "Внимание! На данном сервере имеются технические проблемы. Просим Вас использовать Резервный сервер или Сервер М-10";
                //msgparts[1] = "Внимание! На текущий момент имеются тех.проблемы с рынком ММВБ на всех серверах. Проблема на стороне ММВБ.";
                //msgparts[1] = " DDE сервер 'STOCKSHARP'. Документ 'все сделки[]'. Таблица 'все сделки'. Произошла ошибка: Ошибка при передаче таблицы, вывод приостановлен. Исчерпано время для обмена данными: сервер слишком загружен";
                foreach (Regex re in messagePatterns) {
                    if (re.IsMatch(message)) {
                        RobotLogger.Instance.AddDebugLog(
                            "Quik сообщение `{0}' удовлетворяет шаблону `{1}'",
                            message, re.ToString());
                        if (WorkingCalendar.isTradingTime(DateTime.Now.TimeOfDay)) {
                            Sms.Invoke(
                                new SmsMessage() {
                                    Sender = GateName,
                                    Text = "important quik message"
                                });
                        }
                        break;
                    }
                }
                
            }
        }  // filterMessage

        /// <summary>
        /// Название шлюза от которого приходят сообщения
        /// </summary>
        public string GateName { get ; set; }
        /// <summary>
        /// Поток
        /// </summary>
        Thread _thread = null;
        /// <summary>
        /// Событие, по тайм-ауту которого осуществляется поиск новых сообщений
        /// </summary>
        AutoResetEvent _timeOutEvent = new AutoResetEvent(false);
        
        /// <summary>
        /// Признак работы потока
        /// </summary>
        bool _work = true;
        
        /// <summary>
        /// Интервал проверки новых сообщений
        /// </summary>
        static readonly TimeSpan _checkInterval = TimeSpan.FromSeconds(10);
        
        /// <summary>
        /// Количество соощений полученное при последней проверке
        /// </summary>
        int _lastCount = 0;

        /// <summary>
        /// Список шаблонов сообщений требующих смс сигнализирования
        /// </summary>
        IEnumerable<Regex> messagePatterns;
    }
}  // namespace RobotCommon.Quik