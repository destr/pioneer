﻿using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace Robot.Common.Equity {
    /// <summary>
    /// Интерфейс менедеров объёма
    /// </summary>
    public abstract class IVolumeManager {                              
        /// <summary>
        /// Рассчитать максимальный объём
        /// </summary>
        /// <param name="openprice">Цена открытия свечи</param>
        /// <param name="stopprice">Стоп цена</param>
        /// <returns></returns>
        public abstract decimal maxVolume(decimal openprice, decimal stopprice);
        public abstract decimal maxVolumeAfterStop(decimal openprice,
            decimal stopprice, decimal prevvolume, decimal newstopprice);
        /// <summary>
        /// Получение ГО в зависимости от направления сделки
        /// </summary>
        /// <param name="openprice">Цена открытия свечи входа</param>
        /// <param name="stopprice">Стоп-цена</param>
        /// <returns></returns>
        protected decimal marginValue(decimal openprice, decimal stopprice) {
            decimal margin = (openprice < stopprice) ? security.MarginSell : security.MarginBuy;
            logger.AddDebugLog(
                "Получение ГО " +
                "margin({0}) = (openprice({1}) < stopprice({2})) ? " +
                "sec_.MarginSell({3}) : sec_.MarginBuy({4})",
                margin, openprice, stopprice, security.MarginSell, security.MarginBuy);
            return margin;
        }  // margin

        /// <summary>
        /// Портфель
        /// </summary>
        public Portfolio portfolio {get; set;}
        /// <summary>
        /// Инструмент, для которого рассчитывается риск
        /// </summary>
        public Security security { get; set; }
        /// <summary>
        /// Интерфейс логгера стратегии
        /// </summary>
        public ILogReceiver logger { get; set; }
    }
}  // namespace RobotCommon.Equity