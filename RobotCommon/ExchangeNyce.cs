﻿using System;
using StockSharp.BusinessEntities;

namespace Robot.Common.Exchange {
    public class ExchangeNyse : StockSharp.BusinessEntities.ExchangeBoard {
        public ExchangeNyse() {
            WorkingTime nycewt = ExchangeBoard.Forts.WorkingTime.Clone();
            nycewt.SpecialHolidays = new DateTime [] {};
            nycewt.SpecialWorkingDays = new DateTime [] {};
            base.WorkingTime = nycewt;

            Exchange = ExchangeBoard.Forts.Exchange.Clone();
            Exchange.Name = "Nyse";
        }
    }
}
