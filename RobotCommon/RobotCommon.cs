﻿using System;
using System.Text.RegularExpressions;
using Robot.Logging;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace Robot.Common {
namespace Helpers {
    /// <summary>
    /// Правила округления
    /// </summary>
    public enum RoundRule {
        /// <summary>
        /// Автоматически, поведение функции Math.Round()
        /// </summary>
        Auto,
        /// <summary>
        /// В большую сторону
        /// </summary>
        More,
        /// <summary>
        /// В меньшую
        /// </summary>
        Less
    }
    public static class Common {
        public static string WildcardToRegex(string pattern) {
            return String.Format("^{0}$",
                Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", "."));
        }  // WildcardToRegex
    }
    public static class TradeHelper {
        /// <summary>
        /// Округление цены кратно цене инструмента с проверкой границ минимальной 
        /// и максимальной цен
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <param name="price">Цена</param>
        /// <param name="rule">Правило округления</param>
        /// <returns>Округлённая цена</returns>
        public static decimal RoundPrice(Security security, decimal price,
            RoundRule rule = RoundRule.Auto) {
            decimal quot = price / security.MinStepSize;
            switch (rule) {
                case RoundRule.Auto:
                    quot = Math.Round(quot);
                    break;
                case RoundRule.Less:
                    quot = Math.Floor(quot);
                    break;
                case RoundRule.More:
                    quot = Math.Ceiling(quot);
                    break;
            }

            quot *= security.MinStepSize;
            return CheckAndCorrectRangePrice(security, quot);
            
        }  // RoundPrice
        /// <summary>
        /// Проверить цена на принадлежность минимальному и максимальному диапазону цен и
        /// скорректировать цену, в случае выхода за пределы диапазона
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <param name="price">Цена</param>
        /// <returns></returns>
        public static decimal CheckAndCorrectRangePrice(Security security, decimal price) {
            // проверка границ цены
            if (price > security.MaxPrice) {
                RobotLogger.Instance.AddWarningLog(
                    "Округлённая цена {0} инструмента {1} больше максимльной {2}. Принудительное" +
                    " округление до максимальной!", price, security.Code, security.MaxPrice);
                price = security.MaxPrice;
                return price;
            }
            if (price < security.MinPrice) {
                RobotLogger.Instance.AddWarningLog(
                    "Округлённая цена {0} инструмента {1} меньше минимальной {2}. Принудительное" +
                    " округление до минимальной!", price, security.Code, security.MinPrice);
                price = security.MinPrice;
                return price;
            }
            return price;
        }
        
    }
}  // namespace Helpers
}  // namespace RobotCommon