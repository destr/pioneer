﻿using System;

namespace Robot.Common {
namespace Helpers {
    /// <summary>
    /// Обёртка над событием Error стратегии
    /// генерирующая событие с двумя параметрами
    /// </summary>
    public class StrategyEventErrorWrapper {
        
        /// <summary>
        /// Название стратегии
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Генерируемое событие
        /// </summary>
        public event Action<string, Exception> Error;
        
        /// <summary>
        /// Вызов события error с именем стратегии
        /// </summary>
        /// <param name="e">Исключение</param>
        public void wrap(Exception e) {
            Error.Invoke(Name, e);
        }  // wrap
    }  // class StrategyEventErrorWrapper
}  // namespace Helpers
}  // namespace RobotCommon