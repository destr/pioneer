﻿using System;
using System.Collections.Generic;
using Robot.Common.Exchange.Rts;
using Robot.Logging;
using Robot.Sms;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Quik;

namespace Robot.Common.Quik {

    public class ExportMessage : SmsMessage {
        [Flags]
        public enum MessageType {
            Timeout,
            Restored
        }
        private MessageType _type;
        new public string Sender { get; protected set; }
        public MessageType Type {
            set {
                _type = value;
                base.Sender = String.Format("export {0}", _type);
            }
            get {
                return _type;
            }
        }
        
        public string Trader;
        public override string ToText() {
            string ret;
            ret = String.Format("{0}: {1}", Trader, base.Sender);
            return ret;
        }
    }

    /// <summary>
    /// Обработчик тайм-аута экспорта DDE
    /// </summary>
    public class ExportTimeOutHandler : ISmsable {
        public event Action<SmsMessage> Sms;
        /// <summary>
        /// Признак тайм-аута
        /// </summary>
        public bool ExportTimeouted { get {
            return _smsSend;
        }}
        public ExportTimeOutHandler(QuikTrader trader) {
            Trader = trader;
            
            Trader.ReConnectionSettings.ExportTimeOutInterval = _timeOutInterval;
            Trader.ReConnectionSettings.ExportTimeOut += new Action(timeOut);
        }
        
        /// <summary>
        /// Обработчик события тайм-аута экспорта
        /// </summary>
        void timeOut() {
            // в неторговый день ничего не делаем
            if (!WorkingCalendar.isTradingDate(DateTime.Now)) {
                RobotLogger.Instance.AddDebugLog(
                    "Проблемы экспорта, но день не торговый. Игнорируется");
                return;
            }
            // ВО время экспирации опцинонов сессия начинается позже
            // помним про этом
            if (WorkingCalendar.isExpirationDate(DateTime.Now)) {
                TimeSpan time = DateTime.Now.TimeOfDay;
                if (WorkingCalendar.isEveningSession(time)) {
                    // до 19-10 можно забить
                    if (time < WorkingCalendar.ExpirationDaySessionTime) {
                        RobotLogger.Instance.AddDebugLog(
                            "День экспирации опционов. Вечерняя сессия начинается в {0}" +
                            ". Проблемы экспорта игнорируются",
                            WorkingCalendar.ExpirationDaySessionTime);
                        return;
                    }
                }
            }

            if (_smsSend) {
                return;
            }
            

            // сообщение ещё не отправлялось
            // отправляем сообщеие, подписываемся на все сделки
            //SmsClient.send(String.Format("{0}: export timeout!", Trader.Name)); 
            Sms.Invoke(new ExportMessage() {
                Type = ExportMessage.MessageType.Timeout,
                Trader = Trader.Name
            }); 
            _smsSend = true;
            
            Trader.NewTrades += new Action<IEnumerable<Trade>>(newTrades);
            RobotLogger.Instance.AddInfoLog("Проблемы с DDE экспортом");
        }
        void newTrades(IEnumerable<Trade> trades) {
            // если мы тут, значит пошли сделки и можно восстанавливать состояние
            RobotLogger.Instance.AddDebugLog(
                "Появились новые сделки, экспорт восстановлен");

            _smsSend = false;
            //SmsClient.send("{0}: export restored", Trader.Name);
            Sms.Invoke(new ExportMessage() {
                Trader = Trader.Name,
                Type = ExportMessage.MessageType.Restored

            });
            Trader.NewTrades -= newTrades;

        }  // newOrders

        /// <summary>
        /// Получить шлюз
        /// </summary>
        public QuikTrader Trader {get; internal set;}

        /// <summary>
        /// Признак того, что смс о проблемах экспорта уже отправлялось
        /// </summary>
        bool _smsSend = false;

        /// <summary>
        /// Интервал проверки тайм-аута экспорта
        /// </summary>
        TimeSpan _timeOutInterval = TimeSpan.FromSeconds(60);
    }
}  // namespace RobotCommon.Quik