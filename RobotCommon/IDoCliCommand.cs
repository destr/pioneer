﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace Robot.Common.Cli {
    [Flags]
    /// <summary>
    /// Команда
    /// </summary>
    public enum Command {
        /// <summary>
        /// Сигнал входа в лонг
        /// </summary>
        [Description("Ручной сигнал входа в позицию")]
        Long,
        /// <summary>
        /// Сигнал входа в шорт
        /// </summary>
        [Description("Ручной сигнал входа в позицию")]
        Short,
        /// <summary>
        /// Информация о стратегии
        /// </summary>
        [Description("Информация о стратегии")]
        InfoSt,
        /// <summary>
        /// Запустить стратегию
        /// </summary>
        [Description("Запустить стратегию")]
        StartSt,
        /// <summary>
        /// Остановить стратегию
        /// </summary>
        [Description("Остановить стратегию")] 
        StopSt,
        /// <summary>
        /// Загрузить историю свечей для стратегии согласно конфигурации
        /// </summary>
        [Description("Загрузить историю свечей для стратегии согласно конфигурации")]
        LoadHistorySt,
        /// <summary>
        /// Загрузить и сохранить настройки стратегии
        /// </summary>
        [Description("Загрузить и сохранить настройки стратегии")]
        LoadSaveSt,
        /// <summary>
        /// Остановить шлюз
        /// </summary>
        [Description("Остановить шлюз")]
        StopGw,
        /// <summary>
        /// Запустить шлюз
        /// </summary>
        [Description("Запустить шлюз")]
        StartGw,
        /// <summary>
        /// Показать информацию о шлюзе
        /// </summary>
        [Description("Показать информацию о шлюзе")]
        ShowGw,
        /// <summary>
        /// Переключение режима отправки смс
        /// </summary>
        [Description("Переключение режима отправки смс")]
        Smsdummy,
        /// <summary>
        /// Закрыть позицию
        /// </summary>
        [Description("Закрыть позицию стратегии")]
        ClosePosition,
        /// <summary>
        /// Не показывать уровень логгирования в консоле
        /// </summary>
        [Description("Не показывать уровень логгирования в консоле")]
        IgnoreLogLevel,
        /// <summary>
        /// Очистить уровни логгирования
        /// </summary>
        [Description("Очистить уровни логгирования")]
        ClearLogLevels,
        /// <summary>
        /// Установить временя первой свечи
        /// </summary>
        [Description("Установить временя первой свечи")]
        SetBaseCandleTime,
        /// <summary>
        /// Перерегестрировать стоп
        /// </summary>
        [Description("Перерегестрировать стоп")]
        ReRegisterStop,
        /// <summary>
        /// Загрузка данных с финам
        /// </summary>
        [Description("Загрузка данных с финам")]
        FinamLoad,
        /// <summary>
        /// Загрузить даные с финам и сохранить как
        /// </summary>
        [Description("Загрузить даные с финам и сохранить как")]
        FinamLoadAs,
        /// <summary>
        /// Обновить информацию об инструментах
        /// </summary>
        [Description("Обновить информацию об инструментах")]
        FinamUpdate,
        /// <summary>
        /// Поиск инструмента в кеше
        /// </summary>
        [Description("Поиск инструмента в кеше")]
        FinamSearch,
        /// <summary>
        /// Показать список алиасов инструментов
        /// </summary>
        [Description("Вывести список алиасов инструментов")]
        FinamAlias,
        /// <summary>
        /// Неизвестная команда
        /// </summary>
        Unknown,
        /// <summary>
        /// Показать справку по команде
        /// </summary>
        [Description("Показать эту справку")]
        Help
    }
    /// <summary>
    /// Описание команды
    /// </summary>
    public class CommandDescription {
        /// <summary>
        /// Команда
        /// </summary>
        public Command Cmd;
        /// <summary>
        /// Аргументы
        /// </summary>
        public string[] Args;
        /// <summary>
        /// Название метода сформированного по команде
        /// </summary>
        /// <returns>Название метода</returns>
        public string MethodName() {
            return String.Format("{0}Command", Cmd.ToString());
        }
    }
    public static class Helper {
        public static bool InvokeCommand(object obj, CommandDescription command) {
            MethodInfo method = obj.GetType().GetMethod(command.MethodName());
            if (method == null) {
                Console.WriteLine("Method {0} not found in object {1}", command.MethodName(),
                    obj.GetType().Name);
                return false;
            }
            method.Invoke(obj, new object[] { command.Args });
            return true;
        }
    }

    public interface IDoCliCommand {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="cmd">Описание команды</param>
        /// <returns>true если команда выполнена</returns>
        bool DoCliCommand(CommandDescription cmd);
    }
}