﻿
namespace Robot.Common.Types {
    public enum VolumeUnit {

        /// <summary>
        /// Количество контрактов
        /// </summary>
        Volume,

        /// <summary>
        /// Процет риска
        /// </summary>
        RiskPercent,

        /// <summary>
        /// Процент от капитала
        /// </summary>
        EquityPercent,
    }
}
