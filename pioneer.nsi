;--------------------------------
; ������������ ������������

  #���������� ���������� � ��������� PE �����
  !ifndef PRODUCT_NAME
   !define PRODUCT_NAME       'PioneerRobot'             # ��� �������� ��� ������������
  !endif

  !ifndef VERSION
    !define VERSION            '1.0.0'
  !endif
  
  !define VI_COMMENTS        'Pioneer robot'

  !define VI_COMPANY         'Multiple Market Systems'
  !define VI_TRADEMARKS      'Multiple Market Systems'
  !define VI_COPYRIGHT       'Copyright (C) Multiple Market Systems' 
  !define VI_FILEDESCRIPTION 'Pioneer robot installer'
  !define VI_PRODUCT_NAME    "${PRODUCT_NAME}"
  
  !ifndef BUILD_NUMBER
	!define BUILD_NUMBER 0
  !endif

  !define FULL_VERSION "${VERSION}.${BUILD_NUMBER}"
  # ��� ����� �����������
  !ifndef INSTALLER_NAME
    !define INSTALLER_NAME     "${PRODUCT_NAME}_${FULL_VERSION}.exe"
  !endif
  # ������� ���������� �� ���������
  
	!define DEFAULT_INSTDIR "C:\${PRODUCT_NAME}"
;------------------------------------------------------------------------------
; ������������ �����
!include "MUI2.nsh"

;------------------------------------------------------------------------------
; ���������

  #��������
#  !define MUI_HEADERIMAGE
#  !define MUI_HEADERIMAGE_BITMAP "st_logo.bmp" ; optional
#  !define MUI_ABORTWARNING
  #���������� ��������� ��������� � ��������
  !define MUI_LANGDLL_ALLLANGUAGES
  #�������� ������������
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  #���� ����������
  !insertmacro MUI_LANGUAGE "Russian"

;------------------------------------------------------------------------------
; �������� ������������

  # ������������ ��
  Name "${PRODUCT_NAME}"
  # ��� ����� ������������
  OutFile "${INSTALLER_NAME}"
  # ����� ��������� �� ���������
  InstallDir "${DEFAULT_INSTDIR}"

  #������� ������� ��� ���������� ������������
  RequestExecutionLevel user

  #���������� � ������
  VIProductVersion `${FULL_VERSION}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "ProductName" `${VI_PRODUCT_NAME}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "Comments" `${VI_COMMENTS}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "CompanyName" `${VI_COMPANY}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "LegalTrademarks" `${VI_TRADEMARKS}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "LegalCopyright" `${VI_COPYRIGHT}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "FileDescription" `${VI_FILEDESCRIPTION}`
  VIAddVersionKey /LANG=${LANG_RUSSIAN} "FileVersion" `${FULL_VERSION}`

  BrandingText "${PRODUCT_NAME}"
  Caption "${PRODUCT_NAME} ${FULL_VERSION}"
  CRCCheck on

;------------------------------------------------------------------------------
; ������ ������������

; ������ ����������� ������
Section "�������" sid_clean

  ;��������� ������������ � ����� ���������
  SetDetailsView show
  SetOverwrite on
  SetShellVarContext all

  IfFileExists "$INSTDIR" 0 finish_delete
    SetOutPath "$INSTDIR"

  Delete  "*"

  finish_delete:
  IfFileExists "$INSTDIR" +2
  CreateDirectory "$INSTDIR"
  FileOpen $0 "$INSTDIR\version.ini" w
  FileWrite $0 ${FULL_VERSION}
  FileClose $0
  
  ClearErrors
SectionEnd

; ��������� �� ������� ������ (������� bin)
Section "����� ������" sid_user

  #---[ bin ]---
  CreateDirectory "$INSTDIR"
  SetOutPath "$INSTDIR"
  
  File release\*
  
  ClearErrors
SectionEnd

Section /o "������ ������������" sid_configexample
	SetOutPath "$INSTDIR\configexample"
  File configexample\*
  
  SetOutPath "$INSTDIR\configexample\Strategies"
  File configexample\Strategies\*
  
  ClearErrors
SectionEnd

; �������� ��� ������������� - ����� ������ (�.�. ��������) � ����������� �� ������ ��������� (silent/not slient)
Function .onInit
  #������� ������ �������� ����������� ������������
  #IntOp $0 ${SF_SELECTED} | ${SF_RO}
  #SectionSetFlags ${sid_clean} $0

  # �������������� ������� ���������� �����
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "${VI_PRODUCT_NAME}") i .r1 ?e'

  Pop $R0
  StrCmp $R0 0 +3
    MessageBox MB_OK|MB_ICONEXCLAMATION "Installer ${PRODUCT_NAME} already running"
    Abort

FunctionEnd

LangString DESC_sid_clean ${LANG_RUSSIAN} "�������� ������ ������������� �����"
LangString DESC_sid_user ${LANG_RUSSIAN} "��������� ����� ������ ������"
LangString DESC_sid_configexample ${LANG_RUSSIAN} "������ ���������������� ������ ������"

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${sid_clean} $(DESC_sid_clean)
  !insertmacro MUI_DESCRIPTION_TEXT ${sid_user} $(DESC_sid_user)
  !insertmacro MUI_DESCRIPTION_TEXT ${sid_configexample} $(DESC_sid_configexample)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

