﻿// <copyright file="RobotLogger.cs" company="Multiple Market Systems"> 
// Multiple Market Systems
// </copyright>
namespace Robot.Logging
{
    using System;
    using Ecng.Collections;
    using StockSharp.Logging;

    /// <summary>
    /// Глобальный логгер робота
    /// </summary>
    public class RobotLogger : ILogReceiver
    {
        /// <summary>
        /// Экземпляр объекта
        /// </summary>
        private static volatile RobotLogger instance;

        /// <summary>
        /// Объект синхронизации
        /// </summary>
        private static object syncObject = new object();

        /// <summary>
        /// Уровень логгирования
        /// </summary>
        private LogLevels levels;

        /// <summary>
        /// Предотвращает вызов конструктора по умолчанию для класса 
        /// <see cref="RobotLogger"/>
        /// </summary>
        private RobotLogger()
        {
            this.Name = "PioneerRobot";
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Событие нового сообщения в логе
        /// </summary>
        public event Action<LogMessage> Log;

        /// <summary>
        /// Получает экземляр объекта
        /// </summary>
        public static RobotLogger Instance 
        {
            get 
            {
                if (instance == null) 
                {
                    lock (syncObject) 
                    {
                        if (instance == null) 
                        {
                            instance = new RobotLogger();
                        }
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Получает GUID логгера
        /// </summary>
        public Guid Id { get; internal set; }

        /// <summary>
        /// Получает родительсткий логгер
        /// </summary>
        ILogSource ILogSource.Parent 
        { 
            get { return null; } 
        }

        /// <summary>
        /// Получает имя логгера
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Получает дочерние источники логов. Всегда null
        /// </summary>
        public INotifyList<ILogSource> Childs
        {
            get { return null; } 
        }

        /// <summary>
        /// Получает текущее дата
        /// </summary>
        public DateTime CurrentTime
        {
            get { return DateTime.Now; } 
        }

        /// <summary>
        /// Получает или задает уровень логгирования
        /// </summary>
        public LogLevels LogLevel 
        {
            get { return this.levels; }
            set { this.levels = value; }
        }

        /// <summary>
        /// Добавление нового сообщения в лог
        /// </summary>
        /// <param name="message">Добавить сообщение</param>
        public void AddLog(LogMessage message) 
        { 
            this.Log.Invoke(message); 
        }

        /// <summary>
        /// Очистка объекта
        /// </summary>
        public void Dispose()
        {
        }
    }
}  // namespace CommonRobot
