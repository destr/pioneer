﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Robot.Common.Quik;
using Robot.Configuration;
using Robot.Logging;
using Robot.Sms;
using Robot.Strategy;
using StockSharp.Algo.Candles;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Quik;

namespace PioneerRobot {
    using StringList = List<string>;
    /// <summary>
    /// Состояние менеджера шлюзов
    /// </summary>
    [Flags]
    enum TraderManagerState {
        /// <summary>
        /// Начальное состояние
        /// </summary>
        None        = 0,
        /// <summary>
        /// Портфели найдены
        /// </summary>
        PortfolioOk = 0x1,
        /// <summary>
        /// Инструменты найдены
        /// </summary>
        SecurityOk  = 0x2
    }
    /// <summary>
    /// Создатель торговых шлюзов
    /// </summary>
    public partial class TraderManager : ISmsable {
        public event Action<SmsMessage> Sms;
        private AutoResetEvent      _eventWait;
        public  QuikTrader          _trader {get; internal set;}
        private QuikTerminal        _terminal;
        public  CandleManager       _candleManager;
        private TraderManagerState  _state = TraderManagerState.None;
        /// <summary>
        /// Признак запущенного менеджера
        /// </summary>
        public bool IsRunning = false;
        /// <summary>
        /// Конфигурация
        /// </summary>
        private Gate _gate;
        /// <summary>
        /// Фабрика стратегий
        /// </summary>
        private StrategyFactory     _factory;
        /// <summary>
        /// Менеджер стратегий
        /// </summary>
        private StrategyManager     _strategyManager;
        public StrategyManager StrategyManager { get  {return _strategyManager;} }

        private StringList _securityList = new StringList();
        private StringList _portfolioList = new StringList();
        private SystemMessageRedirector _systemMessageRedirector = null;
        public string Name {
            get {
                if (_trader == null) {
                    return _gate.name;
                }
                return _trader.Name; 
            }
        }

        public TraderManager(StrategyFactory factory, Gate g) {
            _gate = g;
            _factory = factory;
            _eventWait     = new AutoResetEvent(false);
        }
        public void create() {
            startQuik();
            // получаем из конфигураций стратегий инструменты и портфели, 
            // которые должен экспортировать шлюз, и мы этого будем ждать
            //neededSecuritiesAndPortfolios();

            _trader = new QuikTrader() {
                Name = _gate.name,
                LogLevel = LogLevels.Debug
            };

            _trader.ReConnectionSettings.ReConnectingAttemptCount = -1;
            _trader.ReConnectionSettings.Interval = TimeSpan.FromSeconds(10);
            _trader.ReConnectionSettings.ConnectDisconnectTimeOutInterval = TimeSpan.FromSeconds(10);
            _trader.ReConnectionSettings.ConnectingAttemptCount = 10;
            _trader.ReConnectionSettings.WorkingTime = ExchangeBoard.Forts.WorkingTime;
            
            ExportTimeOutHandler handler = new ExportTimeOutHandler(_trader);
            handler.Sms += Sms;

            _trader.ReConnectionSettings.ConnectionRestored += () => 
                RobotLogger.Instance.AddDebugLog("Conecction restored");
            _trader.Disconnected += () => 
                RobotLogger.Instance.AddDebugLog("Trader disconnected");
            _trader.ProcessDataError += (Exception e) => { 
                RobotLogger.Instance.AddErrorLog(e); };
            

            _trader.Connected += new Action(trader__Connected);
            _trader.NewPortfolios += new Action<IEnumerable<Portfolio>>(trader__NewPortfolios);
            _trader.NewSecurities += new Action<IEnumerable<Security>>(trader__NewSecurities);

            
            
            _eventWait.Reset();
            _trader.Connect();
            RobotLogger.Instance.AddInfoLog("Ожидание подключения QUIK...");
            /// ждём 
            int timeout = _gate.ConnectTimeoutSeconds * 1000;
            
            _eventWait.WaitOne(timeout);
            if (!_trader.IsConnected) {
                throw new Exception(
                    String.Format("Превышен интервал ожидания({0} мс) подключения к шлюзу.",
                    timeout));
            }
            // добавляем экспорт минимальной и максимальной цен
            _trader.SecuritiesTable.Columns.Add(DdeSecurityColumns.MinPrice);
            _trader.SecuritiesTable.Columns.Add(DdeSecurityColumns.MaxPrice);
            _trader.SecuritiesTable.Columns.Add(DdeSecurityColumns.MinStepPrice);
            _trader.SecuritiesTable.Columns.Add(DdeSecurityColumns.MarginSell);
            _trader.SecuritiesTable.Columns.Add(DdeSecurityColumns.MarginBuy);
            
            _candleManager = new CandleManager(_trader);
            _candleManager.LogLevel = LogLevels.Debug;
            _candleManager.Container.CandlesKeepTime = 
                TimeSpan.FromDays(UserConf.Instance.robotMain().CandlesKeepDays);
            _trader.TradesKeepTime = 
                TimeSpan.FromDays(UserConf.Instance.robotMain().TradesKeepTime);
            _trader.StartExport();
            RobotLogger.Instance.AddInfoLog("Ожидание инструментов и портфелей...");

            if (!_eventWait.WaitOne(timeout)) {
                whyTimeout();
                throw new Exception(
                    String.Format("Превышен интервал ожидания ({0} мс) инструментов и портфелей",
                    timeout));

            }
            _strategyManager = new StrategyManager(_factory, _candleManager, _trader) {
                GateName = _trader.Name,
                StrategyWatchdog = new StrategyWatchdog() {
                    CheckIntervalMin = TimeSpan.FromMinutes(_gate.WatchIntervalMin),
                    ExportTimeoutHandler = handler
                }
            };

            this.StrategyManager.Sms += Sms;
            _strategyManager.Init();
            _systemMessageRedirector = new SystemMessageRedirector() {
                Terminal = _terminal,
                GateName = _trader.Name};

            _systemMessageRedirector.Sms += Sms;
            _systemMessageRedirector.Start();
            
        }

        public void destroy() {
            RobotLogger.Instance.AddInfoLog("Останов экспорта");
            _trader.StopExport();
            _systemMessageRedirector.Stop();
            _strategyManager.Dispose();
        }  // destroy
        public void start() {
            create();
            _strategyManager.startAllStrategies();
            string smstext = String.Format("started");
            if (_strategyManager.failedStrategy.Count != 0) {
                smstext += String.Format(" St failed: ");
                smstext += String.Join(", ", _strategyManager.failedStrategy); 
                _strategyManager.failedStrategy.Clear();
            }

            Sms.Invoke(new SmsMessage() {
                Sender = Name,
                Text = smstext
            });
            IsRunning = true;
        }
        public void stop() {
            if (!IsRunning) return;
            _strategyManager.stopAllStrategies();
            Sms.Invoke(new SmsMessage() {
                Sender = Name,
                Text = "stopped"
            });


            // UnRegister всех свечей
            foreach (CandleSeries series in _candleManager.Series) {
                _candleManager.Stop(series);
            }

            /// вызываем принудительно сборщик мусора
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            destroy();
        }  // stop
        /// <summary>
        /// Обработчик события новых инструментов
        /// </summary>
        /// <param name="securities"></param>
        void trader__NewSecurities(IEnumerable<Security> securities) { 
            RobotLogger.Instance.AddDebugLog("Обработка новых инструментов...");
            
            int counter = 0;
            foreach (string seccode in _securityList) {
                foreach (Security sec in _trader.Securities) {
                    if (sec.Code == seccode) ++counter;
                }
            }
            if (counter == _securityList.Count) {
                _state |= TraderManagerState.SecurityOk;
                if ((_state & TraderManagerState.PortfolioOk) != 0) {
                    _eventWait.Set();
                }
            }
        }
        /// <summary>
        /// Обработчик события новых портфелей
        /// </summary>
        /// <param name="portfolios"></param>
        void trader__NewPortfolios(IEnumerable<Portfolio> portfolios) {

            RobotLogger.Instance.AddDebugLog("Обработка новых портфелей...");
            int counter = 0;
            /// TODO можно сделать enum и проверять через contains
            foreach (string pname in _portfolioList) {
                foreach (Portfolio p in _trader.Portfolios) {
                    if (p.Name == pname) ++counter;
                }
            }
            if (counter == _portfolioList.Count) {
                _state |= TraderManagerState.PortfolioOk;
                if ((_state & TraderManagerState.SecurityOk) != 0) {
                    _eventWait.Set();
                }
            }
        }

        void trader__Connected() {
            RobotLogger.Instance.AddDebugLog("Подключился к шлюзу");
            _eventWait.Set();
        }
        void startQuik() {
            
            _terminal = QuikTerminal.Get(_gate.path);
            if (!_terminal.IsLaunched) {
                RobotLogger.Instance.AddInfoLog(
                "Запуск QUIK по пути {0}", _gate.path);
                _terminal.Launch();
            } else {
                RobotLogger.Instance.AddInfoLog(
                    "QUIK {0} уже запущен", _gate.path);
            }
            if (!_terminal.IsConnected) {
                RobotLogger.Instance.AddInfoLog(
                    "Подключаюсь к QUIK с логином {0}", _gate.login);
                _terminal.Login(_gate.login, _gate.password);
                RobotLogger.Instance.AddInfoLog("QUIK запущен");
            } else {
                RobotLogger.Instance.AddInfoLog(
                    "QUIK уже подключен");
            }
        }
#if false
        void neededSecuritiesAndPortfolios() {
            try {
                foreach (var p in UserConf.Instance.GateStrategyParameters(_gate.name)) {
                    _securityList.Add(p.security);
                    _portfolioList.Add(p.portfolio);
                }

            } catch (Exception e) {
                RobotLogger.Instance.AddErrorLog(e);
            }
        }
#endif
        /// <summary>
        /// Показать причины по которым возник timeout
        /// </summary>
        void whyTimeout() {
            {
                var listlookup = _trader.Securities.ToLookup(sec => sec.Code);
                var listdiff = _securityList.Where(secname => (!listlookup.Contains(secname)));
                foreach (string sec in listdiff) {
                    RobotLogger.Instance.AddErrorLog(
                        "Не экспортируется инструмент: {0}", sec);
                }
            }
            {
                var listlookup = _trader.Portfolios.ToLookup(p => p.Name);
                var listdiff = _portfolioList.Where(pname => (!listlookup.Contains(pname)));
                foreach (string pname in listdiff) {
                    RobotLogger.Instance.AddErrorLog(
                        "Не экспортируется портфель: {0}", pname);
                }
            }

        }  // whyTimeout
        
    }
}  // namespace PioneerRobot