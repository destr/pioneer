﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using Robot.Common.Exchange.Rts;
using Robot.Configuration;
using Robot.Logging;
using Robot.Sms;
using StockSharp.Logging;


namespace PioneerRobot {
    /// <summary>
    /// Робот
    /// </summary>
    public partial class PioneerRobot: IDisposable, ISmsable {
        public event Action<SmsMessage> Sms;
        /// <summary>
        /// Конструктор
        /// </summary>
        public PioneerRobot() {
            UserConf.Instance.Load();
            Main main = UserConf.Instance.robotMain();
            // Устанавливаем уровень логов для console
            IgnoredLogLevels ignoredLevels = main.IgnoreConsoleLogLevels;

            for (int i = 0; i < ignoredLevels.Count; ++i) {
                LogLevels level = ignoredLevels.GetItemAt(i).name;
                addIgnoreLevel(level);
            }
            _smsEventsMerger = new SmsEventsMerger();

            Sms += new Action<SmsMessage>(_smsEventsMerger.SmsMerge);
            SmsLogRedirector.Sms += _smsEventsMerger.SmsMerge;

            if (!CmdOptions.NoXmlRpc) {
                try {
                    _listenerService = new XmlRpc.PioneerListenerService(this);
                    _xmlRpcServer = new XmlRpc.Server(_listenerService, main.XmlRpcNet, main.XmlRpcPort,
                        main.XmlRpcAccessToken);
                    _xmlRpcServer.Start();
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(
                        string.Format("Ошибка создания xmlrpc сервера {0}"), e.Message);
                    _xmlRpcServer = null;
                    _listenerService = null;
                }
            }
        }  // Ctor
        public void Dispose() {
            _smsEventsMerger.stop();
            if (!CmdOptions.NoXmlRpc && _xmlRpcServer != null) {
                _xmlRpcServer.Stop();
            }
        }  // Dtor

        public void addIgnoreLevel(LogLevels level) {
            switch (level) {
                case LogLevels.Debug:
                    ConsoleListener.Filters.Add(_ignoreDebug);
                    break;
                case LogLevels.Info:
                    ConsoleListener.Filters.Add(_ignoreInfo);
                    break;
            }
        }  // addIgnoreLevel

        /// <summary>
        /// Загрузка конфигурации робота
        /// </summary>
        void loadSettigns() {
            // загружаем все конфигурации стратегий
            foreach (Gate g in UserConf.Instance.Gates()) {
                UserConf.Instance.LoadStrategiesConfigurations(g);
            }

            createTraders();
        }  // loadSettings
        /// <summary>
        /// Запуск робота
        /// </summary>
        public void start() {
            try {
                RobotLogger.Instance.AddInfoLog("Запуск робота {0}({1})", assemblyTitle(),
                    Assembly.GetExecutingAssembly().GetName().Version);
                loadSettigns();
                while (work) {
                    Thread.Sleep(1000);
                    if (exchangeEndWork()) {
                        RobotLogger.Instance.AddInfoLog("Рабочий день закончен");
                        stopTraders();
                        if (!robotSleep()) {
                            break;
                        }
                        startTraders();
                    }
                }
                destroyTraders();
                
            } catch (Exception e) {
                RobotLogger.Instance.AddErrorLog(e);
                /// Ожидаем отправки смс. Не завершаем eventsMerger
                Thread.Sleep(5000);
            } finally {
                _smsEventsMerger.stop();
            }

        }  // start
        /// <summary>
        /// Останов робота
        /// </summary>
        public void stop() {
            work = false;
        }  // stop
        /// <summary>
        /// Разбудить робота и завершить
        /// </summary>
        public void wakeAndStop() {
            stop();
        }  // wakeAndStop
        /// <summary>
        /// Вывести время общее время работы
        /// </summary>
        public void uptime() {
            TimeSpan uptime = DateTime.Now - _startDate;
            Console.WriteLine("{0} up", uptime);
        }  // uptime
        /// <summary>
        /// Получение название робота из assemblyTitle
        /// </summary>
        /// <returns></returns>
        string assemblyTitle () {
            object [] atrs = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            if (atrs.Length > 0) {
                AssemblyTitleAttribute atr = (AssemblyTitleAttribute)atrs[0];
                if (atr.Title != "") {
                    return atr.Title;
                }
            }

            return Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
        }
        /// <summary>
        /// Создание шлюзов
        /// </summary>
        /// <param name="parameters">Список параметров шлюзов</param>
        void createTraders() {
            foreach (Gate g in UserConf.Instance.Gates()) {
                RobotLogger.Instance.AddInfoLog("Создание шлюза {0}..." , g.name);

                try {

                    TraderManager manager = new TraderManager(factory_, g);
                    manager.Sms += Sms;
                    //manager.create();
                    if (!noautostartst) {
                        manager.start();
                    }
                    _traders.Add(manager);
                    
                } catch(Exception e) {
                    RobotLogger.Instance.AddErrorLog(e);
                    Sms.Invoke(new RobotMessage() {
                        Sender = g.name,
                        Message = "start error"
                    });
                }
            }
            noautostartst = false;
            if (_traders.Count == 0) {
                throw new Exception("Не удалось создать ни одного шлюза");
            }
        }  // createTraders
        /// <summary>
        /// Запустить шлюзы
        /// </summary>
        void startTraders() {
            foreach (TraderManager trader in _traders) {
                trader.start();
            }
        } // startTraders
        /// <summary>
        /// Остановить шлюзы
        /// </summary>
        void stopTraders() {
            foreach (TraderManager trader in _traders) {
                trader.stop();
            }  
        }  // stopTraders 

        /// <summary>
        /// Уничтожение шлюзов
        /// </summary>
        void destroyTraders() {
            RobotLogger.Instance.AddInfoLog("Завершение шлюзов...");

            foreach(TraderManager manager in _traders) {
                RobotLogger.Instance.AddInfoLog(
                    "Завершение торгового шлюза {0} ...", manager.Name);
                if (!manager.IsRunning) continue;
                manager.stop();
                manager.destroy();
            }
            _traders.Clear();
        }  // destroyTraders

        bool robotSleep() {
            DateTime nowdate = DateTime.Now;
            // вычисляем время сна
            TimeSpan sleeptime = _wakeupTime - nowdate.TimeOfDay;
            if (sleeptime.CompareTo(TimeSpan.Zero) < 0) {
                TimeSpan days = WorkingCalendar.nextTradingDate(nowdate.Date) - nowdate.Date;
                sleeptime = sleeptime.Add(days);
            }
            DateTime wakeupDate = nowdate.Add(sleeptime);
            RobotLogger.Instance.AddInfoLog("Робот ушёл спать на {0}(до {1})", 
                sleeptime, wakeupDate);
            while (wakeupDate > DateTime.Now) {
                if (!work) {
                    // разбудил пользователь
                    return (false);
                }
                Thread.Sleep(5000);
            }

            RobotLogger.Instance.AddInfoLog("Робот простнулся. Доброе утро!");
            return (true);
        }  // robotSleep

    }  // class PioneerRobot
    

    class MainProgram {
        private static readonly LogManager _logManager = new LogManager();
        static Mutex mutex = new Mutex(true, "{13D5C1CA-80C6-48BD-8A64-13CC2BD0B356}");
        public static LogManager logManager() {
            return _logManager;
        }
        private static PioneerRobot _robot = null;
        private static bool _robotWork = false;

        static void Main(string[] args) {
            _handler += new EventHandler(consoleCtrlCheck);
            SetConsoleCtrlHandler(_handler, true);
            try {

                // По умолчанию включаем Debug
                // Консоль может из конфига и командой переопределить
                RobotLogger.Instance.LogLevel = LogLevels.Debug;
                _logManager.Sources.Add(RobotLogger.Instance);
                
                PioneerRobot.ConsoleListener = new ConsoleLogListener();
                //                PioneerRobot.ConsoleListener.Filters.Add(LogListener.AllErrorFilter);
                //                PioneerRobot.ConsoleListener.Filters.Add(LogListener.AllWarningFilter);

                _logManager.Listeners.Add(PioneerRobot.ConsoleListener);

                PioneerRobot.SmsLogRedirector = new SmsLogRedirectorListener();
                PioneerRobot.SmsLogRedirector.Filters.Add(LogListener.AllErrorFilter);
                PioneerRobot.SmsLogRedirector.Filters.Add(LogListener.AllWarningFilter);

                if (!mutex.WaitOne(TimeSpan.Zero, true)) {
                    Console.WriteLine("Робот уже запущен!");
                    return;
                }

                if (!parseCommandLine(args)) {
                    return;
                }
                FileLogListener fileloglistener = new FileLogListener(
                    GlobalConf.LogFileName()) {
                        Append = true
                    };
                _logManager.Listeners.Add(fileloglistener);

                _robot = new PioneerRobot();
                Cli.CliParser cli = new Cli.CliParser(_robot);
                Thread thread = new Thread(_robot.start);

                _logManager.Listeners.Add(PioneerRobot.SmsLogRedirector);

                thread.Start();
                _robotWork = true;
                while (_robotWork) {
                    string command = Console.ReadLine();
                    if (command == null) {
                        // Прервали по Ctrl+C
                        thread.Join();
                        continue;
                    }
                    if (!cli.Execute(command)) {
                        switch (command) {
                            case "quit":
                                _robot.wakeAndStop();
                                _robotWork = false;
                                break;
                            case "uptime":
                                _robot.uptime();
                                break;
                            case "openlogs":
                                Process.Start("explorer.exe", GlobalConf.StrategyLogDir());
                                break;
                            case "openstorage":
                                Process.Start("explorer.exe", GlobalConf.StorageDir);
                                break;
                            default:
                                RobotLogger.Instance.AddErrorLog(
                                    "Не известная команда `{0}'", command);
                                break;
                        }
                    }
                }
                _robot.Dispose();
                thread.Join();
                mutex.ReleaseMutex();
            } catch (Exception e) {
                RobotLogger.Instance.AddErrorLog(
                    "Произошла критическая ошибка: {0}. Завершаюсь.", e);
                mutex.ReleaseMutex();
                /// Добавляем задержку, иначе логер не успеет ничего вывести.
                Thread.Sleep(2000);
                Environment.Exit(1);
            }
        }
        /// <summary>
        /// Разбор опций командной строки
        /// </summary>
        /// <param name="args">Список опций</param>
        /// <returns>false если произошла ошибка</returns>
        static bool parseCommandLine(string[] args) {

            PioneerRobot.CmdOptions = new Options();
            if (!CommandLine.Parser.Default.ParseArgumentsStrict(args, PioneerRobot.CmdOptions)) {
                return (false);
            }

            if (PioneerRobot.CmdOptions.ConfigDir != null) {
                GlobalConf.ConfigRootDir = PioneerRobot.CmdOptions.ConfigDir;
                RobotLogger.Instance.AddInfoLog("Смена корневого конфигурационного каталога на {0}",
                    GlobalConf.ConfigRootDir);
            }
            if (PioneerRobot.CmdOptions.WakeupTime != null) {
                TimeSpan val = TimeSpan.Parse(PioneerRobot.CmdOptions.WakeupTime);
                RobotLogger.Instance.AddInfoLog("Установка нового времи запуска стратегий: {0}, старое {1}",
                    val.ToString(), PioneerRobot._wakeupTime);
                PioneerRobot._wakeupTime = val;
            }

            if (PioneerRobot.CmdOptions.ExchangeEndTime != null) {
                TimeSpan val = TimeSpan.Parse(PioneerRobot.CmdOptions.ExchangeEndTime);
                RobotLogger.Instance.AddInfoLog("Установка нового времени завершения работы биржи: " +
                    "{0} старое: {1}", val, PioneerRobot._exchangeEndTime);
                PioneerRobot._exchangeEndTime = val;
            }
            if (!string.IsNullOrEmpty(PioneerRobot.CmdOptions.LogDir)) {
                GlobalConf.LogRootDir = PioneerRobot.CmdOptions.LogDir;
                RobotLogger.Instance.AddInfoLog("Смена корневого каталога лог-файлов на {0}" ,
                    GlobalConf.LogRootDir);
            }
            if (PioneerRobot.CmdOptions.NoAutoStartSt) {
                RobotLogger.Instance.AddInfoLog("Стратегии не будут запущены автоматически");
                PioneerRobot.noautostartst = true;
            }
            return (true);
        }  // parseCommandLine
        
        
        /// Ctrl+c
        // Declare the SetConsoleCtrlHandler function
        // as external and receiving a delegate. 
        
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler Handler, bool Add);

        private delegate bool EventHandler(CtrlTypes CtrlType);
        static EventHandler _handler;

        // A delegate type to be used as the handler routine 
        // for SetConsoleCtrlHandler.
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);

        // An enumerated type for the control messages
        // sent to the handler routine.
        public enum CtrlTypes {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }
        /// <summary>
        /// Обработчик событияя
        /// </summary>
        /// <param name="ctrlType">Тип события</param>
        /// <returns>true если событие обработано</returns>
        private static bool consoleCtrlCheck(CtrlTypes ctrlType) {
            if (ctrlType == CtrlTypes.CTRL_C_EVENT ||
                ctrlType == CtrlTypes.CTRL_BREAK_EVENT ||
                ctrlType == CtrlTypes.CTRL_CLOSE_EVENT) {
                RobotLogger.Instance.AddInfoLog("Завершение робота по прерыванию {0}", ctrlType);
                _robotWork = false;
                _robot.wakeAndStop();
                return (true);
            }
            return (false);
        }
    }
}
