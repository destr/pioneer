﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Reflection;
using global::Robot.Common.Cli;
using global::Robot.Logging;
using global::Robot.Strategy.Cli;
using StockSharp.Logging;
using global::Robot.Configuration;
using System.Linq;
using global::Robot.Strategy;

namespace PioneerRobot.Cli {

    /// <summary>
    ///  Парсер команд
    /// </summary>
    class CliParser {
        readonly string[] specialCommands = new string[] { "quit", "uptime", "openlogs", "openstorage"};
        public CliParser(PioneerRobot robot) {
            robot_ = robot;
        }  // Ctor

        public bool Execute(string cmdline) {
            cmdline = cmdline.Trim();

            if (this.specialCommands.Any(cmdline.Contains)) {
                return (false);
            }
            
            // regexp  для разбора команд вида
            // function() for st in trader
            // func(param) for st
            // func() in trader
            // func(t,p) in trader
            Regex cmdre = new Regex("^(\\w+)"   +   // команда
                    "\\s*\\("                   +
                        "([\\w,:&\\.*\\s]+)?"         +   // параметры команды опционально
                    "\\)"                       +
                    "\\s*"                      +
                    "("                         +
                        "(for\\s+"              +
                        "([\\w*?\\.]+))?" +   // название стратегии
                        "(\\s+in\\s+([\\w*?\\.]+))?"   +   // название шлюза
                    ")?$"
                    );
            Match m = cmdre.Match(cmdline);

            if (m.Success) {
                // Название шлюза и название стратегии преобразуем в r
                try {
                    cmd_ = (Command)Enum.Parse(typeof(Command), m.Groups[1].Value, true);
                    cmdparams_ = m.Groups[2].Value;
                    stname_ = m.Groups[5].Value;
                    tradername_ = m.Groups[7].Value;

                    //Console.WriteLine(cmd_);
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(
                        String.Format("Произошла ошибка при разборе команды: `{0}': ",
                            m.Groups[1], e.Message));
                    return (true);
                }
                try {
                    return doCommand();
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(
                        "Произошла ошибка при выполенние команды: {0}: {1}", cmd_, e);
                    /// команда существует. и мы еЁ пытались исполнить
                    return (true);
                }
            }
            RobotLogger.Instance.AddErrorLog("Не удалось разобрать команду: {0}", cmdline);
            return true;
        }  // Execute

        /// <summary>
        /// Выполнение команды
        /// </summary>
        /// <rereturns>false если команду не удалось выполнить</rereturns>
        bool doCommand() {
            CommandDescription cmd = new CommandDescription();
            cmd.Cmd = cmd_;
            cmd.Args = cmdparams_.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);

            switch (cmd.Cmd) {
                case Command.ClosePosition:
                case Command.Long:
                case Command.ReRegisterStop:
                case Command.Short:
                case Command.InfoSt:
                    foreach (ICliStrategy cli in robot_.FindStrategy(tradername_, stname_)) {
                        cli.DoCliCommand(cmd);
                    }
                    break;
                case Command.StartGw:
                case Command.StopGw:
                case Command.ShowGw:
                    foreach (TraderManager m in robot_.FindTraderManager(tradername_)) {
                        try {
                            m.DoCliCommand(cmd);
                        } catch (Exception e) {
                            RobotLogger.Instance.AddErrorLog(e);
                        }
                    }
                    break;
                case Command.StartSt:
                case Command.StopSt:
                    /// Добавляем название стратегии нулевым аргументом для поддержки старого
                    /// синтаксиса команд
                    if (cmd.Args.Length == 0) {
                        cmd.Args = new string [] { stname_ };
                    }
                    foreach (StrategyManager sm in robot_.FindStrategyManager(tradername_)) {
                        try {
                            sm.DoCliCommand(cmd);
                        } catch (Exception e) {
                            RobotLogger.Instance.AddErrorLog(e);
                        }
                    }
                    break;
                case Command.LoadHistorySt:
                    loadHistory();           
                    break;
                case Command.FinamLoad:
                case Command.FinamLoadAs:
                case Command.FinamSearch:
                case Command.FinamUpdate:
                case Command.FinamAlias:
                    robot_.FinamDownloaderCli.DoCliCommand(cmd);
                    break;
                case Command.Help:
                    showHelp();
                    break;
                case Command.Smsdummy:
                    robot_.SmsdummyCommand(cmd.Args);
                    break;
                default:
                    throw new ArgumentException(
                        String.Format("Необработанная команда {0}", cmd.Cmd.ToString()));
            }

            return true;
        }
        void loadHistory() {
            IEnumerable<Gate> gates = UserConf.Instance.Gates();
            string tradername = this.tradername_;
            if (string.IsNullOrEmpty(tradername)) {
                tradername = "*";
            }
            tradername = Robot.Common.Helpers.Common.WildcardToRegex(tradername);
            Regex re = new Regex(tradername, RegexOptions.IgnoreCase);
            var needGates = gates.Where(g => re.IsMatch(g.name));
            if (needGates.Count() == 0) {
                throw new Exception(String.Format("Не найдено ни одного шлюза с именем {0}", tradername));
            }
            string stname = this.stname_;
            if (string.IsNullOrEmpty(stname)) {
                stname = "*";
            }

            StrategyFactory factory = new StrategyFactory();

            stname = Robot.Common.Helpers.Common.WildcardToRegex(stname);
            Regex stre = new Regex(stname, RegexOptions.IgnoreCase);
            foreach (Gate g in needGates) {
                var stlist = UserConf.Instance.GateStrategies(g.name)
                    .Where(st => stre.IsMatch(st.name));

                foreach (GateStrategy gst in stlist) {
                    Console.WriteLine("Загрузка истории для стратегии: {0}", gst.name);
                    Configuration conf = UserConf.Instance.StrategyConfiguration(g.name, gst.name);
                    StrategyPluginParameters section = (StrategyPluginParameters)conf.GetSection(
                        GlobalConf.StrategyPluginParameters);
                    IStrategyPlugin st = factory.createStrategy(section.parameters.ctor);
                    ISecurityDownload sd = (ISecurityDownload)st;
                    foreach (var s in sd.DownloadSecurityList(conf)) {
                        Console.Write("Security: {0}", s.Code);
                        robot_.FinamDownloaderCli.FinamLoadCommand(s.Args());
                    }
                }
            }
        }
        void showHelp() {
            string [] ignored = new string[]{"value__", "Unknown"};
            string format = "{0,-20} - {1}";
            foreach (FieldInfo fi in typeof(Command).GetFields()) {
                DescriptionAttribute[] da = (DescriptionAttribute[])fi.GetCustomAttributes(
                    typeof(DescriptionAttribute), false);

                if (ignored.Contains(fi.Name)) continue;
                
                string description = fi.Name;
                if (da != null && da.Length > 0) {
                    description = da[0].Description;
                }

                Console.WriteLine(String.Format(format, fi.Name, description));
            }
        }
        /// <summary>
        /// Команда
        /// </summary>
        Robot.Common.Cli.Command cmd_;
        /// <summary>
        /// Параметры команды
        /// </summary>
        string cmdparams_;
        /// <summary>
        /// Название стратегии для которой выполяется команда
        /// </summary>
        string stname_;
        /// <summary>
        /// Название шлЮза для которого выполняется команда
        /// </summary>
        string tradername_;
        /// <summary>
        /// Робот
        /// </summary>
        PioneerRobot robot_ = null;
        
    }
}  // namespace PioneerRobot.Cli