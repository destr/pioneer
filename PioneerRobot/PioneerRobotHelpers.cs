﻿using System;
using Robot.Common.Exchange.Rts;

namespace PioneerRobot {
    partial class PioneerRobot {
        /// <summary>
        /// Проверить завершила ли биржа работу
        /// </summary>
        /// <returns></returns>
        public bool exchangeEndWork() {
            // если запуск осуществляется в выходной день, уходить спать
            if (!WorkingCalendar.isTradingDate(DateTime.Now)) {
                return (true);
            }
            TimeSpan end = DateTime.Now.TimeOfDay - TimeSpan.FromMinutes(delay_stop);
            return end >= _exchangeEndTime;
            
        }  // exchangeEndWork
    }
}  // namespace PioneerRobot