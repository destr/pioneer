﻿using Robot.Common.Cli;

namespace PioneerRobot {
    partial class TraderManager : IDoCliCommand {
        public bool DoCliCommand(CommandDescription command) {
            return Helper.InvokeCommand(this, command);
        }  // DoCliCommand
        /// <summary>
        /// Запустить шлюз
        /// </summary>
        /// <param name="args">Параметры</param>
        public void StartGwCommand(string[] args) {
            start();
        }  // StartGwCommand
        /// <summary>
        /// Остановить шлюз
        /// </summary>
        /// <param name="args">Параметры</param>
        public void StopGwCommand(string[] args) {
            stop();
        }  // StopGwCommand

    }  // class TraderManager
}  // namespace PioneerRobot