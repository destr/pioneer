﻿using System.Collections.Generic;
using Robot.Sms;
namespace PioneerRobot {

    public sealed class RobotMessage : SmsMessage {
        public string Message;
        public override string ToText() {
            return Sender + ": " + Message;
        }
    }

    /// <summary>
    /// Параметры торгового шлюза
    /// </summary>
    public struct TraderParameters {
        /// <summary>
        /// Название
        /// </summary>
        public string name {get; internal set;}
        /// <summary>
        /// Путь к шлюзу
        /// </summary>
        public string path {get; internal set;}
        /// <summary>
        /// Логин
        /// </summary>
        public string login {get; internal set;}
        /// <summary>
        /// Пароль
        /// </summary>
        public string password {get; internal set;}
        /// <summary>
        /// Конфигурации стратегий, работающих на этом шлюзе
        /// </summary>
        public List<string> strategyconfigs {get; internal set;}
    }
}