﻿using System;
using System.Reflection;
using CommandLine;
using CommandLine.Text;

namespace PioneerRobot {
    /// <summary>
    /// Опции командной строки
    /// </summary>
    class Options {
        [Option('c', "configdir", HelpText = "Путь к корневому каталогу с конфигурацией")]
        public string ConfigDir { get; set; }

        [Option('w', "wakeuptime", HelpText = "Время запуска робота")]
        public string WakeupTime { get; set; }

        [Option('E', "exchangeendtime", HelpText = "Время завершения работы биржи")]
        public string ExchangeEndTime { get; set; }

        [Option('N', "noautostartst", HelpText = "Не запускать стратегию при запуске робота", DefaultValue=false)]
        public bool NoAutoStartSt { get; set; }

        [Option('L', "logdir", HelpText = "Альтернативный каталог с логами")]
        public string LogDir { get; set; }

        [Option('X', "noxmlrpc", HelpText = "Не запускать сервис xmlrpc", DefaultValue=false)]
        public bool NoXmlRpc { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }
        
        [HelpOption]
        public string GetUsage() {
            var help = new HelpText{
                Heading = new HeadingInfo("Робот \"Пионер\"",
                    Assembly.GetExecutingAssembly().GetName().Version.ToString()),
                AdditionalNewLineAfterOption = false,
                AddDashesToOption = true
            };

            if (this.LastParserState.Errors.Count > 0) {
                var errors = help.RenderParsingErrorsText(this, 2);
                if (!string.IsNullOrEmpty(errors)) {
                    help.AddPreOptionsLine(string.Concat(Environment.NewLine, "Error(s):"));
                    help.AddPostOptionsLine(errors);
                    return help;
                }
            }

            help.AddOptions(this);
            return help;
        }
    }
}
