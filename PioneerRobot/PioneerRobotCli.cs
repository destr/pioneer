﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using StockSharp.Logging;

using Robot.Common.Cli;
using Robot.Storage.Finam;
using Robot.Strategy.Cli;
using Robot.Logging;

namespace PioneerRobot {
    /// <summary>
    /// Реализация функций реализующих управление из командной строки
    /// </summary>
    partial class PioneerRobot {
        public IEnumerable<ICliStrategy> FindStrategy(string tradername, string stname) {
            List<ICliStrategy> result = new List<ICliStrategy>();
            foreach (TraderManager m in traderManagerByName(tradername)) {
                result.AddRange(m.StrategyManager.FindStrategy(stname));
            }
            return result.AsEnumerable();
        }
        /// <summary>
        /// Найти менеджер шлюза 
        /// </summary>
        /// <param name="tradername">Имя шлюза, может быть маской</param>
        /// <returns>Список шлюзов</returns>
        public IEnumerable<TraderManager> FindTraderManager(string tradername) {
            return traderManagerByName(tradername);
        }  // FindTraderManager
        /// <summary>
        /// Найти менеджер стратегий
        /// </summary>
        /// <param name="tradername">Имя шлюза</param>
        /// <returns>Список менеджеров стратегий</returns>
        public IEnumerable<StrategyManager> FindStrategyManager(string tradername) {
            List<StrategyManager> result = new List<StrategyManager>();
            foreach (TraderManager m in traderManagerByName(tradername)) {
                if (!m.IsRunning) {
                    RobotLogger.Instance.AddWarningLog("Шлюз {0} не запущен", m.Name);
                    continue;
                }
                result.Add(m.StrategyManager);
            }
            return result.AsEnumerable();
        }
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="command">Описание команды</param>
        public bool DoCliCommand(CommandDescription command) {
            return Helper.InvokeCommand(this, command);
        }  // DoCommand

        public FinamHistoryDownloaderCli FinamDownloaderCli {
            get {
                return _finamDownloaderCli;
            }
        }

        public void SmsdummyCommand(string[] args) {
            string value = "";
            if (args.Length > 0)
                value = args[0];
            value = value.ToLower();
            if (value == "on") {
                _smsEventsMerger.Dummy = true;
            } else if (value == "off") {
                _smsEventsMerger.Dummy = false;
            }
        }  // SmsdummyCommand

        /// <summary>
        /// Получить менеджер шлюза по имени. Если имя не задано и шлюз одни
        /// то возвращается он
        /// </summary>
        /// <param name="tradername">Название шлюза</param>
        /// <returns>Менеджер шлюза</returns>
        IEnumerable<TraderManager> traderManagerByName(string tradername) {
            if (string.IsNullOrEmpty(tradername) && _traders.Count > 1) {
                throw new Exception("Шлюз не один, но в команде шлюз не указан.");
            }
            if (string.IsNullOrEmpty(tradername)) {
                tradername = "*";
            }
            tradername = Robot.Common.Helpers.Common.WildcardToRegex(tradername);
            
            Regex re = new Regex(tradername, RegexOptions.IgnoreCase);
            var result = _traders.Where(m => re.IsMatch(m.Name));
            if (result.Count() == 0) {
                throw new Exception(String.Format("Не найдено ни одного шлюза с именем {0}", tradername));
            }
            return result;
        }  // traderManagerByName
       
    }  // class PioneerRobot
}  // namespace PioneerRobot