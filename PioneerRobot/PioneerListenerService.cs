﻿using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using System;
using System.Text;
using System.Diagnostics;
using CookComputing.XmlRpc;
using Robot.Logging;
using Robot.Strategy;
using StockSharp.Logging;

namespace PioneerRobot.XmlRpc {
    public class PioneerListenerService : XmlRpcListenerService {

        public PioneerListenerService(PioneerRobot robot)  {
            Robot = robot;
        }
        /// <summary>
        /// Робот
        /// </summary>
        public PioneerRobot Robot { get; internal set; }
        /// <summary>
        /// Получить список стратегий на роботе
        /// </summary>
        /// <returns>Список стратегий</returns>
        [XmlRpcMethod("GetStrategyList")]
        public IEnumerable<string> GetStrategyList() {
            RobotLogger.Instance.AddDebugLog("Call rpc method GetStrategyList");
            return new [] {"a", "b", "C"};
        }
        [XmlRpcMethod("GetStrategyStatus")]
        public string GetStrategyStatus(string tradername, string stname) {
            Console.WriteLine("GetStrategyStatus");
            IStrategyPlugin plugin = (IStrategyPlugin)Robot.FindStrategy(tradername, stname);
            if (plugin == null) {
                return String.Format("{0}:{1}:not found", tradername, stname);
            }
            return plugin.State().ToString();
        }
        /// <summary>
        /// Получить идентификатор робота
        /// </summary>
        /// <returns></returns>
        [XmlRpcMethod("GetRobotId")]
        public int GetRobotId() {
            return Process.GetCurrentProcess().Id;
        }
    }
}
