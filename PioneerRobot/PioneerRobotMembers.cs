﻿using System;
using System.Collections.Generic;
using Robot.Sms;
using Robot.Storage.Finam;
using Robot.Strategy;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace PioneerRobot {
    /// <summary>
    /// Члены
    /// </summary>
    public partial class PioneerRobot {
        /// <summary>
        /// Признак демо квика
        /// </summary>
        public static bool demo_quik = false;
        /// <summary>
        /// Не запускать автоматически стратегии при загрузке
        /// </summary>
        public static bool noautostartst = false;
        /// <summary>
        /// Время завершения работы биржи
        /// </summary>
        public static TimeSpan _exchangeEndTime = ExchangeBoard.Forts.WorkingTime.Times[2].Max;
        /// <summary>
        /// Время запуска стратегий
        /// </summary>
        public static TimeSpan _wakeupTime = new TimeSpan(9, 43, 0);
        /// <summary>
        /// Список активных шлюзов
        /// </summary>
        List<TraderManager> _traders = new List<TraderManager>(); 
        /// <summary>
        /// Фабрика стратегий
        /// </summary>
        StrategyFactory factory_ = new StrategyFactory();
        /// <summary>
        /// SMS клиент
        /// </summary>
        private SmsEventsMerger _smsEventsMerger;
        /// <summary>
        /// Флаг признака работы робота
        /// </summary>
        private bool work = true;
        /// <summary>
        /// Задержка перед остановкой стратегий относительно времени окончания 
        /// работы биржи в минутах.
        /// </summary>
        static int delay_stop = 1;
        /// <summary>
        /// Дата запуска
        /// </summary>
        DateTime    _startDate = DateTime.Now;

        /// <summary>
        /// Сервис ответов на xmlrpc запросы
        /// </summary>
        private XmlRpc.PioneerListenerService _listenerService = null;
        /// <summary>
        /// XmlRpcServer
        /// </summary>
        private XmlRpc.Server _xmlRpcServer = null;
        /// <summary>
        /// Опции командной строки
        /// </summary>
        internal static Options CmdOptions;

        public static ConsoleLogListener ConsoleListener;
        public static SmsLogRedirectorListener SmsLogRedirector;
        private static Func<LogMessage, bool> _ignoreDebug = (m) => { return !(m.Level == LogLevels.Debug); };
        private static Func<LogMessage, bool> _ignoreInfo = (m) => { return !(m.Level == LogLevels.Info); };

        /// <summary>
        /// Скачивание истоирческий данных с финам
        /// </summary>
        public FinamHistoryDownloaderCli _finamDownloaderCli = new FinamHistoryDownloaderCli();
        
    }
}  // namespace PioneerRobot