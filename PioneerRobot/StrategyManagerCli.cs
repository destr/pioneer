﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Robot.Common.Cli;
using Robot.Configuration;
using Robot.Logging;
using Robot.Strategy;
using Robot.Strategy.Cli;
using StockSharp.Algo.Strategies;
using StockSharp.Logging;

namespace PioneerRobot {
    
    /// <summary>
    /// Функции реализующие командный интерфейс
    /// </summary>
    partial class StrategyManager {
        public bool DoCliCommand(CommandDescription command) {
            return Helper.InvokeCommand(this, command);
        }  // DoCliCommand

        public IEnumerable<ICliStrategy> FindStrategy(string stname) {
            return strategyByName(stname).Cast<ICliStrategy>();
        }
        /// <summary>
        /// Отобразить стратегии и их состояние
        /// </summary>
        public void showStrategies() {
            foreach (Strategy s in plugin_list_) {
                try {
                    Console.WriteLine(((IStrategyPlugin)s).State());
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(e);
                }
            }
        }  // showStrategies

        /// <summary>
        /// Получения информации о стратегии
        /// </summary>
        /// <param name="stname"></param>
        /// <returns></returns>
        public string infost(string stname) {
            foreach (IStrategyPlugin p in strategyByName(stname)) {
                try {
                    return p.State().ToString();
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(e);
                }
            }
            return "";
        }  // infost
        /// <summary>
        /// Запустить отстановленную стратегию
        /// </summary>
        /// <param name="args">Аргументы</param>
        public void StartStCommand(string [] args) {
            if (args.Length == 0) {
                throw new ArgumentNullException("Не задано имя стратегии");
            }
            string stname = args[0];
            // проверим, может быть такая стратегия уже запущена
            if (plugin_list_.FirstOrDefault( s => s.Name == stname) != null) {
                throw new Exception(String.Format(
                    "Стратегия {0} уже запущена", stname));
            }
            // Перезагрузим конфигурацию стратегии
            UserConf.Instance.ReloadStrategyConfiguration(GateName, stname);
            IStrategyPlugin stplugin = createStrategyPlugin(stname);
            if (stplugin == null) {
                throw new Exception(String.Format(
                    "Неизвестная стратегия {0}", stname));
            }
            Strategy st = (Strategy)stplugin;
            st.Start();
            plugin_list_.Add(st);

        }  // StartStCommand
        /// <summary>
        /// Остановить стратегию. Имя стратегии может быть задано маской
        /// </summary>
        /// <param name="args">Аргументы</param>
        public void StopStCommand(string[] args) {
            string stname = "";
            if (args.Length != 0) {
                stname = args[0];
            }
            IList<Strategy> toRemove = new List<Strategy>();
            foreach (IStrategyPlugin p in strategyByName(stname)) {
                try {
                    stopStrategy(p);
                    toRemove.Add((Strategy)p);
                } catch (Exception e) {
                    RobotLogger.Instance.AddErrorLog(e);
                }
            }
            plugin_list_.RemoveAll(s => toRemove.Contains(s));
        }  // StopStCommand
        /// <summary>
        /// Загрузить/сохранить параметры стратегии не запуская её
        /// </summary>
        /// <param name="stname">Название стратегии</param>
        public void loadsaveStrategy(string stname) {
            if (strategyLoaded(stname)) {
                throw new Exception("Невозможно выполнить операцию load/save на работающей стратегии");
            }
            IStrategyPlugin stplugin = createStrategyPlugin(stname);
            // ставим признак того, что для стретгии рабочий день закончен
            //stplugin.manualWorkDayEnd();
            destroyStrategy(stplugin);
        }  // loadsaveStrategy
        /// <summary>
        /// Поиск стратегии по имени. Если именя стратегии не указано, но статегия
        /// всего одна, то возвращается она
        /// </summary>
        /// <param name="stname">Название стратегии</param>
        /// <returns>Интерфейс плагина стратегии</returns>
        IEnumerable<IStrategyPlugin> strategyByName(string stname) {
            if (string.IsNullOrEmpty(stname) && plugin_list_.Count > 1) {
                throw new Exception("Шлюз не один, но в команде шлюз не указан.");
            }
            if (string.IsNullOrEmpty(stname)) {
                stname = "*";
            }
            stname = Robot.Common.Helpers.Common.WildcardToRegex(stname);
            var list = plugin_list_.Where(p => Regex.IsMatch(p.Name, stname));
            if (list.Count() == 0) {
                throw new ArgumentException(
                    String.Format("Не найдено ни одной стратегии с именем {0}", stname));
            }

            return list.Cast<IStrategyPlugin>();
        }  // stratetgyByName
        bool strategyLoaded(string stname) {
            // проверим, может быть такая стратегия уже запущена
            return (plugin_list_.FirstOrDefault(s => s.Name == stname) != null);
        }
        public void invokeStop() {

            //IStrategyPlugin stplugin = strategyByName("");
            //stplugin.invokeStop();
        }
        
    }  // class StrategyManager
} // namespace PioneerRobot