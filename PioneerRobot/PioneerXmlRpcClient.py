#!/usr/bin/python
# coding: utf-8

import sys
import smsc_api
import configparser
import time
import datetime
import dateutil.parser
from argparse import ArgumentParser
from urllib.parse import urlparse, quote, urlunparse
from xmlrpc.client import ServerProxy, Error

class ClientInfo:
    def __init__(self, name, client, strategies, phone):
        self.name = name
        self.client = client
        self.strategies = strategies

        # Признак информирования об ошибке
        self.error = False
        self.smsc = smsc_api.SMSC()
        self.phone = phone

    def check(self):
        for st in self.strategies:
            try:
                status = self.client.GetStrategyStatus(self.name, st)
                self.check_status(status)                
                self.error = False
            except Exception as e:
                if not self.error:
                    self.error = True
                    self.send_sms()
                print(e)
    
    def send_sms(self):
        print ("send_sms")
        pass

    def check_status(self, status):
        print(status)
        pass


class PioneerXmlRpcClient:
    """
    Работа с xmlrpc API робота
    """
    def __init__(self):
        self.opts = None
        self.config = None
        self.clients = list()
        self.smsc = smsc_api.SMSC()

    def main(self):
        self.parse_options()
        
        robots = self.config['global']['robots'].split(',')
        for robot in robots:
            # items возвращают массив кортежей, которые конвертируем в словарь
            robot_cfg = dict(val for val in self.config.items(robot))
            if 'verbose' is not robot_cfg:
                robot_cfg['verbose'] = None
            if 'strategies' not in robot_cfg:
                print('{}: Not set strategies list'.format(robot))
                continue
            
            st = robot_cfg['strategies'].split(',')            
            
            print('Create connection to {} with strategies {}'.format(robot_cfg['url'], st))
            try:
                client = self.__create_client(robot_cfg)
                robot_id = client.GetRobotId()
                self.config[robot]['robotid'] = str(robot_id)
            except:
                # ошибка создания подключения. Надо сообщить
                self.send_sms(self.config[robot])
                

        with open(self.opts.config, 'w') as cfg:
            self.config.write(cfg)


    def send_sms(self, cfg):
        dt = None
        new_robot = False
        if 'lasterror' in cfg:
            dt = dateutil.parser.parse(cfg['lasterror'])
        if 'lastrobotid' in cfg:
            new_robot = cfg['lastrobotid'] != cfg['robotid']
        #print (str(dt))
        if not dt or dt.date() != datetime.datetime.now().date() or new_robot:
            # дата вчерашняя, отправляем снова
            try:
                print("send sms")
                m = self.smsc.send_sms(cfg['phone'], 'Robot {} dead'.format(cfg['name']))
                print(m)
                cfg['lasterror'] = str(datetime.datetime.now())
                cfg['lastrobotid'] = cfg['robotid']
            except:
                # тут уже отправит крон письмо
                print('Send sms error')
        else:
            print("No send sms")
            
    def __create_client(self, cfg):
        url = self.__format_url(cfg)
        client = ServerProxy(url, verbose=cfg['verbose'])
        try :
            print(client.system.listMethods())
            return client
        except Exception as e:
            print("Create error client for robot %s: %s" % (cfg['name'], e), file=sys.stderr)
            raise

    def __format_url(self, cfg):
        if 'username' in cfg and 'password' in cfg:
            url = list(urlparse(cfg['url']))
            url[1] = "{}:{}@{}".format(cfg['username'],  quote(cfg['password']), url[1])
            return urlunparse(tuple(url))

        return cfg['url']

    def parse_options(self):
        """
        Разбор опций
        """
        parser = ArgumentParser()
        parser.add_argument('-u,--url', dest='url', action='store', help='Xml-rpc URL')
        parser.add_argument('-V,--verbose', dest='verbose', action='store',
                            type=type(int), default=None, help='Verbose level')
        parser.add_argument('-c,--config', dest='config', action='store', help='Path to configuration')
        self.opts = parser.parse_args()

        if self.opts.url and self.opts.url[-1] != '/':
            self.opts.url += '/'

        # загрузка конфигурации
        print('Loading configuration from %s' % self.opts.config)
        self.config = configparser.ConfigParser()
        self.config.read(self.opts.config)

        smsc_api.SMSC_LOGIN = self.config['smsc']['login']
        smsc_api.SMSC_PASSWORD = self.config['smsc']['password']
## 
        if 'debug' in self.config['smsc']:
            smsc_api.SMSC_DEBUG = True

if __name__ == "__main__":
    try:
        client = PioneerXmlRpcClient()
        client.main()
    except Exception as e:
        print(e)





