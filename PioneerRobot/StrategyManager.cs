﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Robot.Common.Cli;
using Robot.Common.Helpers;
using Robot.Configuration;
using Robot.Logging;
using Robot.Sms;
using Robot.Strategy;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Strategies;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace PioneerRobot {
    
    using StrategyPluginList = List<Strategy>;
     
    /// <summary>
    /// Менеджер стратегий
    /// </summary>
    public partial class StrategyManager : ISmsable, IDisposable, IDoCliCommand {
        public event Action<SmsMessage> Sms;
        /// <summary>
        /// Наблюдение за позициями стратегий
        /// </summary>
        public StrategyWatchdog StrategyWatchdog { get; set; }

        /// <summary>
        /// Название шлюза
        /// </summary>
        public string GateName { get; set; }
        public StrategyManager (StrategyFactory factory, CandleManager manager, ITrader trader) {
            _factory = factory;
            _candleManager = manager;
            _trader = trader;
        }   // Ctor
        /// <summary>
        /// Инициализация менеджера стратегий
        /// </summary>
        public void Init() {
            StrategyWatchdog.Sms += Sms;
            StrategyWatchdog.Start();
        }

        public void Dispose() {
            StrategyWatchdog.Stop();
        }  // Dispose

        /// <summary>
        /// Запустить стратегии
        /// </summary>
        public void startAllStrategies() {
            
            RobotLogger.Instance.AddInfoLog("Запуск стратегий...");
            failedStrategy.Clear();
            foreach (GateStrategy gateSt in UserConf.Instance.GateStrategies(GateName)) {
                                
                try {
                    IStrategyPlugin stplugin = createStrategyPlugin(gateSt.name);
                    startStrategy(stplugin);

                } catch (Exception e) {
                    failedStrategy.Add(gateSt.name);
                    RobotLogger.Instance.AddErrorLog(e);
                }
            }

        }  // startAllStrategies
        /// <summary>
        /// Остановить все стратегии
        /// </summary>
        public void stopAllStrategies() {
            foreach (IStrategyPlugin stplugin in plugin_list_) {
                stopStrategy(stplugin);
            }
            plugin_list_.Clear();
        }  // stopAllStrategies
        /// <summary>
        /// Создать плагин стратегии
        /// </summary>
        /// <param name="configName">Название конфигурации описывающей плагин</param>
        /// <returns>Плагин или null если не удалось создать</returns>
        IStrategyPlugin createStrategyPlugin(string configName) {
            if (!UserConf.Instance.IsConfigLoaded(GateName, configName)) {
                UserConf.Instance.LoadStrategyConfiguration(GateName, configName);
            }
            // проверяем конфигурации стартегии в загружнных
            Configuration config = UserConf.Instance.StrategyConfiguration(
                GateName, configName);
      
            RobotLogger.Instance.AddInfoLog(
                   "Создание стратегии {0} ...", configName);
            
            StrategyPluginParameters section = (StrategyPluginParameters)config.GetSection(
                GlobalConf.StrategyPluginParameters);
            
            string ctor = section.parameters.ctor;
            Strategy st = null;
            IStrategyPlugin stplugin = null;
            try {
                stplugin = _factory.createStrategy(ctor);
            }
            catch (Exception e) {
                string err = String.Format("Ошибка при создании плагина стратегии {0} " +
                    " : {1}", configName, e);
                RobotLogger.Instance.AddErrorLog(err);
                throw new Exception(err);
            }
            try {
                st = (Strategy)stplugin;
                st.LogLevel = LogLevels.Debug;
                st.Trader = _trader;
                st.Name = configName;

                stplugin.CandleManager = _candleManager;
                stplugin.RegisterAsLogSource(PioneerRobot.ConsoleListener);

                
                stplugin.Sms += Sms;

                initStrategyPlugin(stplugin);

                RobotLogger.Instance.AddInfoLog(
                    "Стратегия {0} создана", configName);
                return stplugin;
            } catch (Exception e) {
                string err = String.Format("Ошибка при создании стратегии {0}: {1}",
                    configName, e);
                RobotLogger.Instance.AddErrorLog(err);
                stplugin.UnregisterAsLogSource();
                throw new Exception(err);
            }
        }

        /// <summary>
        /// Инициализировать созданную стратегию
        /// </summary>
        /// <param name="stplugin">Плагин стратегии</param>
        void initStrategyPlugin(IStrategyPlugin stplugin) {
            // загружаем глобальные настройки.
            Strategy st = (Strategy)stplugin;
            Configuration config = UserConf.Instance.StrategyConfiguration(
                GateName, st.Name);
            stplugin.Load(config);
        }  // initStrategyPlugin
        /// <summary>
        /// Запустить стратегию
        /// </summary>
        /// <param name="stplugin">Плагин стратегии</param>
        void startStrategy(IStrategyPlugin stplugin) {
            Strategy st = (Strategy)stplugin;
            RobotLogger.Instance.AddInfoLog(
                "Запуск стратегии {0}...", st.Name);
            st.Start();
            
            var wrapper = new StrategyEventErrorWrapper() { Name = st.Name };
            st.Error += new Action<Exception>(wrapper.wrap);
            wrapper.Error += new Action<string, Exception>(action_strategyErrorWrapper);
            plugin_list_.Add((Strategy)stplugin);
            StrategyWatchdog.Add(stplugin);
        }
        /// <summary>
        /// Обработчик события ошибки в стратегии
        /// </summary>
        /// <param name="strategyname"></param>
        /// <param name="error"></param>
        void action_strategyErrorWrapper(string strategyname, Exception error) {
            RobotLogger.Instance.AddErrorLog(error);
            RobotLogger.Instance.AddErrorLog(
                "Ошибка при выполнении стратегии {0}: {1}",
                strategyname, error.Message);
            StopStCommand(new string [] {strategyname});
            Sms.Invoke(new SmsMessage() {
                Sender = strategyname,
                Text = "runtime error"
            });
        }  // action_strategyErrorWrapper

        /// <summary>
        /// Остановить стратегию
        /// </summary>
        /// <param name="st">Название стратегии</param>
        void stopStrategy(IStrategyPlugin stplugin) {
            Strategy st = (Strategy)stplugin;
            RobotLogger.Instance.AddInfoLog(
                   "Останов стратегии {0}...", st.Name);

            st.Stop();
            destroyStrategy(stplugin);
            StrategyWatchdog.Remove(stplugin);
            /// если стратегия ещё не закончилась, то не сохраняем отчёт
            if (!stplugin.WorkDayEnd()) return;
            if (st.MyTrades.Count() == 0) {
                RobotLogger.Instance.AddDebugLog(
                    "Стратегия не произвела сделок, отчёт не сохраняется");
                return;
            }
            
        }  // stopStrategy
        /// <summary>
        /// Удалить плагин стратегии
        /// </summary>
        /// <param name="stplugin"></param>
        void destroyStrategy(IStrategyPlugin stplugin) {
            stplugin.Save();
            stplugin.UnregisterAsLogSource();
        }  // destroyStrategy
        
        /// <summary>
        /// Формирование имени отчёта стратегии
        /// </summary>
        /// <param name="stname">Название стратегии</param>
        /// <returns>Полный путь к отчёту</returns>
        string reportFileName(string stname) {
            string result = GlobalConf.ReportDir(stname);
            result = Path.Combine(result, DateTime.Now.ToString("dd-MM-yyyy HH-mm"));

            return result;
        }  // reportFileName

       
        /// <summary>
        /// Менеджер свечей
        /// </summary>
        private CandleManager       _candleManager;
        /// Фабрика стратегий
        /// </summary>
        private StrategyFactory     _factory;
        /// <summary>
        /// Шлюз
        /// </summary>
        private ITrader             _trader;
        /// <summary>
        /// Список плагинов
        /// </summary>
        private StrategyPluginList  plugin_list_ =  new StrategyPluginList();
        
        
        /// <summary>
        /// Список стратегий запуск которых завершился ошибкой
        /// </summary>
        public List<string> failedStrategy = new List<string>();
    }
}  // namespace PioneerRobot