﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="5517d62f-22e6-4ca4-a865-0f8c12eadcd6" namespace="Robot.Configuration" xmlSchemaNamespace="urn:Robot.Configuration" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
    <enumeratedType name="SendAction" namespace="Robot.Configuration">
      <literals>
        <enumerationLiteral name="Always" documentation="Отправлять смс всегда" />
        <enumerationLiteral name="WaitFailure" documentation="Дожидаться неуспешного объединения sms" />
        <enumerationLiteral name="WaitSuccess" documentation="Дожидаться успешного объединения" />
      </literals>
    </enumeratedType>
    <externalType name="LogLevels" namespace="StockSharp.Logging" />
  </typeDefinitions>
  <configurationElements>
    <configurationElementCollection name="Gates" xmlItemName="gate" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Gate" />
      </itemType>
    </configurationElementCollection>
    <configurationSection name="Robot" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="robot">
      <elementProperties>
        <elementProperty name="gates" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="gates" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Gates" />
          </type>
        </elementProperty>
        <elementProperty name="main" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="main" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Main" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationSection name="SmsGate" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="smsgate">
      <elementProperties>
        <elementProperty name="smsc" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="smsc" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Smsc" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="Phone">
      <attributeProperties>
        <attributeProperty name="phone" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="phone" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="GateStrategy">
      <attributeProperties>
        <attributeProperty name="name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="true">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationSection name="StrategyPluginParameters" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="StrategyPluginParameters">
      <elementProperties>
        <elementProperty name="parameters" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="parameters" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/PluginParameters" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="PluginParameters">
      <attributeProperties>
        <attributeProperty name="ctor" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ctor" isReadOnly="true" documentation="Конструктор плагина">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationSection name="SmsMerger" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="smsmerger">
      <elementProperties>
        <elementProperty name="MergeGroups" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="mergeGroups" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/MergeGroups" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElementCollection name="MergeGroups" xmlItemName="mergeGroup" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/MergeGroup" />
      </itemType>
    </configurationElementCollection>
    <configurationElementCollection name="MergeGroup" xmlItemName="sender" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <attributeProperties>
        <attributeProperty name="Name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="Name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="WaitSeconds" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="WaitSeconds" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="SendAction" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SendAction" isReadOnly="false">
          <type>
            <enumeratedTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/SendAction" />
          </type>
        </attributeProperty>
        <attributeProperty name="SmsMerger" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SmsMerger" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <itemType>
        <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Sender" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="Sender">
      <attributeProperties>
        <attributeProperty name="Name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElementCollection name="Smsc" xmlItemName="phone" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <attributeProperties>
        <attributeProperty name="login" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="login" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="password" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="password" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="dummy" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="dummy" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Boolean" />
          </type>
        </attributeProperty>
        <attributeProperty name="smsalive" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="smsalive" isReadOnly="false" documentation="Время жизни смс в формате hh:mm:ss">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/TimeSpan" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <itemType>
        <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Phone" />
      </itemType>
    </configurationElementCollection>
    <configurationElementCollection name="Gate" xmlItemName="strategy" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <attributeProperties>
        <attributeProperty name="login" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="login" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="password" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="password" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="path" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="path" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="ConnectTimeoutSeconds" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ConnectTimeoutSeconds" isReadOnly="false" documentation="Timeout ожидания в секундах подключения quik к серверу">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="WatchIntervalMin" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="WatchIntervalMin" isReadOnly="false" documentation="Интервал проверки (в минутах) состояние стратегии ">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <itemType>
        <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/GateStrategy" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="Main">
      <attributeProperties>
        <attributeProperty name="CandlesKeepDays" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="CandlesKeepDays" isReadOnly="false" documentation="Хранить свечи в менеджере свечей указаное количество дней">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="TradesKeepTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="TradesKeepTime" isReadOnly="false" documentation="Время хранения тиковых сделок в памяти. По-умолчанию равно 2-ум дням. Если значение установлено в Zero, то сделки не будут удаляться.">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="XmlRpcPort" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="XmlRpcPort" isReadOnly="false" documentation="Порт на котором сервер слушает запросы" defaultValue="8000">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="XmlRpcNet" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="XmlRpcNet" isReadOnly="false" documentation="Сеть в которой слушать xml-rcp запросы" defaultValue="&quot;+&quot;">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="XmlRpcAccessToken" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="XmlRpcAccessToken" isReadOnly="false" documentation="Ключ для вызова методов">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <elementProperties>
        <elementProperty name="IgnoreConsoleLogLevels" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="IgnoreConsoleLogLevels" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/IgnoredLogLevels" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationElement>
    <configurationElementCollection name="IgnoredLogLevels" xmlItemName="level" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/IgnoreLevel" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="IgnoreLevel">
      <attributeProperties>
        <attributeProperty name="name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/5517d62f-22e6-4ca4-a865-0f8c12eadcd6/LogLevels" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>