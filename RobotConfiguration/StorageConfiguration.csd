﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="76d4ebe6-3f1b-4e16-89ed-b9bd83a23052" namespace="Robot.Configuration" xmlSchemaNamespace="urn:Robot.Configuration" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="Storage" documentation="Конфигурация хранилища инструментов" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="storage">
      <elementProperties>
        <elementProperty name="SecurityAliases" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="SecurityAliases" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/76d4ebe6-3f1b-4e16-89ed-b9bd83a23052/Aliases" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElementCollection name="Aliases" documentation="Список алиасов" xmlItemName="Aliases" codeGenOptions="Indexer, AddMethod, RemoveMethod, GetItemMethods">
      <itemType>
        <configurationElementMoniker name="/76d4ebe6-3f1b-4e16-89ed-b9bd83a23052/AliasValue" />
      </itemType>
    </configurationElementCollection>
    <configurationElement name="AliasValue">
      <attributeProperties>
        <attributeProperty name="name" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="name" isReadOnly="false" documentation="Название алиаса">
          <type>
            <externalTypeMoniker name="/76d4ebe6-3f1b-4e16-89ed-b9bd83a23052/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="value" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="value" isReadOnly="false" documentation="Значение">
          <type>
            <externalTypeMoniker name="/76d4ebe6-3f1b-4e16-89ed-b9bd83a23052/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>