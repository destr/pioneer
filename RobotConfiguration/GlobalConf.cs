﻿using System;
using System.IO;

namespace Robot.Configuration {
    /// <summary>
    /// Глобальные конфигурационные параметры
    /// </summary>
    public static class GlobalConf {
        /// <summary>
        /// Формат имени лог-файла
        /// </summary>
        static public readonly string LogFilenameFormat = "dd-MM-yyyy";
        /// <summary>
        /// Максимальная длина имени стратегии
        /// </summary>
        static public readonly int MaxStrategyNameLen   = 18;
        /// <summary>
        /// Подкаталог с конфигурацией стратегий
        /// </summary>
        static public readonly string StrategiesSubDir  = "Strategies";
        /// <summary>
        /// Подкаталог логов
        /// </summary>
        static public readonly string LogsSubDir = "Logs";
        /// <summary>
        /// Подкаталог логов стратегий
        /// </summary>
        static public readonly string StrategyLogsSubDir = "Strategies";
        /// <summary>
        /// Подкаталог хранилища данных робота
        /// </summary>
        static public readonly string StorageSubDir = "Storage";
        /// <summary>
        /// Подкаталог отчётов
        /// </summary>
        static public readonly string ReportsSubDir = "Reports";
        /// <summary>
        /// Подкаталог данных с финам
        /// </summary>
        static public readonly string FinamSubDir = "Finam";
        /// <summary>
        /// Название конфигурационного файла робота
        /// </summary>
        static public readonly string DefaultConfigName = "robot.config";
        
        /// <summary>
        /// Название конфигурационного файла календаря
        /// </summary>
        static public readonly string DefaultRtsCalendarConfigName = "rtscalendar.config";

        /// <summary>
        /// Название конфигурационного файла хранилища
        /// </summary>
        static public readonly string DefaultStorageConfigName = "storage.config";
        
        /// <summary>
        /// Название робота
        /// </summary>
        static public readonly string RobotName = "PioneerRobot";
        static public readonly string StrategyPluginParameters = "StrategyPluginParameters";
        static public readonly string ConfigSaveAsSuffix = ".new";
        /// <summary>
        /// Корневой каталог конфигурации
        /// </summary>
        static public string ConfigRootDir {
            get { return _configRootDir; }
            set { _configRootDir = value; LogRootDir = value; }
        }
        static private string _configRootDir = ".";
        static public string LogRootDir = null;
        static GlobalConf() {
            ConfigRootDir =
                Path.Combine(Environment.GetFolderPath(
                    Environment.SpecialFolder.LocalApplicationData),
                    RobotName);
        }
        /// <summary>
        /// Получить путь к глобальному конфигурационному файлу
        /// </summary>
        /// <returns>Путь к файлу</returns>
        static string ConfigPath {
            get {
                return Path.Combine(ConfigRootDir, DefaultConfigName);
            }
        }
        /// <summary>
        /// Получить путь к каталогу сохранения отчётов
        /// </summary>
        /// <param name="strategyName">Название стратегии</param>
        /// <returns>Путь к каталогу</returns>
        static public string ReportDir(string strategyName) {
            string result = Path.Combine(ConfigRootDir,
                ReportsSubDir, strategyName);
            if (!Directory.Exists(result)) {
                Directory.CreateDirectory(result);
            }
            return result;
        }
        /// <summary>
        /// Путь к каталогу данных финам
        /// </summary>
        static public string FinamDir {
            get {
                string result = Path.Combine(ConfigRootDir, FinamSubDir);
                if (!Directory.Exists(result)) {
                    Directory.CreateDirectory(result);
                }
                return result;
            }
        }
        /// <summary>
        /// Путь к каталогу хранилищу данных
        /// </summary>
        static public string StorageDir {
            get {
                string result = Path.Combine(ConfigRootDir, StorageSubDir);
                if (!Directory.Exists(result)) {
                    Directory.CreateDirectory(result);
                }
                return result;
            }
        }
        /// <summary>
        /// Получить путь к конфигурационному файлу робота
        /// </summary>
        static public string RobotConfigurationPath {
            get {
                return Path.Combine(ConfigRootDir, DefaultConfigName);
            }
        }
        /// <summary>
        /// Получить путь к конфигурационному файлу календаря
        /// </summary>
        static public string RtsCalendarConfigurationPath {
            get {
                return Path.Combine(ConfigRootDir, DefaultRtsCalendarConfigName);
            }
        }

        /// <summary>
        /// Получить путь конфигурационному файлу хранилища
        /// </summary>
        static public string StorageConfigurationPath {
            get {
                return Path.Combine(ConfigRootDir, DefaultStorageConfigName);
            }
        }
        
        /// <summary>
        /// Получение имени лог-файла для записи
        /// </summary>
        /// <returns>Путь к лог файлу</returns>
        public static string LogFileName() {
            string logdir = Path.Combine(LogRootDir, LogsSubDir);
            if (!Directory.Exists(logdir)) {
                Directory.CreateDirectory(logdir);
            }

            string path = String.Format("{0}.log", DateTime.Now.ToString(GlobalConf.LogFilenameFormat));
            return Path.Combine(logdir, path);
        }  // logFileName
        /// <summary>
        /// Получение подкаталога сохранения логов стратегии
        /// </summary>
        /// <returns></returns>
        public static string StrategyLogDir() {
            string path = Path.Combine(LogRootDir, LogsSubDir, StrategyLogsSubDir);
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
            }
            return path;
        }  // strategyLogDir

        /// <summary>
        /// Получить путь к каталогу с конфигурациями стратегий
        /// </summary>
        /// <returns>Путь относительно рабочего каталога</returns>
        public static string StrategiesConfigurationDirPath() {
            return Path.Combine(ConfigRootDir, StrategiesSubDir);
        }
    }
}  // namespace RobotCommon.Config