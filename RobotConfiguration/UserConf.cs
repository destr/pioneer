using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Robot.Logging;
using StockSharp.Logging;

namespace Robot.Configuration {
    public class UserConf {
        /// <summary>
        /// Загрузка конфигурации стратегии
        /// <param name="gateName">Название шлюза</param>
        /// <param name="gateStrategyName">Название стратегии</param>
        /// </summary>
        public void LoadStrategyConfiguration(string gateName, string gateStrategyName) {
            string configKey = String.Format(_strategyConfigKeyFormat, gateName, gateStrategyName);
            if (_configMap.ContainsKey(configKey)) {
                RobotLogger.Instance.AddDebugLog(
                    "Конфигурация стратегии {0} в шлюзе {1} уже загружена", gateStrategyName, gateName);
                return;
            }
            string fileName = Path.Combine(GlobalConf.StrategiesConfigurationDirPath(),
                gateStrategyName);
            fileName += ".config";
            // проверяем наличие конфликтов
            string confictFileName = fileName + GlobalConf.ConfigSaveAsSuffix;
            if (File.Exists(confictFileName)) {
                RobotLogger.Instance.AddWarningLog(
                    "Найдена конфигурация {0}. Объедините конфигурации или удалите указаную", confictFileName);
            }
            try {
                internalLoad(fileName, configKey);
            } catch (Exception e) {
                RobotLogger.Instance.AddWarningLog(e.Message);
                throw;
            }
        }
        /// <summary>
        /// Перезагрузить конфигурацию стратегии
        /// </summary>
        /// <param name="gateName">Название шлюза</param>
        /// <param name="strategyName">Название стратегии</param>
        public void ReloadStrategyConfiguration(string gateName, string strategyName) {
            string key = configurationKey(gateName, strategyName);
            if (!_configMap.ContainsKey(key)) {
                // нет конфигурации и не надо ничего перезагружать
                return;
            }
            _configMap.Remove(key);
            LoadStrategyConfiguration(gateName, strategyName);
        }
        private StrategyPluginParameters StrategyGateParameters(string gateName, string strategyName) {
            System.Configuration.Configuration config = StrategyConfiguration(gateName, strategyName);
            return (StrategyPluginParameters)config.GetSection(GlobalConf.StrategyPluginParameters);
        }
        /// <summary>
        /// Загрузить конфигурацию робота
        /// </summary>
        /// <param name="filename">Путь к конфигурации</param>
        public void LoadRobotConfiguration() {
            internalLoad(GlobalConf.RobotConfigurationPath, _robotConfigAlias);
        }
        public Main robotMain() {
            return robotSection().main;
        }
        public void Load() {
            LoadRobotConfiguration();
            LoadRtsCalendar();
            LoadStorageConfiguration();
        }
        /// <summary>
        /// Загрузить конфигурацию календаря rts
        /// </summary>
        public void LoadRtsCalendar() {
            internalLoad(GlobalConf.RtsCalendarConfigurationPath, _rtsCalendarConfigAlias);
        }
        
        /// <summary>
        /// Загрузить конфигурацию хранилища
        /// </summary>
        public void LoadStorageConfiguration() {
            internalLoad(GlobalConf.StorageConfigurationPath, _storageConfigAlias);
        }  // LoadStorageConfiguration

        /// <summary>
        /// Загрузить конфигурации стратегий шлюза
        /// </summary>
        /// <param name="g">Шлюз</param>
        public void LoadStrategiesConfigurations(Gate g) {
            IEnumerable<GateStrategy> strategies = this.strategies(g);
            foreach (GateStrategy st in strategies) {
                LoadStrategyConfiguration(g.name, st.name);
            }
        }  // LoadStrategiesConfigurations


        /// <summary>
        /// Получить список шлюзов и их конфигурацию
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Gate> Gates() {
            Robot robotSection = this.robotSection();
            IList<Gate> values = new List<Gate>();

            for (int i = 0; i < robotSection.gates.Count; ++i) {
                values.Add(robotSection.gates.GetItemAt(i));
            }

            return values.AsEnumerable<Gate>();
        }  // gates
        /// <summary>
        /// Получить имена всех конфигураций стратегий шлюза
        /// </summary>
        /// <param name="gateName">Название шлюза</param>
        /// <returns>Список имён</returns>
        public IEnumerable<GateStrategy> GateStrategies(string gateName) {
            return strategies(gateByName(gateName));
        }  // Strategies
        /// <summary>
        /// Проверка загруженной конфигурации
        /// </summary>
        /// <param name="gateName">Название шлюза</param>
        /// <param name="strategyConfigName">Название конфигурации стратегии</param>
        /// <returns></returns>
        public bool IsConfigLoaded(string gateName, string strategyConfigName) {
            return _configMap.ContainsKey(configurationKey(gateName, strategyConfigName));
        }
        /// <summary>
        /// Получить конфигурацию стратегии
        /// </summary>
        /// <param name="gateName">Название шлюза на котором работает стратегия</param>
        /// <param name="strategyConfigName">Название конфигурации стратегии</param>
        /// <returns></returns>
        public System.Configuration.Configuration StrategyConfiguration(string gateName,
            string strategyConfigName) {
            return _configMap[configurationKey(gateName, strategyConfigName)];
        }  // StrategyConfiguration

        /// <summary>
        /// Получить конфигурацию календаря
        /// </summary>
        /// <returns></returns>
        public RtsCalendar RtsCalendarConfiguration() {
            System.Configuration.Configuration config = _configMap[_rtsCalendarConfigAlias];
            if (config == null) {
                throw new Exception(
                    String.Format("Календарь rts пуст"));
            }

            return config.GetSection("rtsCalendar") as RtsCalendar;
        }

        /// <summary>
        /// Получить конфигурацию хранилища
        /// </summary>
        /// <returns>Конфигурация хранилища</returns>
        public Storage StorageConfiguration() {
            System.Configuration.Configuration config = _configMap[_storageConfigAlias];
            if (config == null) {
                throw new Exception("Конфигурация хранилища пуста");
            }

            return config.GetSection("storage") as Storage;
        }  // StorageConfiguration

        /// <summary>
        /// Получить конфигурацию sms шлюза
        /// </summary>
        /// <returns></returns>
        public Smsc SmscConfiguration() {
            System.Configuration.Configuration config = robotConfig();
            SmsGate smsSection = (SmsGate)config.GetSection("smsgate");
            return smsSection.smsc;
        }  // SmscConfiguration
        /// <summary>
        /// Получить секцию конфигурации смс-объединителя
        /// </summary>
        /// <returns></returns>
        public SmsMerger MergeGroupsConfiguration() {
            System.Configuration.Configuration config = robotConfig();
            return (SmsMerger)config.GetSection("smsmerger");
        }
        /// <summary>
        /// Получить список конфигураций стратегий шлюза
        /// </summary>
        /// <param name="g">Шлюз</param>
        /// <returns>Список конфигураций стратегии</returns>
        public IEnumerable<GateStrategy> strategies(Gate g) {
            IList<GateStrategy> values = new List<GateStrategy>();
            for (int i = 0; i < g.Count; ++i) {
                values.Add(g.GetItemAt(i));
            }

            return values.AsEnumerable<GateStrategy>();
        }
        /// <summary>
        /// Сформировать конфигурационный ключ стратегии
        /// </summary>
        /// <param name="gateName">Название шлюза на котром работает стратегия</param>
        /// <param name="strategyConfigName">Название конфигурации стратегии</param>
        /// <returns></returns>
        private string configurationKey(string gateName, string strategyConfigName) {
            return String.Format(_strategyConfigKeyFormat, gateName, strategyConfigName);
        }  // configurationKey


        /// <summary>
        /// Реализация загрузки конфигурационного файла
        /// </summary>
        /// <param name="filename">Конфигурационный файл</param>
        /// <param name="alias">Имя конфигурации</param>
        private void internalLoad(string filename, string alias) {
            if (_configMap.ContainsKey(alias)) {
                throw new Exception(String.Format("Configuration with alias {0} already exist", alias));
            }
            if (!File.Exists(filename)) {
                throw new Exception(String.Format("Файл конфигурации {0} не существует", filename));
            }

            ExeConfigurationFileMap confFileMap = new ExeConfigurationFileMap();
            confFileMap.ExeConfigFilename = filename;


            System.Configuration.Configuration config =
                ConfigurationManager.OpenMappedExeConfiguration(confFileMap, ConfigurationUserLevel.None);
            _configMap.Add(alias, config);
        }
        /// <summary>
        /// Получить конфигурационную секцию робота
        /// </summary>
        /// <returns></returns>
        private Robot robotSection() {
            System.Configuration.Configuration config = robotConfig();
            Robot robotSection = (Robot)config.GetSection("robot");
            if (robotSection == null) {
                throw new Exception(
                    String.Format("Не удалось получить конфигурационную секцию robot из файла {0}",
                    config.FilePath));
            }
            return robotSection;
        }
        private System.Configuration.Configuration robotConfig() {
            System.Configuration.Configuration config;
            if (!_configMap.TryGetValue(_robotConfigAlias, out config)) {
                throw new Exception("Robot configuration not load");
            }
            return config;
        }


        private Gate gateByName(string gateName) {
            return robotSection().gates.GetItemByKey(gateName);
        }
        private IDictionary<String, System.Configuration.Configuration> _configMap =
            new Dictionary<String, System.Configuration.Configuration>();

        private readonly string _robotConfigAlias = "__robot__";
        private readonly string _rtsCalendarConfigAlias = "__rts_calendar__";
        private readonly string _storageConfigAlias = "__storage__";
        private readonly string _strategyConfigKeyFormat = "gate::{0}/{1}";

        private UserConf() {}
        private static volatile UserConf _instance;
        private static object _syncObject = new Object();

        public static UserConf Instance {
            get {
                if (_instance == null) {
                    lock (_syncObject) {
                        if (_instance == null) {
                            _instance = new UserConf();
                        }
                    }

                }
                return _instance;
            }
        }

    }
}