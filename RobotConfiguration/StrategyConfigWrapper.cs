﻿using System;
using System.Configuration;
using Robot.Logging;
using StockSharp.Logging;


namespace Robot.Configuration {
    public class StrategyConfigWrapper<T> where T: ConfigurationSection {
        public StrategyConfigWrapper(System.Configuration.Configuration config) {
            _config = config;
            _spp = (StrategyPluginParameters)_config.GetSection(GlobalConf.StrategyPluginParameters);
            _p = ((T)Convert.ChangeType(_config.GetSection(_parametersSection), typeof(T)));
        }
        public StrategyPluginParameters GateParameters() {
            return _spp;
        }

        public T P() {
            return _p;
        }
        public void Save() {
            try {
                Config.Save(System.Configuration.ConfigurationSaveMode.Modified);
            } catch (ConfigurationErrorsException e) {
                RobotLogger.Instance.AddWarningLog(e.Message);
                string fileName = Config.FilePath;
                fileName += GlobalConf.ConfigSaveAsSuffix;
                RobotLogger.Instance.AddDebugLog(
                    "Конфигурация сохраняется в файл {0}", fileName);
                Config.SaveAs(fileName, ConfigurationSaveMode.Full, true);
            }
        }

        public System.Configuration.Configuration Config {
            get {
                return _config;
            }
        }
        private System.Configuration.Configuration _config;
        private T _p;
        private StrategyPluginParameters _spp;
        private readonly string _parametersSection = "parameters";
    }
}
