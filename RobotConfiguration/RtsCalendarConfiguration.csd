﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="a3fe13f9-2e60-4415-879e-854349b1fead" namespace="Robot.Configuration" xmlSchemaNamespace="urn:Robot.Configuration" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="RtsCalendar" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="rtsCalendar">
      <elementProperties>
        <elementProperty name="AdditionalWorkingDates" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="AdditionalWorkingDates" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/WorkingDates" />
          </type>
        </elementProperty>
        <elementProperty name="AdditionalHolidayDates" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="AdditionalHolidayDates" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/HolidaysDates" />
          </type>
        </elementProperty>
        <elementProperty name="ExpirationDates" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="ExpirationDates" isReadOnly="false">
          <type>
            <configurationElementCollectionMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/ExpirationDates" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="Date">
      <attributeProperties>
        <attributeProperty name="value" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="value" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/DateTime" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElementCollection name="HolidaysDates" xmlItemName="date" codeGenOptions="ICollection">
      <itemType>
        <configurationElementMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/Date" />
      </itemType>
    </configurationElementCollection>
    <configurationElementCollection name="WorkingDates" xmlItemName="date" codeGenOptions="GetItemMethods, ICollection">
      <itemType>
        <configurationElementMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/Date" />
      </itemType>
    </configurationElementCollection>
    <configurationElementCollection name="ExpirationDates" xmlItemName="date" codeGenOptions="ICollection">
      <attributeProperties>
        <attributeProperty name="ExpirationDaySesionTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ExpirationDaySesionTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/TimeSpan" />
          </type>
        </attributeProperty>
      </attributeProperties>
      <itemType>
        <configurationElementMoniker name="/a3fe13f9-2e60-4415-879e-854349b1fead/Date" />
      </itemType>
    </configurationElementCollection>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>