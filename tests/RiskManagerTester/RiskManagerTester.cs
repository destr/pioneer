﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common.Equity;
using Robot.Logging;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace RiskManagerTester {
    [TestClass]
    public class RiskManagerTester {
        static readonly decimal _plsRiskManagerLongExpected = 4;
        static readonly decimal _plsRiskManagerShortExpected = 2;
        static readonly decimal _plsRiskManagerLongOverturnExpected = 5;
        static readonly decimal _plsRiskManagerShortOverturnExpected = 5;
        static readonly decimal _plsRiskManagerSupportExpected = 147000;
        static readonly decimal _plsRiskManagerResistExpected = 142000;
        static readonly decimal _plsRiskManagerValue = 2;

        static readonly decimal _bvsRiskManagerValue = 3;
        static readonly decimal _bvsRiskManagerHLevelExpected = 151750;
        static readonly decimal _bvsRiskManagerLLevelExpected = 144250;
        static readonly decimal _bvsRiskManagerLongExpected = 4;
        static readonly decimal _bvsRiskManagerShortExpected = 1;
        /// <summary>
        /// Расчёт объёма в стратегии PLS
        /// </summary>
        static void PlsRiskManagerInit() {
            // портфель
            Portfolio portfolio = new Portfolio();
            //portfolio.BeginAmount.Value = 100000;

            // Инструмент
            Security security = new Security();
            // стоимость шага цены
            security.MinStepPrice = 3;
            // Шаг цены
            security.MinStepSize = 5;
            security.MarginSell = security.MarginBuy = 10000;

            Console.WriteLine("Test start Long");
            PlsRiskManagerLong(portfolio, security);

            Console.WriteLine("Test start Short");
            PlsRiskManagerShort(portfolio, security);

            Console.WriteLine("Test long -> short");
            PlsAfterStopLong(portfolio, security);

            Console.WriteLine("Test short -> long");
            PlsAfterStopShort(portfolio, security);

        }  // PlsRiskManager
        static void PlsRiskManagerLong(Portfolio portfolio, Security security) {
            RiskVolumeManager riskManager = createRiskManager(portfolio, security, _plsRiskManagerValue);
            decimal maxVolume = riskManager.maxVolume(147100, 146350);
            Console.WriteLine("Max volume: {0}", maxVolume);
            checkValue(_plsRiskManagerLongExpected, maxVolume);
        }  // PlsRiskManagerLong

        static void PlsRiskManagerShort(Portfolio portfolio, Security security) {
            RiskVolumeManager riskManager = createRiskManager(portfolio, security, _plsRiskManagerValue);
            decimal maxVolume = riskManager.maxVolume(141500, 142650);
            checkValue(maxVolume, _plsRiskManagerShortExpected);
        }  // PlsRiskManagerShort

        static void PlsAfterStopLong(Portfolio portfolio, Security security) {
            RiskVolumeManager riskManager = createRiskManager(portfolio, security, _plsRiskManagerValue);

            decimal actual = riskManager.maxVolumeAfterStop(147100, 146350,
                _plsRiskManagerLongExpected, _plsRiskManagerSupportExpected);
            Assert.AreEqual(_plsRiskManagerLongOverturnExpected, actual);

        }  // PlsAfterStopLong

        static void PlsAfterStopShort(Portfolio portfolio, Security security) {
            RiskVolumeManager riskManager = createRiskManager(portfolio, security, _plsRiskManagerValue);
            decimal actual = riskManager.maxVolumeAfterStop(141500, 142650,
                _plsRiskManagerShortExpected, _plsRiskManagerResistExpected);
            Assert.AreEqual(_plsRiskManagerShortOverturnExpected, actual);
        }  // PlsAfterStopShorts

        static void BvsRiskManagerLong(Portfolio port, Security sec) {
            RiskVolumeManager riskManager = createRiskManager(port, sec, _bvsRiskManagerValue);
            decimal actual = riskManager.maxVolume(152000, 150850);
            Assert.AreEqual(_bvsRiskManagerLongExpected, actual);

        } // BvsRiskManagerLong

        static void BvsRiskManagerShort(Portfolio port, Security sec) {
            RiskVolumeManager riskManager = createRiskManager(port, sec, _bvsRiskManagerValue);
            decimal actual = riskManager.maxVolume(142500, 145150);
            Assert.AreEqual(_bvsRiskManagerShortExpected, actual);            
        }  // BvsRiskManager

        static RiskVolumeManager createRiskManager(Portfolio portfolio, Security security,
            decimal risk) {
            RiskVolumeManager riskManager = new RiskVolumeManager(risk);
            riskManager.security = security;
            riskManager.portfolio = portfolio;
            riskManager.logger = RobotLogger.Instance;
            return riskManager;
        }  // createRiskManager
        /// <summary>
        /// Проверка значения ожидаемого и реально полученного
        /// </summary>
        /// <param name="expected">Ожидаемое значение</param>
        /// <param name="actual">Реальное значение</param>
        static void checkValue(decimal expected, decimal actual) {
            Assert.AreEqual(expected, actual);
        }  // checkValue

        public RiskManagerTester() {
            LogManager logManager = new LogManager();
            logManager.Sources.Add(RobotLogger.Instance);
            logManager.Listeners.Add(new ConsoleLogListener());
        }
        [TestMethod]
        public void PlsRiskManager() {
            PlsRiskManagerInit();
        }  // PlsRiskManager
        [TestMethod]
        public void BvsRiskManager() {
            Security sec = new Security();
            sec.MarginBuy = sec.MarginSell = 10000;
            sec.MinStepPrice = 3;
            sec.MinStepSize = 5;

            Portfolio port = new Portfolio();
            //port.BeginAmount.Value = 100000;

            Console.WriteLine("Bvs test long");
            BvsRiskManagerLong(port, sec);

            Console.WriteLine("Bvs test short");
            BvsRiskManagerShort(port, sec);


        }

    }
}
