﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Logging;
using Robot.Sms;
using Robot.Strategy;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace SmsEventsMergerTester {
    [TestClass]
    public class SmsEventsMergerTester {
        event Action<SmsMessage> ev;
        SmsEventsMerger _merger;
        private static readonly LogManager log_manager_ = new LogManager();
        public SmsEventsMergerTester() {

            log_manager_.Sources.Add(RobotLogger.Instance);
            log_manager_.Listeners.Add(new ConsoleLogListener());


            //_merger = new SmsEventsMerger(@"..\..\..\configexample\smsmerger.xml", 
            //@"..\..\..\configexample\smsconfig.xml");
        }
        [TestMethod]
        public void MergeAll() {
            //_merger.Save();
            //return;
            ev += new Action<SmsMessage>(_merger.SmsMerge);
            //sendsms(3);
            
            //Thread.Sleep(1000);
            sendsms(new string[] { "pls_rts_1", "pls_rts_2" });
            Thread.Sleep(4000);
            sendsms(new string[] { "pls_rts_1"});
            Thread.Sleep(4000);
            sendsms(new string[] { "bvs_rts_eq", "bvs_rts_rp", "bvs_si" });
            Thread.Sleep(4000);
            sendsms(new string[] { "bvs_rts_eq", "bvs_rts_rp"});
            Thread.Sleep(4000);
            sendsms(new string[] { "st0", "st1", "st2" });
            Thread.Sleep(4000);
            sendsms(new string[] { "st0", "st1" });
            Thread.Sleep(4000);
            ev -= _merger.SmsMerge;
        }
        [TestMethod]
        public void MergePart() {
            ev += new Action<SmsMessage>(_merger.SmsMerge);
            sendsms(2);
            
            Thread.Sleep(12000);
            sendsmsOrderInfo(2);
            Thread.Sleep(12000);

            ev -= _merger.SmsMerge;
        }
        [TestMethod]
        public void MergeNoGroup() {
            ev += new Action<SmsMessage>(_merger.SmsMerge);
            ev.Invoke(new StrategyPositionInfo() {
                Sender = String.Format("ddddd{0}", 0),
                Direction = OrderDirections.Buy
            });
        }
        void sendsms(int count) {
            for (int i = 0; i < count; ++i) {
                ev.Invoke(new StrategyPositionInfo() {
                    Sender = String.Format("st{0}", i),
                    Direction = OrderDirections.Buy
                });
            }
        }
        void sendsms(string[] names) {
            foreach (string name in names) {
                ev.Invoke(new StrategyPositionInfo() {
                    Sender = name,
                    Direction = OrderDirections.Buy
                });
            }
        }
        void sendsmsOrderInfo(int count) {
            for (int i = 0; i < count; ++i) {
                ev.Invoke(new OrderInfo() {
                    Sender = String.Format("st{0}", i),
                    IsStopOrder = true,
                    Action = OrderInfo.OrderAction.CancelFailed
                });
            }
        }
        
    }
}
