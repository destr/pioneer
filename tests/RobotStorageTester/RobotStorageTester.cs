﻿using System;
using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace RobotStorageTester {
    [TestClass]
    public class RobotStorageTester {
        [TestMethod]
        public void OpenConfiguration() {

            ExeConfigurationFileMap configurationPath = new ExeConfigurationFileMap();
            configurationPath.ExeConfigFilename = @"..\..\..\configexample\RobotConfiguration.csd.config";
            //@"C:\Users\destr\Documents\Visual Studio 2010\Projects\trading\Robots\PioneerRobot\configexample\RobotConfiguration.csd.config";
            try {
                System.Configuration.Configuration config =
                ConfigurationManager.OpenMappedExeConfiguration(configurationPath, ConfigurationUserLevel.None);

                Robot.Configuration.Robot robotSection = (Robot.Configuration.Robot)config.GetSection("robot");
                for (int i = 0; i < robotSection.gates.Count; ++i) {
                    Console.WriteLine(robotSection.gates.GetItemAt(i).login);
                }

                
            } catch (Exception e) {
                Console.WriteLine(e);
            }
        }
    }
}
