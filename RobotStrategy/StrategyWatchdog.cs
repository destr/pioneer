﻿using System;
using System.Collections.Generic;
using System.Threading;
using Robot.Common.Quik;
using Robot.Logging;
using Robot.Sms;
using StockSharp.Algo;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace Robot.Strategy {
    
    /// <summary>
    /// Наблюдатель за позицией стратегии и наличием стоп-заявки
    /// </summary>
    public class StrategyWatchdog : ISmsable {
        public event Action<SmsMessage> Sms;
        /// <summary>
        /// Интервал проверки
        /// </summary>
        public TimeSpan CheckIntervalMin { get; set; }
        /// <summary>
        /// Обработчик тайм-аута экспорта
        /// </summary>
        public ExportTimeOutHandler ExportTimeoutHandler { get; set; }
        /// <summary>
        /// Информация он наблюдаемой стратегии
        /// </summary>
        internal class WatchInfo {
            /// <summary>
            /// Плагин стратегии
            /// </summary>
            public IStrategyPlugin Plugin { get; set; }
            /// <summary>
            /// Состояния
            /// </summary>
            public enum StateWatch {
                /// <summary>
                /// Признак отсутствующего стопа
                /// </summary>
                NoStop      = 0x1,
                /// <summary>
                /// Признак того, что смс уже отправлялось
                /// </summary>
                AlreadySms  = 0x2,
                /// <summary>
                /// Не поддерживает alive состояния
                /// </summary>
                NoSupportAliveWarning = 0x4,
                /// <summary>
                /// Признак отправки сообщения об умершей стратегии
                /// </summary>
                DeadSms = 0x8
            }
            /// <summary>
            /// Состояние наблюдения
            /// </summary>
            public StateWatch State { get; set; }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public StrategyWatchdog() {
        }
        /// <summary>
        /// Запустить наблюдение
        /// </summary>
        public void Start() {
            _thread = new Thread(doWork);
            _thread.Start();
        }  // Start
        /// <summary>
        /// Остановить наблюдение
        /// </summary>
        public void Stop() {
            _checkWaiter.Set();
        }  // Stop
        /// <summary>
        /// Добавить стратегию под наблюдение
        /// </summary>
        /// <param name="plugin"></param>
        public void Add(IStrategyPlugin plugin) {
            RobotLogger.Instance.AddDebugLog(
                "Добавлена стратегия для наблюдения за позицией: {0}",
                (plugin as StockSharp.Algo.Strategies.Strategy).Name);
            lock (_watchList) {
                _watchList.Add(new WatchInfo() {
                    Plugin = plugin
                });
            }
        }  // Add
        /// <summary>
        /// Удалить стратегию из-под наблюдения
        /// </summary>
        /// <param name="plugin"></param>
        public void Remove(IStrategyPlugin plugin) {
            RobotLogger.Instance.AddDebugLog(
                "Удалена стратегия из списка наблюдения за позицией: {0}",
                (plugin as StockSharp.Algo.Strategies.Strategy).Name);
            lock (_watchList) {
                _watchList.RemoveAll(w => w.Plugin == plugin);
            }
        }  // Remove
        public bool IsRunning {
            get {
                if (_thread == null) return false;
                return _thread.IsAlive;
            }
        }
        /// <summary>
        /// Рабочая функция потока
        /// </summary>
        private void doWork() {
            for (; ;) {
                if(_checkWaiter.Wait(CheckIntervalMin)) {
                    // Нас разбудили, выходим
                    break;
                }
                lock (_watchList) {
                    foreach (WatchInfo info in _watchList) {
                        StrategyStateInfo sinfo = info.Plugin.State();
                        RobotLogger.Instance.AddDebugLog(sinfo.ToString());
                        DateTime dt = DateTime.Now;
                        if (!Robot.Common.Exchange.Rts.WorkingCalendar.isTradingDate(dt)) {
                            RobotLogger.Instance.AddDebugLog("Не рабочий день. Проверка игноируется.");
                            continue;
                        }

                        if (!checkAlive(sinfo, info)) {
                            RobotLogger.Instance.AddInfoLog("Стратегия {0} умерла", sinfo.Name);
                            continue;
                        }

                        
                        /// Если не в позиции, то ничего не проверяем
                        if ((sinfo.MarketState & StrategyMarketState.InMarket) == 0)
                            continue;
                        // Стоп есть, всё в порядке
                        if (sinfo.StopVolume != 0) {
                            // Если вдруг выставили стоп, а потом он опять пропал
                            if ((info.State & WatchInfo.StateWatch.NoStop) != 0) {
                                info.State &= ~WatchInfo.StateWatch.NoStop;
                                info.State &= ~WatchInfo.StateWatch.AlreadySms;
                            }
                            continue;
                        }

                        if ((info.State & WatchInfo.StateWatch.NoStop) == 0) {
                            info.State |= WatchInfo.StateWatch.NoStop;
                            // стопа нет, возможно переходные процессы
                            // на следующем шаге отправляем смс
                            continue;
                        }
                        
                        // Если уже был NoStop то высылаем смс
                        if ((info.State & WatchInfo.StateWatch.NoStop) != 0 &&
                            (info.State & WatchInfo.StateWatch.AlreadySms) == 0) {
                            info.State |= WatchInfo.StateWatch.AlreadySms;
                                Sms.Invoke(new SmsMessage() {
                                    Sender = (info.Plugin as StockSharp.Algo.Strategies.Strategy).Name,
                                    Text = "No acive stop"
                                });
                        }
                    }
                }  // lock
            }  // while
        }  // doWork

        private bool checkAlive(StrategyStateInfo sinfo, WatchInfo info) {
            if (sinfo.AliveStatus == null) {
                if ((info.State & WatchInfo.StateWatch.NoSupportAliveWarning) == 0) {

                    RobotLogger.Instance.AddWarningLog(
                        "Стратегия {0} не поддерживает мониторинг состояний", sinfo.Name);

                    info.State |= WatchInfo.StateWatch.NoSupportAliveWarning;
                }

                return true;
            }

            if (sinfo.AliveStatus.IsAlive()) {
                // Стратегия жива
                if ((info.State & WatchInfo.StateWatch.DeadSms) != 0) {
                    // Стратегия ожила
                    info.State &= ~WatchInfo.StateWatch.DeadSms;
                    Sms.Invoke(new SmsMessage() {
                        Sender = "Alive",
                        Text = String.Format("{0} : again run", sinfo.Name)
                    });
                }
                return (true);
            }
            // если отвалился экспорт, то ничего страшного, уже оповестили пользователя
            if (ExportTimeoutHandler.ExportTimeouted) {
                RobotLogger.Instance.AddInfoLog(
                    "Стратегия {0} не отвечает, потому что проблемы с экспортом", sinfo.Name);
                // Сбрасываем время обновления состояния стратегии
                sinfo.AliveStatus.UpdateByTimeout();
                return (true);
            }
            /// Убедимся, что время торговое, т.к. во время клиринга события не приходят
            DateTime dt = DateTime.Now;
            if (!ExchangeBoard.Forts.WorkingTime.IsTradeTime(dt)) {
                RobotLogger.Instance.AddInfoLog(
                    "Стратегия {0} не отвечает, потому что время {1} не торговое", sinfo.Name, dt);
                sinfo.AliveStatus.UpdateByCliring();
                return (true);
            }

            if ((info.State & WatchInfo.StateWatch.DeadSms) == 0) {
                Sms.Invoke(new SmsMessage() {
                    Sender = "Alive",
                    Text = String.Format("{0}: dead", sinfo.Name)
                });

                info.State |= WatchInfo.StateWatch.DeadSms;
            }

            return (false);
        }  // checkAlive

        /// <summary>
        /// Поток
        /// </summary>
        private Thread _thread = null;
        /// <summary>
        /// Список наблюдаемых стратегий
        /// </summary>
        private List<WatchInfo> _watchList = new List<WatchInfo>();
        /// <summary>
        /// Сигнал ожидания проверки
        /// </summary>
        private ManualResetEventSlim _checkWaiter = new ManualResetEventSlim(false);

    }
}
