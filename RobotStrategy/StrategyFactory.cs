﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Robot.Strategy {
    
    /// <summary>
    /// Информация о плагине
    /// </summary>
    struct PluginInfo {
        public PluginInfo(string path, string c) {
            dllpath = path;
            ctor = c;
        }
       
        /// <summary>
        /// Путь к плагину
        /// </summary>
        public string dllpath;

        /// <summary>
        /// Конструктор плагина
        /// </summary>
        public string ctor;
    }
    /// <summary>
    /// Фабрика стратегий
    /// </summary>
    public class StrategyFactory {
        /// <summary>
        /// Конструктор
        /// </summary>
        public StrategyFactory() {
             _pluginList = new Dictionary<PluginInfo, Type>();
            loadStrategyPluguns();
        }
        /// <summary>
        /// Загрузка плагинов стратегий
        /// </summary>
        void loadStrategyPluguns() {
            string[] pluginpathlist = Directory.GetFiles(_strategiesDir, "StrategyPlugin.*.dll");
            foreach (string pluginfullpath in pluginpathlist) {
                _pluginList.Add(new PluginInfo(pluginfullpath, ctorName(pluginfullpath)), null);

            }
        }
        /// <summary>
        /// Создание плагина стратегии
        /// </summary>
        /// <param name="strategyconstructor"></param>
        /// <returns>Объект плагина стратегии</returns>
        public IStrategyPlugin createStrategy(string strategyconstructor) {
            foreach (PluginInfo key in _pluginList.Keys) {
                if (key.ctor == strategyconstructor) {
                      return internalCreater(key);
                }
            }
            throw new Exception(String.Format("Не найдена стратегия {0}", strategyconstructor));
        }
        /// <summary>
        /// Создание плагина стратегии
        /// </summary>
        /// <param name="key">Название плагина</param>
        /// <returns>Объект плагина стратегии</returns>
        IStrategyPlugin internalCreater(PluginInfo key) {
            Type objtype;
            if (!_pluginList.TryGetValue(key, out objtype)) {
                throw new Exception("No key");
            }
            try {
                // Загружаем и создаём.
                if (objtype == null) {
                    Assembly ass = null;

                    ass = Assembly.LoadFrom(key.dllpath);
                    objtype = ass.GetType(key.ctor, true);
                    _pluginList[key] = objtype;
                }

                // создаём объект
                return (IStrategyPlugin)Activator.CreateInstance(objtype);
            } catch(Exception e) {
                Console.WriteLine(e);
                throw;
            }
        }
        /// <summary>
        /// Получить имя конструктор из пути к плагину
        /// </summary>
        /// <param name="pluginfullpath">Полный путь к плагину</param>
        /// <returns>Имя конструктора</returns>
        string ctorName(string pluginfullpath) {
            // определяем имя конструктора из имени файла
            int posbegin = pluginfullpath.LastIndexOf('\\') + 1;
            int posend = pluginfullpath.LastIndexOf('.');
            string namespacesname = pluginfullpath.Substring(posbegin, posend - posbegin);
            posbegin = namespacesname.LastIndexOf('.');
            string ctorname = namespacesname + namespacesname.Substring(posbegin);
            return ctorname;
        }

        /// <summary>
        /// Список названий плагинов и их загруженных типов
        /// </summary>
        private Dictionary<PluginInfo, Type>    _pluginList;
        /// <summary>
        /// Каталог со стратегиями
        /// </summary>
        private readonly string _strategiesDir = ".";
    }
}
