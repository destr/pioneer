﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Sms;
using StockSharp.Algo;
using StockSharp.BusinessEntities;

namespace Robot.Strategy {
    [Flags]
    public enum StrategyMarketState {
        /// <summary>
        /// Не в позиции
        /// </summary>
        Free = 0,
        /// <summary>
        /// Длинная позиция
        /// </summary>
        Long = 0x1,
        /// <summary>
        /// Короткая позиция
        /// </summary>
        Short = 0x2,
        /// <summary>
        /// В позиции
        /// </summary>
        InMarket = Long | Short,
        /// <summary>
        /// Неизвестная позиция. Данных нет
        /// </summary>
        Unknown = 0x4,
        /// <summary>
        /// Сохраняем позиции
        /// </summary>
        KeepPosition = 0x10
    }
    /// <summary>
    /// Информаци о позиции стратегии
    /// </summary>
    public class StrategyPositionInfo : SmsMessage {
        /// <summary>
        /// Цена по которой исполнилась заявка
        /// </summary>
        public decimal  price { get; set; }
        /// <summary>
        /// Направление заявки
        /// </summary>
        public OrderDirections Direction { get; set; }
        public override string ToText() {
            string ret;
            if (Direction == OrderDirections.Buy) {
                ret = "Long";
            } else {
                ret = "Short";
            }
            ret += " by " + Sender;

            return ret;
        }
    }

    /// <summary>
    /// Информация о заявке
    /// </summary>
    public class OrderInfo : SmsMessage {
        public bool IsStopOrder = false;
        /// <summary>
        /// Действие с ордером
        /// </summary>
        [Flags]
        public enum OrderAction {
            /// <summary>
            /// Активирован
            /// </summary>
            Activated,
            /// <summary>
            /// Ошибка регистрации
            /// </summary>
            Failed,
            /// <summary>
            /// Ошибка при отмене
            /// </summary>
            CancelFailed
        }
        public OrderAction Action;
        /// <summary>
        /// Название стратегии
        /// </summary>
        public override string ToText() {
            string ret = Sender;
            ret += ": ";
            ret += IsStopOrder ? "stop" : "order";
            ret += " ";
            ret += Action;
            
            return ret;
        }
    }

    /// <summary>
    /// Сообщение стратегии о том, что она жива
    /// </summary>
    public abstract class StrategyAliveStatus {
        /// <summary>
        /// Тип обновления статуса
        /// </summary>
        [Flags]
        public enum UpdateType {
            /// <summary>
            /// Не обновлялось
            /// </summary>
            None        = 0,
            /// <summary>
            /// Время клиринга
            /// </summary>
            Cliring     = 0x1,
            /// <summary>
            /// Тайм-аут экспорта
            /// </summary>
            Timeout     = 0x2,
            /// <summary>
            /// Событие стратегии
            /// </summary>
            Event       = 0x4
        }
        /// <summary>
        /// Проверка условий для определения статуса стратегии
        /// </summary>
        /// <returns></returns>
        public abstract bool IsAlive();
        
        /// <summary>
        /// Обновление стратегией
        /// </summary>
        public void UpdateByStrategy() {
            UpdateTime = DateTime.Now;
            _updateType |= UpdateType.Event;
            DoUpdateByStrategy();
        }

        /// <summary>
        /// Дополнительные обновления осуществялемые стратегией
        /// </summary>
        protected virtual void DoUpdateByStrategy() {
        }
        protected virtual void DoUpdateByCliring() {
        }
        protected virtual void DoUpdateByTimeout() {
        }

        /// <summary>
        /// Обновление статуса стратегии во время клиринга
        /// </summary>
        public void UpdateByCliring() {
            UpdateTime = DateTime.Now;
            _updateType |= UpdateType.Cliring;
            DoUpdateByCliring();
        }  // UpdateByCliring
        /// <summary>
        /// Обновление статуса стратегии во время тайм-аута экспорта
        /// </summary>
        public void UpdateByTimeout() {
            UpdateTime = DateTime.Now;
            _updateType |= UpdateType.Timeout;
            DoUpdateByTimeout();
        } // UpdateByTimeout

        /// <summary>
        /// Последнее время обновления статуса
        /// </summary>
        public DateTime UpdateTime { get; protected set; }

        /// <summary>
        /// Тайм-фрем стратегии
        /// </summary>
        public TimeSpan TimeFrame { get; set; }
        
        /// <summary>
        /// Тип обновлени
        /// </summary>
        private UpdateType _updateType = UpdateType.None;

    };

    /// <summary>
    /// Информация о выполняемой стратегии
    /// </summary>
    public class StrategyStateInfo {
        /// <summary>
        /// Название стратегии
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Портфель
        /// </summary>
        public string Portfolio { get; set; }
        /// <summary>
        /// Торгуемый инструмент
        /// </summary>
        public string SecurityCode { get; set;}
        /// <summary>
        /// Состояние стратегии на рынке
        /// </summary>
        public StrategyMarketState MarketState { get; set; }
        /// <summary>
        /// Рабочее состояние стратегии
        /// </summary>
        public ProcessStates ProcessState { get; set; }
        /// <summary>
        /// Объем позиции
        /// </summary>
        public decimal Volume { get; set; }
        /// <summary>
        /// Статус живсти стратегии
        /// </summary>
        public StrategyAliveStatus AliveStatus { get; set; }
        /// <summary>
        /// Объем стопа ордера
        /// </summary>
        [System.ComponentModel.DefaultValue(0)]
        public decimal StopVolume { get; set; } 
        /// <summary>
        /// Расширенная информация
        /// </summary>
        [System.ComponentModel.DefaultValue(null)]
        public IDictionary<string, string> Extended { get; set; }
        /// <summary>
        /// Преобразование в строку
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            string extented = "";
            if (Extended != null) {
                extented = ", ";
                extented += string.Join(", ", Extended.Select(kv => kv.Key + ": " + kv.Value));
            }
            return String.Format("Стратегия: {0}, Портфель: {1}, Инструмент: {2}, Состояния: {3}, {4}" + 
                ", Объём: {5}, Объём стопа: {6} Alive Last update: {7}{8}", Name, Portfolio, SecurityCode, 
                MarketState, ProcessState, Volume, StopVolume,
                AliveStatus != null ? AliveStatus.UpdateTime.ToString() : "Not supported", extented);
        }
    }

}