﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Logging;
using StockSharp.Algo.Candles;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace Robot.Strategy.Helper {
    
    /// <summary>
    /// Вспомогательные фунции
    /// </summary>
    public static class StrategyPluginHelper {
        static public void InsertCandlesInSeries(IEnumerable<TimeFrameCandle> candles,
            CandleManager candleManager, CandleSeries candleSeries) {
            foreach (TimeFrameCandle candle in candles) {
                // проверка наличия свечи в сери
                if (candleSeries.GetTimeFrameCandle(candle.OpenTime) != null) {
                    RobotLogger.Instance.AddDebugLog("Свеча для инструмента {0} со временем {1} "
                        + "уже загружена", candle.Security.Code, candle.OpenTime);
                    continue;
                }
                candle.Series = candleSeries;
                candleManager.Container.AddCandle(candleSeries, candle);
            }
        }
        /// <summary>
        /// Проверка регистрации свечей для инструмента и регистрация в если не зарегистрированы
        /// </summary>
        /// <param name="security">Инструмент</param>
        /// <returns></returns>
        static public CandleSeries CheckAndRegisterSeries(CandleManager cm,
            Security security, TimeSpan timeFrame) {

            if (!cm.IsCandlesRegistered<TimeFrameCandle>(security, timeFrame)) {
                RobotLogger.Instance.AddDebugLog("Регистрация серии свечей для инструмента {0}",
                security.Code);
                CandleSeries cs = new CandleSeries(typeof(TimeFrameCandle), security, timeFrame);
                cm.Start(cs);
                return cs;
            }
            RobotLogger.Instance.AddDebugLog("Серии свечей для инструмента {0} уже зарегистрированы",
                security.Code);
            return cm.GetSeries<TimeFrameCandle>(security, timeFrame);
        }
        /// <summary>
        /// Поиск инструмента в таблице инструментов шлюза
        /// </summary>
        /// <param name="trader">Шлюз</param>
        /// <param name="secCode">Код инструмента</param>
        /// <returns></returns>
        static public Security LoadSecurity(ITrader trader, string secCode) {
            Security s = trader.Securities.FirstOrDefault(
                          p => p.Code == secCode);

            if (s == null) {
                throw new Exception(String.Format(
                    "Инструмент {0} не найден в таблице 'Инструменты'", secCode));
            }
            return s;
        }
        /// <summary>
        /// Поиск портфеля в таблице портфелей шлюза
        /// </summary>
        /// <param name="trader">Шлюз</param>
        /// <param name="portfolio">Портфель</param>
        /// <returns></returns>
        static public Portfolio LoadPortfolio(ITrader trader, string portfolio) {
            Portfolio p_ = trader.Portfolios.FirstOrDefault(
                p => p.Name == portfolio);

            if (p_ == null) {
                throw new Exception(String.Format(
                    "Портфель {0} не найден в таблице 'Портфель по деривативам'", portfolio));
            } 
            return p_;
        }

    }
}
