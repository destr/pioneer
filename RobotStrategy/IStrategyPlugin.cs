﻿using Robot.Sms;
using StockSharp.Algo.Candles;
using StockSharp.Logging;

namespace Robot.Strategy {
    /// <summary>
    /// Интерфейс стратегии
    /// </summary>
    public interface IStrategyPlugin: ISmsable {

        /// <summary>
        /// Установить менеджер свечей для стратегии
        /// </summary>
        /// <param name="candle_manager">Менеджер свечей</param>
        CandleManager CandleManager { get; set; }
        /// <summary>
        /// Установить сигнал входа в рынок
        /// </summary>
        bool ManualLongSignal { get; set; }
        /// <summary>
        /// Установить сигнал входа в короткую позицию
        /// </summary>
        bool ManualShortSignal { get; set; }
        /// <summary>
        /// Показать состояние стратегии
        /// </summary>
        StrategyStateInfo State();
        /// <summary>
        /// Загрузить конфигурацию
        /// </summary>
        /// <param name="config">Конфигурация</param>
        void Load(System.Configuration.Configuration config);
        /// <summary>
        /// Сохранитьконфигурацию
        /// </summary>
        void Save();
        /// <summary>
        /// Зарегистрировать стратегию как источник лог сообщений
        /// </summary>
        void RegisterAsLogSource(ConsoleLogListener consoleListener);
        /// <summary>
        /// Убрать стратегию как источник лог-сообщений
        /// </summary>
        void UnregisterAsLogSource();
        /// <summary>
        /// Проверка факта завершения дня для стратегии
        /// </summary>
        /// <returns>true если рабочий день стратегии кончился</returns>
        bool WorkDayEnd();
    }
}