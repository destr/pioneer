﻿using System;
using System.Timers;
using System.IO;
using System.ComponentModel;
using Robot.Configuration;
using Robot.Logging;
using StockSharp.Logging;
using StockSharp.Algo.Candles;

namespace Robot.Strategy {
    public class StrategyPluginBase {
        public StockSharp.Algo.Strategies.Strategy strategy { get; internal set; }
        public StrategyPluginBase(StockSharp.Algo.Strategies.Strategy st) {
            strategy  = st;
            lm = new LogManager();
            exitTimer = new Timer();
            // 1 секунда
            exitTimer.Interval = 1000;
        } // Ctor
        /// <summary>
        /// Добавить стратегию как источник логов
        /// </summary>
        public void RegisterAsLogSource(ConsoleLogListener consoleListener) {
            lm.Listeners.Add(consoleListener);

            string logfilepath = Path.Combine(GlobalConf.StrategyLogDir(), strategy.Name);
            string logfilename = String.Format("{0}.log",
                DateTime.Now.ToString(GlobalConf.LogFilenameFormat));

            logfilepath = Path.Combine(logfilepath, logfilename);
            fl = new FileLogListener(logfilepath) {
                                        Append = true };
            lm.Listeners.Add(fl);
            lm.Sources.Add(strategy);
        }  // registerAsLogSource
        /// <summary>
        /// Убрать стратегию из списка источников логов
        /// </summary>
        public void UnregisterAsLogSource() {
            lm.Sources.Clear();
            lm.Listeners.Clear();
            fl.Dispose();
        }

        static public bool workDayEnd(TimeSpan time) {
            if (manualWorkDayEnd) {
                return manualWorkDayEnd;
            }
            return DateTime.Now.TimeOfDay >= time;
        }  // workDayEnd

        public DateTime internalSetBaseCandleTime(string datetime) {
            // Пытаемся конвертировать строку с данными
            TimeSpan time;
            DateTime newDate;
            if (TimeSpan.TryParse(datetime, out time)) {
                // новая дата это сегодня + указанное время

                newDate = DateTime.Now.Date + time;
                RobotLogger.Instance.AddInfoLog(
                    "Новая дата базовой свечи: {0}", newDate);
                return newDate;
            }
            // Может быть передали дату
            
            if (!DateTime.TryParse(datetime, out newDate)) {
                throw new Exception(
                    String.Format("Указанное не корректный формат времени свечи: {0}",
                    datetime));
            }
            RobotLogger.Instance.AddInfoLog(
                    "Новая дата базовой свечи: {0}", newDate);
            
           return newDate; 
        }  // SetBaseCandleTime

        /// <summary>
        /// Сигнал ручного входа в длиную позицию
        /// </summary>
        [DefaultValue(false)]
        public bool ManualLongSignal { get; set; }
        [DefaultValue(false)]
        public bool ManualShortSignal { get; set; }
        /// <summary>
        /// Получить менеджер свечей
        /// </summary>
        public CandleManager CandleManager {
            get {
                return this.candleManager;
            }
            set {
                this.candleManager = value;
            }
        }
        CandleManager candleManager;
        public Timer ExitTimer {
            get { return this.exitTimer; }
        }
        Timer exitTimer = new Timer();

        static public bool manualWorkDayEnd { get; set; }


        public LogManager lm { get; set; }
        FileLogListener fl;
    }
}  // namespace StrategyPlugin