﻿
using Robot.Common.Cli;

namespace Robot.Strategy.Cli {
    
    /// <summary>
    /// Командный интерфейс стратегии
    /// </summary>
    public interface ICliStrategy : IDoCliCommand {

        /// <summary>
        /// Сигнал входа в long
        /// </summary>
        /// <param name="args">Аргументы</param>
        void LongCommand(string[] args);
        /// <summary>
        /// Сигнал входа в short
        /// </summary>
        /// <param name="args">Аргументы</param>
        void ShortCommand(string[] args);
        /// <summary>
        /// Команда закрытия позиции
        /// </summary>
        /// <param name="args">Аргументы</param>
        void ClosePositionCommand(string[] args);
        /// <summary>
        /// Перерегистрация стоп-ордера с новой ценой
        /// </summary>
        /// <param name="args">Аргументы</param>
        void ReRegisterStop(string[] args);
    }
}  // namespace StrategyFactory