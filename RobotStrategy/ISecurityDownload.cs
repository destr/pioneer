﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Text;

namespace Robot.Strategy {
    /// <summary>
    /// Информация о скачиваемом инструменте
    /// </summary>
    public class DownloadSecurity {
        /// <summary>
        /// Код инструмента
        /// </summary>
        public string Code;
        /// <summary>
        /// Временой интервал свечи
        /// </summary>
        public TimeSpan TimeFrame;
        /// <summary>
        /// Количество рабочих дней скачиваемой истори
        /// </summary>
        public uint DaysToDownload;

        public string[] Args() {
            return new string[] { Code, DaysToDownload.ToString(), TimeFrame.TotalMinutes.ToString() };
        }
    }
    /// <summary>
    /// Интерфейс поддержки получения информации о иснтрументах у стратегии
    /// </summary>
    public interface ISecurityDownload {
        /// <summary>
        /// Получить информацию о инструментах необходимых для работы стратегии
        /// </summary>
        /// <param name="conf">Конфигурация стратегии</param>
        /// <returns>Список инструментов для скачивания</returns>
        IEnumerable<DownloadSecurity> DownloadSecurityList(System.Configuration.Configuration conf);
    }
}
