set strategydir="Strategies"
if not exist %strategydir% (
		mkdir %strategydir%
	)

move /Y StrategyPlugin.%1.dll %strategydir%

if %2 == Debug goto :debug

goto :exit

:debug
move /Y StrategyPlugin.%1.pdb %strategydir%

:exit