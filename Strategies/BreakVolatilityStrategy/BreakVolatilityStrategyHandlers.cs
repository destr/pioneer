﻿using Robot.Strategy;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace StrategyPlugin {
namespace BreakVolatilityStrategy {
    partial class BreakVolatilityStrategy {
        /// <summary>
        /// Действие на регистрацию ордера
        /// </summary>
        /// <param name="order">Ордер</param>
        void action_orderRegistered(Order order) {
            // ставим стоп, раз заявка успешно выполнена
            registerStop(order.Direction);

            var info = new StrategyPositionInfo() {
                Sender = this.Name,
                price = order.Price,
                Direction = order.Direction
            };
            Sms.Invoke(info);
        }  // action_orderRegistered
        /// <summary>
        /// Обработка события активации стоп ордера
        /// </summary>
        /// <param name="order"></param>
        void action_stopOrderActivated(Order order) {
            LoggingHelper.AddInfoLog(this, "Выход из позициии по стопу");
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.Activated
            });
            // Вышли из позиции.
            _cw.P().@internal.DaysInPosition = 0;
            _cw.Save();
        }  // action_stopOrderActivated
        /// <summary>
        /// Событие на ошибку регистрации ордера
        /// </summary>
        /// <param name="order"></param>
        void action_orderRegisterFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка регистрации ордера: {0}",
                order.Error.Message);
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = false,
                Action = OrderInfo.OrderAction.Failed
            });
        }  // action_orderRegisterFailed
        /// <summary>
        /// Событие на ошибку регистрации стоп ордера
        /// </summary>
        /// <param name="order"></param>
        void action_stopOrderRegisterFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка регистрации стоп-ордера: {0}",
                order.Error.Message);
            Sms.Invoke(new OrderInfo(){
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.Failed
            });
        }  // action_stopOrderRegisterFailed

        void action_cancelFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка отмены ордера: {0}", 
                order.Error.Message);
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.CancelFailed
            });
        }  // action_cancelFailed

    }  // class BreakVolatilityStrategy
}  // namespace BreakVolatilityStrategy
}  // namespace StrategyPlugin