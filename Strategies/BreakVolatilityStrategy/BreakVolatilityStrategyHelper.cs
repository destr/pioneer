﻿using System;
using Robot.Common.Exchange.Rts;
using Robot.Common.Helpers;
using Robot.Strategy;
using StockSharp.Algo;
using StockSharp.Algo.Strategies;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Quik;

namespace StrategyPlugin.BreakVolatilityStrategy {
    public partial class BreakVolatilityStrategy {
        /// <summary>
        /// Проверка актуальности уровней предыдущего дня
        /// </summary>
        /// <returns>true если уровни актуальны</returns>
        bool isLevelsActual() {
            if (_l == null) throw new ArgumentNullException("Уровни null. Strategy PANIC!");

            DateTime prevdate = lastTradeDay();
            
            DateTime levelsday = _l.Date.Date;
            if (levelsday == prevdate) {
                LoggingHelper.AddInfoLog(this, "Market day ({0}) and levels day ({1}) actual.",
                    prevdate, levelsday);
                return (true);
            }
            LoggingHelper.AddErrorLog(this,
                        "Day ago ({0}) is trading day but levels day ({1}). Levels NOT actual",
                        prevdate, levelsday);
            return (false);
            
        } // isLevelActual

        /// <summary>
        /// Последний торговый день стратегии с учётом переноса позиции
        /// </summary>
        /// <returns></returns>
        DateTime lastTradeDay() {
            DateTime prevdate = WorkingCalendar.previousTradingDate();
            for (int i = 0; i < _cw.P().@internal.DaysInPosition; ++i) {
                prevdate = WorkingCalendar.previousTradingDate(prevdate);
            }

            return prevdate;
        }  // lastTradeDay
        void exitMarket() {
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                // отменяем все ордера
                CancelActiveOrders();
                OrderDirections direction = ((state & StrategyMarketState.Short) != 0) ?
                    OrderDirections.Buy : OrderDirections.Sell;
                Order order = createLimitOrder(direction);
                order.WhenRegisterFailed().Do(action_orderRegisterFailed).Once().Apply(this);

                RegisterOrder(order);
                LoggingHelper.AddInfoLog(this, "Рабочий день закончен");
            }
        }
        /// <summary>
        /// Уровни рассчитаны
        /// </summary>
        /// <returns>true если уровни рассчитаны</returns>
        bool levelsCalculated() {
            return (low_price_level_ != -1) && (high_price_level_ != -1);
        }  // levelsCanculated
        bool stopLevelsCalculated() {
            return (stop_loss_size_ != -1) && (stop_loss_level_ != -1);
        }
        /// <summary>
        /// Расчёт уровней
        /// </summary>
        /// <param name="open">Цена открытия</param>
        void calculateLevels(decimal open) {

            var main_level = (_l.High - _l.Low) * _cw.P().@base.LevelFactor;
            LoggingHelper.AddDebugLog(this,
                "Рассчитываем уровни: main_level ({0}) = (l_.High({1}) - l_.Low({2})) * _cw.P().@base.LevelFactor({3})",
                main_level, _l.High, _l.Low, _cw.P().@base.LevelFactor);

            low_price_level_ = open - main_level;
            high_price_level_ = open + main_level;
            LoggingHelper.AddDebugLog(this,
                "Calculate levels: low_price_level_({0}) = open({1}) - main_level({2});",
                low_price_level_, open, main_level);
            LoggingHelper.AddDebugLog(this,
                "Calculate levels: high_price_level_({0}) = open({1}) + main_level({2});",
                high_price_level_, open, main_level);

        }  // calculateLeveles
        DateTime openPriceDate(DateTime date = new DateTime()) {
            if (date.Equals(DateTime.MinValue)) {
                date = DateTime.Now.Date;
            }

            return date.Add(ExchangeBoard.Forts.WorkingTime.Times[0].Min);
        }
        /// <summary>
        /// Создать лимитный ордер входа в рынок. По направлению выбирается
        /// максимальная или минимальная цена инструмента.
        /// </summary>
        /// <param name="direction">Направление</param>
        /// <returns>Ордер</returns>
        Order createLimitOrder(OrderDirections direction) {
            decimal price = (direction == OrderDirections.Buy) ? base.Security.MaxPrice :
                base.Security.MinPrice;
            
            var order = this.CreateOrder(direction, price, base.Volume);
            order.Type = OrderTypes.Limit;
            order.Comment = Name;
            return order;
        }  // createLimitOrder
        /// <summary>
        /// Выставления стоп-ордера
        /// </summary>
        /// <param name="direction">Направление оригинальной сделки</param>
        void registerStop(OrderDirections direction) {
            // Округляем цену, в соответствии с инструментом
            var stopprice = TradeHelper.RoundPrice(base.Security, stop_loss_level_);

            LoggingHelper.AddDebugLog(this,
                "Выставляем стоп-ордер на объём {0} направление защищаемой сделки {1} ",
                base.Volume, direction);
            var order = new Order {
                Type = OrderTypes.Conditional,
                Volume = base.Volume,
                Price = boundPriceByDirectionForStop(direction, stopprice),
                Security = base.Security,
                Portfolio = base.Portfolio,
                Direction = (direction == OrderDirections.Buy) ?
                               OrderDirections.Sell : OrderDirections.Buy,
                Comment = base.Name,            
                ExpiryDate = DateTime.MaxValue,
                Condition = new QuikOrderCondition
                {
                    Type = QuikOrderConditionTypes.StopLimit,
                    StopPrice = stopprice,
                    ActiveTime = null,
                    
                }
            };
            LoggingHelper.AddDebugLog(this,
                "Сформированный стоп-ордер Volume {0}, Price {1}, Direction {2},"
                , order.Volume, order.Price, order.Direction);

            var ruleOrderActivated = order.WhenActivated();
            var ruleOrderCancelFailed = order.WhenCancelFailed();
            
            ruleOrderActivated
                .Do(action_stopOrderActivated)
                .Apply(this)
                .Once()
                .Exclusive(ruleOrderCancelFailed);
            ruleOrderCancelFailed
                .Do(action_cancelFailed)
                .Once()
                .Apply(this).Exclusive(ruleOrderActivated);

            base.RegisterOrder(order);
        } // registerStop
        
        /// <summary>
        /// Получить граничную цену для стопа в зависимости от направления сделки
        /// </summary>
        /// <param name="direction">Направление сделки которую защищем стопом</param>
        /// <param name="stopprice">Цена стопа</param>
        /// <returns>Цена</returns>
        decimal boundPriceByDirectionForStop(OrderDirections direction, decimal stopprice) {
            decimal delta = (direction == OrderDirections.Buy) ? -_cw.P().@base.StopPriceDelta:
                _cw.P().@base.StopPriceDelta;
            decimal price = stopprice + delta;
            return TradeHelper.CheckAndCorrectRangePrice(base.Security, price);
        }
        /// <summary>
        /// Восстановление значения объём после перезапуска
        /// </summary>
        void restoreVolume() {
            // если мы не в позиции, то ничего не делаем
            if ((marketState() & StrategyMarketState.InMarket) == 0) {
                return;
            }
            
            Position pos = Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                LoggingHelper.AddWarningLog(this,
                    "Не могу восстановить объём позиции");
                return;
            }
            base.Volume = Math.Abs(pos.CurrentValue);

            LoggingHelper.AddInfoLog(this, "Восстановлен рабочий объём: {0}", base.Volume);
        }  // restoreVolume

    }  // class BreakVolatilityStrategy
}  // namespace StrategBreakVolatilityHelper