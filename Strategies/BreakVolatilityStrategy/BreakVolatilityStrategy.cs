﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ecng.ComponentModel;
using Robot.Common.Equity;
using Robot.Common.Exchange.Rts;
using Robot.Configuration;
using Robot.Sms;
using Robot.Storage;
using Robot.Strategy;
using Robot.Strategy.Helper;
using StockSharp.Algo;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Strategies;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace StrategyPlugin {
namespace BreakVolatilityStrategy {


    public partial class BreakVolatilityStrategy : Strategy, IStrategyPlugin, ISecurityDownload {

        public event Action<SmsMessage>   Sms;
        /// <summary>
        /// Конструктор
        /// </summary>
        public BreakVolatilityStrategy() {
            basePlugin = new StrategyPluginBase(this);
            _aliveStatus = new BreakVolatilityStrategyAliveStatus();
            CancelOrdersWhenStopping = false;
            // время дата свечи по которой будет определяться цена открытия
            //open_price_date_ = DateTime.Now.Date;
            //open_price_date_ = open_price_date_.Add(Exchange.Rts.WorkingTime.Times[0].Min);
        }
        
        public CandleManager CandleManager {
            get {
                return this.basePlugin.CandleManager;
            }
            set {
                this.basePlugin.CandleManager = value;
            }
        }  // CandleManager

        public bool ManualLongSignal { get; set; }
        public bool ManualShortSignal { get; set; }
        
        public void RegisterAsLogSource(ConsoleLogListener console) {
            basePlugin.RegisterAsLogSource(console);
        }  // registerAsLogSource
        public void UnregisterAsLogSource() {
            basePlugin.UnregisterAsLogSource();
        }  // unregisterAsLogSource


        public IEnumerable<DownloadSecurity> DownloadSecurityList(System.Configuration.Configuration config) {
            var cw = new StrategyConfigWrapper<StrategyPlugin.BreakVolatilityStrategy.Parameters>(config);
            IList<DownloadSecurity> result = new List<DownloadSecurity>();

            result.Add(new DownloadSecurity() {
                Code = cw.P().@base.Security,
                TimeFrame  = TimeSpan.FromMinutes(cw.P().@base.TimeFrame),
                DaysToDownload = 1
            });

            return result.AsEnumerable();
        }  // DownloadSecurityList

        public void Save() {
            LoggingHelper.AddInfoLog(this, "Сохранение конфигурации...");
            saveInternalConfiguration();
            _cw.Save();
        }
        
        public void Load(System.Configuration.Configuration config) {
            LoggingHelper.AddInfoLog(this, "Установка параметров стратегии {0}...", Name);
            _cw = new StrategyConfigWrapper<StrategyPlugin.BreakVolatilityStrategy.Parameters>(config);
            
            base.Security = StrategyPluginHelper.LoadSecurity(base.Trader,
                _cw.P().@base.Security);
            base.Portfolio = StrategyPluginHelper.LoadPortfolio(base.Trader,
                _cw.P().@base.Portfolio);

	        _timeFrame = TimeSpan.FromMinutes(_cw.P().@base.TimeFrame);
            _aliveStatus.TimeFrame = _timeFrame;
            _candleSeries = StrategyPluginHelper.CheckAndRegisterSeries(
                this.CandleManager, base.Security, _timeFrame);
            /// узнаём как объём будем считать
            volume_manager_ = VolumeFactory.create(base.Portfolio, base.Security,
                this, _cw.P().@base.VolumeUnit, _cw.P().@base.Volume);
            loadInternalConfiguration();
        }  // Load
 
        void loadInternalConfiguration() {
            LoggingHelper.AddInfoLog(this, "Загрузка внутреней конфигурации стратегии");

            DateTime lastworkdate = lastTradeDay();
            
            LoggingHelper.AddDebugLog(this, "Последний рабочий день стратегии {0}", lastworkdate);
            DateTime dateFrom = lastworkdate + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime dateTo = lastworkdate + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Загрузка свечей в диапазоне [{0};{1}]",
                dateFrom, dateTo);
            
            // загружаем свечи предыдущего торгового дня
            IEnumerable<TimeFrameCandle> candles = global::Robot.Storage.Storage.Instance.
                LoadTimeFrameCandles(base.Security, _timeFrame, dateFrom, dateTo);
            StrategyPluginHelper.InsertCandlesInSeries(candles, this.CandleManager, _candleSeries);

            // Вычисляем уровни
            _l = findLevels(candles);
            
            if (!isLevelsActual()) {
                throw new Exception("Уровни цен не актуалны");
            }
            if (_cw.P().@internal.EnterMarketCounter != 0) {
                LoggingHelper.AddInfoLog(this, "Счётчик входов в позицию {0}",
                    _cw.P().@internal.EnterMarketCounter);
            }
            
            if (_cw.P().@internal.DaysInPosition > 0) {
                LoggingHelper.AddInfoLog(this,
                    "Счётчик дней в позиции не равен 0. Текущее значение {0}",
                    _cw.P().@internal.DaysInPosition);
                
                // Цена открытия берётся следующим днём за днём уровней
                DateTime lastOpenDate = WorkingCalendar.nextTradingDate(lastworkdate);
                open_price_date_ = openPriceDate(lastOpenDate);
                LoggingHelper.AddDebugLog(this, String.Format("Загрузка исторической свечи открытия {0}",
                    open_price_date_));
                // загружаем свечу открытия
                TimeFrameCandle openCandle = global::Robot.Storage.Storage.Instance.
                    LoadTimeFrameCandle(base.Security, _timeFrame, open_price_date_);

                StrategyPluginHelper.InsertCandlesInSeries(new TimeFrameCandle[] { openCandle },
                    this.CandleManager, _candleSeries);
                
                openCandle = _candleSeries.GetTimeFrameCandle(open_price_date_);
                if (openCandle == null) {
                    throw new Exception(String.Format("Нет свечи для времени: {0}", open_price_date_));
                }
                calculateLevels(openCandle.OpenPrice);
                //stop_loss_level_ = _cw.P().@internal.StopLossLevel;
                //stop_loss_size_ = _cw.P().@internal.StopLossSize;
                // загружаем старые уровни для передвижения стопов
                LoggingHelper.AddDebugLog(this,
                    "Предыдущие стоп-уровни: stop_loss_level_({0}), stop_loss_size_({1})",
                    stop_loss_level_, stop_loss_size_);

                LoggingHelper.AddInfoLog(this, "Восстанавливаю из стопа stop_loss_level_");
                Order o = findActiveStop();
                if (o != null) {
                    stop_loss_level_ = Convert.ToDecimal(o.Condition.Parameters["StopPrice"]);
                    LoggingHelper.AddInfoLog(this, "Восстановлен уровень из стоп-ордера",
                        stop_loss_level_);
                } else {
                    LoggingHelper.AddWarningLog(this,
                        "Не найден активный стоп ордер. stop_loss_level_ установлен без учёта сдвига стопа!");
                }

            }
        }  // loadInternalConfiguration
        /// <summary>
        /// Сохранение внутренего состояния стратегии
        /// </summary>
        public void saveInternalConfiguration() {
            // если рабочий день не закончился, то сохраняем только количество
            // входов в рынок, больше ничего не надо
            if (!WorkDayEnd()) {
                return;
            } else {
                // сбрасываем счётчик входа в рынок
                _cw.P().@internal.EnterMarketCounter = 0;
            }

            // Количество дней нахождения в позици
            // увеличиваем их, если остались в позиции
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                ++_cw.P().@internal.DaysInPosition;
                LoggingHelper.AddDebugLog(this,
                    "Увеличиваем счётчик нахождения в позиции до {0}, т.к. стратегия в состоянии {1}",
                    _cw.P().@internal.DaysInPosition, state);

            } else {
                // Вообще счётчик должен быть сброшен
                // но мозможны варианты при аварийном завершени
                if (_cw.P().@internal.DaysInPosition != 0) {
                    LoggingHelper.AddWarningLog(this,
                        "Счетчик дней удержания позиции не равен нулю. Видимо была аврийная остановка."
                        + " Счётчик сбрасывается в 0");
                    _cw.P().@internal.DaysInPosition = 0;
                }
            }

            DateTime begin = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime end = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Сохранение диапазона свечей [{0}, {1}]",
                begin, end);
            
            Range<DateTime> range = new Range<DateTime>(begin, end);
            IEnumerable<TimeFrameCandle> candles =
                _candleSeries.GetCandles<TimeFrameCandle>(range);

            global::Robot.Storage.Storage.Instance.SaveCandles(base.Security, candles,
                _timeFrame, CandleStorageType.Min);
        }  // saveInternalIRobotStorage

        /// <summary>
        /// Поиск уровней дня
        /// </summary>
        /// <param name="candles">Набор свечей</param>
        /// <returns></returns>
        HLOfDayValue findLevels(IEnumerable<Candle> candles) {
            if (candles.Count() == 0) {
                throw new ArgumentException("Пустой список свечей для вычисления уровней");
            }
          
            Candle first = candles.First();
            LoggingHelper.AddDebugLog(this, "Первая свеча по которой определяется HL за день {0}",
                first.OpenTime);
            
            Candle last = candles.Last();
            LoggingHelper.AddDebugLog(this, "Последняя свеча по которой определяется HL за день{0}",
                last.OpenTime);

            // Убедимся, что диапазон свечей одного дня
            if (first.OpenTime.Date != last.OpenTime.Date) {
                throw new ArgumentException(String.Format("Диапазон свечей для вычисления уровней " +
                    "содержит свечи разных дней: {0}, {1}", first.OpenTime.Date, last.OpenTime.Date));
            }
            
            HLOfDayValue hlvalues = new HLOfDayValue();
            // Дата расчёта уровней
            hlvalues.Date = first.OpenTime.Date;
            foreach (Candle c in candles) {
                if (hlvalues.High < c.HighPrice) hlvalues.High = c.HighPrice;
                if (hlvalues.Low > c.LowPrice) hlvalues.Low = c.LowPrice;
            }

            LoggingHelper.AddDebugLog(this,
                "Вычисленные значения: H({0}), L({1})", hlvalues.High, hlvalues.Low);

            return hlvalues;
        }  // findLevels

        protected override void OnStopOrderFailed(OrderFail fail) {
            base.OnStopOrderFailed(fail);
        }
        public bool WorkDayEnd() {
            return StrategyPluginBase.workDayEnd(_cw.P().@base.EndStrategyDay);
        }  // workDayEnd

        public StrategyStateInfo State() {
            StrategyStateInfo info = new StrategyStateInfo() {
                Name = base.Name,
                Portfolio = base.Portfolio.Name,
                SecurityCode = base.Security.Code,
                MarketState = marketState(),
                ProcessState = base.ProcessState
            };

            info.AliveStatus = _aliveStatus;

            info.Extended = new Dictionary<string, string>() {
                {"Уровень high", high_price_level_.ToString()},
                {"Уровень low", low_price_level_.ToString()},
                {"Количество входов", _cw.P().@internal.EnterMarketCounter.ToString()}
            };

            decimal? vol = positionVolume();
            info.Volume = vol == null ? 0 : (decimal)vol;

            Order so = findActiveStop();
            if (so != null) {
                info.StopVolume = so.Volume;
            }

            return info;
        }  // State

        decimal? positionVolume() {
            Position pos = base.Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                return null;
            }
            LoggingHelper.AddDebugLog(this,
                "positionVolume::Текущае значении объёма позиции позиции: {0}", pos.CurrentValue);
            return pos.CurrentValue;
        }
        /// <summary>
        /// Загрузка ордеров стратегии
        /// </summary>
        /// <param name="neworders">Список ордеров</param>
        /// <param name="isstoporders">Флаг признак стопов</param>
        /// <returns></returns>
        protected override IEnumerable<Order> ProcessNewOrders(IEnumerable<Order> neworders,
            bool isstoporders) {
 
            if (_stopOrdersLoaded && _ordersLoaded) {
                return base.ProcessNewOrders(neworders, isstoporders);
            }
            LoggingHelper.AddDebugLog(this, "Обработка ордеров...");
            List<Order> result = new List<Order>();
            foreach (Order o in neworders) {
                string comment = o.Comment;
                if (comment.EndsWith("/")) {
                    comment = comment.TrimEnd('/');
                }
                if (comment != base.Name) continue;
                if (isstoporders && o.State == OrderStates.Active) {
                    LoggingHelper.AddDebugLog(this, "Обработка стоп ордера: {0}, {1}, {2}",
                        o.Comment, o.Id, o.TransactionId);
                    result.Add(o);

                    var ruleActivated = o.WhenActivated();
                    var ruleCancelFailed = o.WhenCancelFailed();
                    ruleActivated.Do(action_stopOrderActivated).Once().Apply(this)
                        .Exclusive(ruleCancelFailed);
                    ruleCancelFailed.Do(action_cancelFailed).Once().Apply(this)
                        .Exclusive(ruleActivated);
                    continue;
                }
                if (!isstoporders) {
                    LoggingHelper.AddDebugLog(this, "Обработка ордера: {0}, {1}, {2}", o.Comment,
                        o.Id, o.TransactionId);
                    result.Add(o);
                    continue;
                }
            }
            
            if (isstoporders) _stopOrdersLoaded = true;
            if (!isstoporders) _ordersLoaded = true;

            IEnumerable<Order> myorders = result.AsEnumerable<Order>();
            return myorders.Concat<Order>(base.ProcessNewOrders(neworders, isstoporders));

        }  // ProcessNewOrders
        
    }
}
}