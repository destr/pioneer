﻿using System;
using Robot.Common.Cli;
using Robot.Strategy;
using Robot.Strategy.Cli;
using StockSharp.Logging;

namespace StrategyPlugin {
namespace BreakVolatilityStrategy {
    /// <summary>
    /// Стратегия. содержащая методы cli
    /// </summary>
    partial class BreakVolatilityStrategy : ICliStrategy {
        public bool DoCliCommand(CommandDescription cmd) {
            return Robot.Common.Cli.Helper.InvokeCommand(this, cmd);
        }  // DoCommand
        public void LongCommand(string[] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в long");
            if (!checkAndCalcLevels()) {
                LoggingHelper.AddErrorLog(this,
                    "Уровни не рассчитаны. Команда игнорируется");
                return;
            }
            this.ManualLongSignal = true;
            tryEnterMarket();
        }  // LongCommand

        public void ShortCommand(string[] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в short");
            if (!checkAndCalcLevels()) {
                LoggingHelper.AddErrorLog(this,
                    "Уровни не рассчитаны. Команда игнорируется");
                return;
            }
            this.ManualShortSignal = true;
            tryEnterMarket();
        }  // ShortCommand
        
        public void ClosePositionCommand(string[] args) {
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) == 0) {
                throw new Exception(String.Format("Стратегия {0} не в позиции", base.Name));
            }
            exitMarket();
        }  // ClosePositionCommand

        public void SetBaseCandleTime(string datetime) {
            high_price_level_ = -1;
            low_price_level_ = -1;
            open_price_date_ = basePlugin.internalSetBaseCandleTime(datetime);
        }  // SetBaseCandleTime

        public void ReRegisterStop(string [] args) {
            if (args.Length == 0) {
                throw new ArgumentNullException("args");
            }
            decimal stopprice = Decimal.Parse(args[0]);
            LoggingHelper.AddInfoLog(this,
                "Ручная перерегистрация стопа и стоп-уровня(0) на значение: {1}", stop_loss_level_, stopprice);
            stop_loss_level_ = stopprice;
            reRegisterStop();
        }  // ReRegisterStop
    }  // class BreakVolatilityStrategy
}  // namespace BreakVolatilityStrategy
}  // namespace StrategyPlugin