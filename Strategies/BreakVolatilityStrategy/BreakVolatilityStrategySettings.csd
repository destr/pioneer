﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="790e2ae3-6930-4f14-84ee-48dfccbad181" namespace="StrategyPlugin.BreakVolatilityStrategy" xmlSchemaNamespace="urn:StrategyPlugin.BreakVolatilityStrategy" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
    <externalType name="Decimal" namespace="System" />
    <externalType name="VolumeUnit" namespace="Robot.Common.Types" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="Parameters" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="parameters">
      <elementProperties>
        <elementProperty name="base" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="base" isReadOnly="true">
          <type>
            <configurationElementMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/BaseParameters" />
          </type>
        </elementProperty>
        <elementProperty name="internal" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="internal" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/InternalParameters" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="BaseParameters">
      <attributeProperties>
        <attributeProperty name="LevelFactor" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="LevelFactor" isReadOnly="true">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="TimeFrame" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="TimeFrame" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopLossPercent" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopLossPercent" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopLossMul" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopLossMul" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopPriceDelta" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopPriceDelta" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="MaxEnterToMarket" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="MaxEnterToMarket" isReadOnly="false" documentation="Максимальное число входов в рынок">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="ExitTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ExitTime" isReadOnly="false" documentation="Время выхода из позиции">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="StartEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StartEnterTime" isReadOnly="false" documentation="Время начала входа в позици">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="DaysKeepPosition" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="DaysKeepPosition" isReadOnly="false" documentation="Количество дней в течение которых удерживать позицию">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndEnterTime" isReadOnly="false" documentation="Время окончания входа в позицию">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndStrategyDay" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndStrategyDay" isReadOnly="false" documentation="Время окончания работы стратегии">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeBegin" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeBegin" isReadOnly="false" documentation="Начало диапазона сохранения свечей">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeEnd" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeEnd" isReadOnly="false" documentation="Конец диапазона сохранения свечей">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="Volume" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Volume" isReadOnly="false" documentation="Значение объёма">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="VolumeUnit" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="VolumeUnit" isReadOnly="false" documentation="Единицы измерения объёма атрибута Volume">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/VolumeUnit" />
          </type>
        </attributeProperty>
        <attributeProperty name="Portfolio" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Portfolio" isReadOnly="false" documentation="Портфель который используется стратегией">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Security" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Security" isReadOnly="false" documentation="Инструмент используемый стратегией">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="InternalParameters">
      <attributeProperties>
        <attributeProperty name="EnterMarketCounter" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="EnterMarketCounter" isReadOnly="false" documentation="Количество попыток входа">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="DaysInPosition" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="DaysInPosition" isReadOnly="false" documentation="Количество дней в течение которых удерживается позиция" defaultValue="0">
          <type>
            <externalTypeMoniker name="/790e2ae3-6930-4f14-84ee-48dfccbad181/Int32" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>