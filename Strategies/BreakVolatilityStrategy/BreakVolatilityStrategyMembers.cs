﻿using System;
using Robot.Common.Equity;
using Robot.Configuration;
using Robot.Storage;
using Robot.Strategy;
using StockSharp.Algo.Candles;

namespace StrategyPlugin {
namespace BreakVolatilityStrategy {

    public sealed class BreakVolatilityStrategyAliveStatus : StrategyAliveStatus {
        /// <summary>
        /// Проверка активности стратегии
        /// </summary>
        /// <returns></returns>
        public override bool IsAlive() {
            DateTime dt = UpdateTime.Add(TimeSpan.FromSeconds(TimeFrame.TotalSeconds * 2));
            if (dt < DateTime.Now) {
                return false;
            }
            return true;
        }
        protected override void DoUpdateByStrategy() {
            // в той стратегии делать ничего не надо
        }
    };

    public partial class BreakVolatilityStrategy {
        /// <summary>
        /// Интервал свечей
        /// </summary>
        private TimeSpan _timeFrame;
        /// <summary>
        /// Серия свечей
        /// </summary>
        CandleSeries _candleSeries;
        /// <summary>
        /// Значения H, L предыдущего дня
        /// </summary>
        private HLOfDayValue   _l;
        /// <summary>
        /// Дата и время свечи, по которой определяется цена открытия
        /// </summary>
        private DateTime open_price_date_;
        /// <summary>
        /// Нижний уровень цены
        /// </summary>
        private decimal low_price_level_ = -1;
        /// <summary>
        /// Верхний уровень цены
        /// </summary>
        private decimal high_price_level_ = -1;
        /// <summary>
        /// Уровень стопа
        /// </summary>
        private decimal stop_loss_level_ = -1;
        /// <summary>
        /// Размер цены стопа рассчитанное значение
        /// </summary>
        private decimal stop_loss_size_ = -1;
        /// <summary>
        /// Базовый функционал плагина
        /// </summary>
        private StrategyPluginBase basePlugin = null;
        /// <summary>
        /// Менеджер расчёта объёма входа
        /// </summary>
        private IVolumeManager volume_manager_ = null;
        /// <summary>
        /// Количество предыдущних свечей
        /// </summary>
        private int number_previous_candles = 5;
        /// <summary>
        /// Время свечи, которую надо игнорировать
        /// </summary>
        private TimeSpan ignoring_candle_time = new TimeSpan(10, 0, 0);
        /// <summary>
        /// Признак загрузки стоп-ордеров
        /// </summary>
        private bool _stopOrdersLoaded = false;
        /// <summary>
        /// Признак закгрузи ордеров
        /// </summary>
        private bool _ordersLoaded = false;

        private StrategyConfigWrapper<Parameters> _cw;

        private BreakVolatilityStrategyAliveStatus _aliveStatus { get; set; }
    }
}  // namespace BreakVolatilityStrategy
}  // namespace StrategyPlugin