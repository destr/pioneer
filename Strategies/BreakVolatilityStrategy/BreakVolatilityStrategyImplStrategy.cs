﻿using System;
using System.Collections.Generic;
using System.Linq;
using Robot.Common.Exchange.Rts;
using Robot.Common.Helpers;
using Robot.Logging;
using Robot.Strategy;
using StockSharp.Algo;
using StockSharp.Algo.Candles;
using StockSharp.BusinessEntities;
using StockSharp.Logging;

namespace StrategyPlugin {
namespace BreakVolatilityStrategy {

    public partial class BreakVolatilityStrategy {

        protected override void OnStarted() {
            _candleSeries.WhenCandlesStarted().Do(NewCandle).Apply(this);
            //candle_manager_.Start(_candleSeries);
            this.WhenPositionChanged().Do(positionChanged).Apply(this);

            open_price_date_ = openPriceDate();
            LoggingHelper.AddInfoLog(this,
                "Информация о стратегии {0}", State());

            this.basePlugin.ExitTimer.Elapsed += TimeCheck;
            this.basePlugin.ExitTimer.Start();
            base.OnStarted();
        }

        void TimeCheck(object sender, System.Timers.ElapsedEventArgs e) {
            // По таймеру выходим только в случае если стратегия умерла
            if (_aliveStatus.IsAlive()) return;
            if (isExitTime(DateTime.Now)) {
                exitMarket();
                this.basePlugin.ExitTimer.Stop();
            }
        } // OnStarting
        
        protected override void OnStopping() {
            base.OnStopping();
            this.basePlugin.ExitTimer.Stop();
            this.CandleManager.Stop(_candleSeries);
        }

        private void NewCandle(Candle candle) {
            _aliveStatus.UpdateByStrategy();
            // убедимся, что уровни рассчитаны
            if (!checkAndCalcLevels()) {
                return;
            }
            tryEnterMarket();
        }
        /// <summary>
        /// Проверка актуальности уровней и их расчёт при необходимости
        /// </summary>
        /// <returns></returns>
        bool checkAndCalcLevels() {
        
            if (levelsCalculated()) return true;
            Candle opencandle = _candleSeries.GetTimeFrameCandle(open_price_date_);
            if (opencandle == null) {
                LoggingHelper.AddWarningLog(this,
                    "Нет свечи для времени: {0}", open_price_date_);
                if (WorkingCalendar.isTradingTime(DateTime.Now.TimeOfDay) &&
                    WorkingCalendar.isTradingDate(DateTime.Now)) {
                    throw new Exception(
                        String.Format("Нет свечи для времени: {0}", open_price_date_));
                }
                return false;
            }
            LoggingHelper.AddInfoLog(this,
                "Уровни не рассчитаны. Производим рассчёт по свече {0}", opencandle.OpenTime);

            calculateLevels(opencandle.OpenPrice);
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                LoggingHelper.AddInfoLog(this,
                    "Стратегия в позиции {0}. Видимо был перезапуск." +
                    " Восстанавливаем внутреннее состояние", state);
                if (!stopLevelsCalculated()) {
                    calcStopLevels(state);
                }
                // восстанавливаем объём
                restoreVolume();
            }
            return true;
        }  // checkAndCalcLevels
        
        private void positionChanged(decimal position) {
        }  // positionChanged

        void tryEnterMarket() {
            // получаем последние свечи
            IEnumerable<TimeFrameCandle> candlelist = candles();
            // проверяем, что самая свежая свеча имеет нужное время
            // т.е. она не старая
            TimeFrameCandle activecandle = candlelist.Last();
            
            DateTime boundmarkettime = _timeFrame.GetCandleBounds(Trader.GetMarketTime(
                ExchangeBoard.Forts.Exchange)).Min;
            if ((activecandle == null) 
                || (activecandle.OpenTime < boundmarkettime)) {
                return;
            }
            // игнорируем первую свечу
            if (activecandle.OpenTime.TimeOfDay == ignoring_candle_time) {
                return;
            }
            foreach (Candle c in candlelist) {  
                LoggingHelper.AddDebugLog(this, "tryEnterCandles: {0}", c.OpenTime);
            }
            StrategyMarketState state = marketState();
            if (isExitTime(activecandle.OpenTime)) {
                // проверяем, действительно ли надо выходить
                if (_cw.P().@internal.DaysInPosition == _cw.P().@base.DaysKeepPosition) {
                    exitMarket();
                }
                return;
            }
            if (candlelist.Count() < number_previous_candles) {
                LoggingHelper.AddErrorLog(this,
                    "Не удалось получить необходимое количество исторических свечей: {0}",
                    number_previous_candles);
                return;
            }
            /// если мы в позиции, то передвигаем стопы
            if ((state & StrategyMarketState.InMarket) != 0) {
                recalcStopLevels(candlelist);
                return;
            }                                          
            // проверяем можем ли мы входить
            if (!canEnter()) {
                return;
            }

            if (haveLongSignal(candlelist)) {
                base.Volume = volume_manager_.maxVolume(activecandle.OpenPrice, stop_loss_level_);
                registerEnterOrders(OrderDirections.Buy);

            } else if (haveShortSignal(candlelist)) {
                base.Volume = volume_manager_.maxVolume(activecandle.OpenPrice, stop_loss_level_);
                registerEnterOrders(OrderDirections.Sell);
            }
        }  // tryEnterMarket
      
        void registerEnterOrders(OrderDirections direction) {
            Order order = createLimitOrder(direction);
            var ruleRegistered = order.WhenRegistered();
            var ruleRegisterFailed = order.WhenRegisterFailed();

            ruleRegistered.Do(action_orderRegistered)
                          .Apply(this).Exclusive(ruleRegisterFailed);
            ruleRegisterFailed.Do(action_orderRegisterFailed)
                               .Apply(this).Exclusive(ruleRegistered);
            
            this.RegisterOrder(order);
            ++_cw.P().@internal.EnterMarketCounter;
            _cw.Save();
            //enter_market_counter_++;
      
        } // registerEnterOrders
        
        /// <summary>
        /// Проверка наличия сигнала в лонг
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveLongSignal(IEnumerable<TimeFrameCandle> candles) {
            Candle active   = candles.ElementAt(4);
            Candle prev_0   = candles.ElementAt(3);
            Candle prev_1   = candles.ElementAt(2);
            Candle prev_2   = candles.ElementAt(1);
            Candle prev_3   = candles.ElementAt(0);

            decimal stoplosssize = stopLossSize();

            //High[bar - 1] > UpLevel &&
            // Open[bar - 1] < UpLevel &
            // Close[bar - 2] < UpLevel &&
            // Close[bar - 3] < UpLevel & (Open[bar] > (UpLevel - (UpLevel - DownLevel) * sl_size)))
            if (((prev_0.HighPrice > high_price_level_)
                && (prev_0.OpenPrice < high_price_level_)
                && (prev_1.ClosePrice < high_price_level_)
                && (prev_2.ClosePrice < high_price_level_)
                && (active.OpenPrice > (high_price_level_ - stoplosssize)))
                || this.ManualLongSignal) {
                LoggingHelper.AddInfoLog(this, "LONG!");
                this.ManualLongSignal = false;
                // покупашка
                // если покупаем, то цену выствляем максимальной. т.к. ордер лимитный
                stop_loss_size_ = stoplosssize;
                stop_loss_level_ = high_price_level_ - stop_loss_size_;

                return (true); 
            }
            
            return (false);
        }  // haveLongSignal
        /// <summary>
        /// Проверка наличия сигнала в шорт
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveShortSignal(IEnumerable<TimeFrameCandle> candles) {
            Candle active = candles.ElementAt(4);
            Candle prev_0 = candles.ElementAt(3);
            Candle prev_1 = candles.ElementAt(2);
            Candle prev_2 = candles.ElementAt(1);
            Candle prev_3 = candles.ElementAt(0);

            decimal stoplosssize = stopLossSize();

            if (((prev_0.LowPrice < low_price_level_)
                  && (prev_0.OpenPrice > low_price_level_)
                       && (prev_1.ClosePrice > low_price_level_)
                       && (prev_2.ClosePrice > low_price_level_)
                       && (active.OpenPrice < (low_price_level_ + stoplosssize)))
                       || this.ManualShortSignal) {
                this.ManualShortSignal = false;
                // продавашка
                LoggingHelper.AddInfoLog(this, "SHORT!");
                //eventStrategyShort.Invoke(new StrategyFactory.StrategyPluginInfo()
                //                            { name = base.Name } );
                stop_loss_size_ = stoplosssize;
                stop_loss_level_ = low_price_level_ + stop_loss_size_;
                

                return (true);
            }

            return (false);
        }  // haveShortSignal
        /// <summary>
        /// Пересчёт стопов и их перерегистрация в случае изменения уровня
        /// </summary>
        /// <param name="candles">Свечи</param>
        void recalcStopLevels(IEnumerable<TimeFrameCandle> candles) {
            Candle active = candles.ElementAt(4);
            Candle prev_0 = candles.ElementAt(3);
            Candle prev_1 = candles.ElementAt(2);
            Candle prev_2 = candles.ElementAt(1);
            Candle prev_3 = candles.ElementAt(0);
            // считаем уровень и округляем в большую сторону
            decimal level = stop_loss_size_ * _cw.P().@base.StopLossMul;
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.Long) != 0) {
                if (prev_0.ClosePrice > prev_1.ClosePrice) {
                    if (active.OpenPrice - stop_loss_level_ >= level) {
                        stop_loss_level_ = stop_loss_level_ + (prev_0.ClosePrice - prev_1.ClosePrice);
                        
                        // Перерегистрация стоп ордера с новой ценой
                        reRegisterStop();
                    }
                }
            } else if ((state & StrategyMarketState.Short) != 0) {
                if (prev_0.ClosePrice < prev_1.ClosePrice) {
                    if (stop_loss_level_ - active.OpenPrice >= level) {
                        stop_loss_level_ = stop_loss_level_ - (prev_1.ClosePrice - prev_0.ClosePrice);

                        // Перерегистрация ордера с новой ценой
                        reRegisterStop();
                    }
                }
            }
        }  // recalStopLevels

        void reRegisterStop() {
            decimal stopprice = TradeHelper.RoundPrice(base.Security, stop_loss_level_);
            LoggingHelper.AddDebugLog(this,
                "Попытка перерегистрации стопа с новой ценой: {0}", stopprice); 
            Order stop_order = findActiveStop();
            if (stop_order != null) {
                var order = stop_order.Clone();
                
                order.Condition.Parameters["StopPrice"] = stopprice;
                OrderDirections boundDirection = order.Direction == OrderDirections.Sell ?
                    OrderDirections.Buy : OrderDirections.Sell;
                order.Price = boundPriceByDirectionForStop(boundDirection, stopprice);
                order.ExpiryDate = DateTime.MaxValue;
                LoggingHelper.AddDebugLog(this, "reRegisterStop::new order: {0}", order);
                LoggingHelper.AddDebugLog(this, "reRegisterStop::old order: {0}", stop_order);

                var ruleOrderActivated = order.WhenActivated();
                var ruleOrderCancelFailed = order.WhenCancelFailed();

                ruleOrderActivated.Do(action_stopOrderActivated)
                    .Apply(this).Exclusive(ruleOrderCancelFailed);
                
                ruleOrderCancelFailed.Do(action_cancelFailed)
                    .Apply(this).Exclusive(ruleOrderActivated);
                base.ReRegisterOrder(stop_order, order);
            } else {
                LoggingHelper.AddWarningLog(this,
                    "reRegisterStop: Нет активного стоп ордера!");
            }
        }  // reRegisterStop
        Order findActiveStop() {

            return this.StopOrders.FirstOrDefault<Order>(o => o.State == OrderStates.Active);
        }
        /// <summary>
        /// Проверка возможности входа
        /// </summary>
        /// <returns>true если можно входить</returns>
        bool canEnter() {
            if (_cw.P().@internal.EnterMarketCounter >= _cw.P().@base.MaxEnterToMarket) {
                LoggingHelper.AddInfoLog(this,
                    "Превышено максимальное({0}) количество входов в рынок",
                    _cw.P().@base.MaxEnterToMarket);
                return (false);
            }
            TimeSpan mtime = Trader.GetMarketTime(ExchangeBoard.Forts.Exchange).TimeOfDay;
            if (mtime < _cw.P().@base.StartEnterTime || mtime > _cw.P().@base.EndEnterTime) {
                LoggingHelper.AddDebugLog(this,
                    "Текущее время рынка ({0}) не входит в интервал работы стратегии ({1}, {2})",
                    mtime, _cw.P().@base.StartEnterTime, _cw.P().@base.EndEnterTime);
                return (false);
            }             
            return (true);
        }  // canEnter

        /// <summary>
        /// Получение текущего состояния стратегии на рынке
        /// </summary>
        /// <returns></returns>
        StrategyMarketState marketState() {
            Position pos = base.Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                return StrategyMarketState.Unknown;
            }
            LoggingHelper.AddDebugLog(this,
                "marketState::Текущае значении позиции: {0}", pos.CurrentValue);
            if (pos.CurrentValue > 0) {
                return StrategyMarketState.Long;
            } else if (pos.CurrentValue < 0) {
                return StrategyMarketState.Short;
            }

            return StrategyMarketState.Free;
        }  // marketState
        /// <summary>
        /// Получение свечей необходмых для анализа сигналов
        /// </summary>
        /// <returns>Список свечей</returns>
        IEnumerable<TimeFrameCandle> candles() {
            IEnumerable<TimeFrameCandle> list =
            _candleSeries.GetCandles<TimeFrameCandle>(number_previous_candles);

            return list;
        }  // candles
        /// <summary>
        /// Проверка времени окончания работы стратегии
        /// </summary>
        /// <param name="datetime">Дата</param>
        /// <returns>true если время окончания работы стратегии</returns>
        bool isExitTime(DateTime datetime) {
            TimeSpan time = datetime.TimeOfDay;
            if (time >= _cw.P().@base.ExitTime) {
                LoggingHelper.AddDebugLog(this,
                    "Рабочий день закончен. Время оконачания дня ({0}) текущее время ({1})",
                    _cw.P().@base.ExitTime, time);
                return (true);
            }
            return (false);
        }  // isExitTime
        /// <summary>
        /// Рассчёт потерь на сделку при выходе по стопу
        /// </summary>
        /// <returns></returns>
        decimal stopLossSize() {
            decimal stoplosssize = (high_price_level_ - low_price_level_) * _cw.P().@base.StopLossPercent;
            return stoplosssize;
        }  // stopLossSize
        /// <summary>
        /// Рассчёт внутренних состояние стратегии
        /// </summary>
        /// <param name="state">Состояние стратегии на рынке</param>
        void calcStopLevels(StrategyMarketState state) {
            stop_loss_size_ = stopLossSize();
            if ((state & StrategyMarketState.Short) != 0) {
                decimal level = getLevelFromStop();
                if (level != 0) {
                    stop_loss_level_ = level;
                } else {
                    stop_loss_level_ = low_price_level_ + stop_loss_size_;
                }
            }
            if ((state & StrategyMarketState.Long) != 0) {
                decimal level = getLevelFromStop();
                if (level != 0) {
                    stop_loss_level_ = level;
                } else {
                    stop_loss_level_ = high_price_level_ - stop_loss_size_;
                }
            }

            LoggingHelper.AddDebugLog(this,
                "Рассчитанные значения: stop_loss_size_ {0}, stop_loss_level_ {1}",
                stop_loss_size_, stop_loss_level_);

        }  // calcInternalValues
        decimal getLevelFromStop() {
            Order order = findActiveStop();
            if (order != null) {
                RobotLogger.Instance.AddDebugLog(
                    "Восстанавливаем уровень по стопу");

                object val = order.Condition.Parameters["StopPrice"];
                stop_loss_level_ = Convert.ToDecimal(val);
                return stop_loss_level_;
            }
            return 0;
        }


    } // class BreakVolatilityStrategy
}  // namespace BreakVolatilityStrategy
}  // namespace StrategyPlugin