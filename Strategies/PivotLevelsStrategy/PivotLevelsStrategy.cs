﻿namespace StrategyPlugin.PivotLevelsStrategy {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Ecng.ComponentModel;
    using global::Robot.Common.Equity;
    using global::Robot.Configuration;
    using global::Robot.Sms;
    using global::Robot.Storage;
    using global::Robot.Strategy;
    using global::Robot.Strategy.Helper;
    using StockSharp.Algo;
    using StockSharp.Algo.Candles;
    using StockSharp.Algo.Strategies;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;


    public partial class PivotLevelsStrategy : Strategy, IStrategyPlugin, ISecurityDownload {
        public event Action<SmsMessage>   Sms;
        /// <summary>
        /// Конструктор
        /// </summary>
        public PivotLevelsStrategy() {
            _aliveStatus = new PivotLevelsStrategyAliveStatus();
            this.basePlugin = new StrategyPluginBase(this);
            CancelOrdersWhenStopping = false;

            // время дата свечи по которой будут рассчитываться уровни
            setClosePriceDate();
        }

        void Trader_NewStopOrders(IEnumerable<Order> obj) {
            Console.WriteLine("throw new NotImplementedException()");
            // ищем ордера, которые принадлежат нам, но ещё не входят в список стратегии
            // Все мои оредра
            var mystops = obj.Where(o => o.Comment.TrimEnd('/') == base.Name)
                /// которые мне ещё не принадлежат
                .Where(o => !this.StopOrders.Contains(o));

            foreach (Order o in mystops) {
                Console.WriteLine("My order: {0}", o);
            }
        }
        
        public CandleManager CandleManager {
            get {
                return this.basePlugin.CandleManager;
            }
            set {
                this.basePlugin.CandleManager = value;
            }
        }  // CandleManager

        public bool ManualLongSignal { get; set; }

        public bool ManualShortSignal { get; set; }
        
        public void RegisterAsLogSource(ConsoleLogListener console) {
            basePlugin.RegisterAsLogSource(console);
        }  // registerAsLogSource
        public void UnregisterAsLogSource() {
            basePlugin.UnregisterAsLogSource();
        }  // unregisterAsLogSource

        public IEnumerable<DownloadSecurity> DownloadSecurityList(System.Configuration.Configuration config) {
            var cw = new StrategyConfigWrapper<StrategyPlugin.PivotLevelsStrategy.Parameters>(config);

            IList<DownloadSecurity> result = new List<DownloadSecurity>();

            DownloadSecurity ds = new DownloadSecurity() {
                Code = cw.P().@base.Security,
                TimeFrame = TimeSpan.FromMinutes(cw.P().@base.TimeFrame),
                DaysToDownload = 1
            };
            result.Add(ds);
            return result.AsEnumerable();
        }  // DownloadSecurityList
       
        public void Save() {
            LoggingHelper.AddInfoLog(this, "Сохранение конфигурации...");
            saveInternalConfiguration();
            _cw.Save();
        }
        public void Load(System.Configuration.Configuration config) {
            LoggingHelper.AddInfoLog(this, "Установка параметров стратегии {0}...", Name);
            _cw = new StrategyConfigWrapper<StrategyPlugin.PivotLevelsStrategy.Parameters>(config);
            base.Security = StrategyPluginHelper.LoadSecurity(base.Trader,
                _cw.P().@base.Security);
            base.Portfolio = StrategyPluginHelper.LoadPortfolio(base.Trader,
                _cw.P().@base.Portfolio);
            
            _timeFrame = TimeSpan.FromMinutes(_cw.P().@base.TimeFrame);
            _aliveStatus.TimeFrame = _timeFrame;
            // проверим что ещЁ не зарегистрированы свечи 
            _candleSeries = StrategyPluginHelper.CheckAndRegisterSeries(this.CandleManager,
                base.Security, _timeFrame);
            
            /// создаем менеджер объёма
            volume_manager_ = VolumeFactory.create(base.Portfolio, base.Security,
                this, _cw.P().@base.VolumeUnit, _cw.P().@base.Volume);

            loadInternalConfiguration();
        }  // Load

        /// <summary>
        /// Загрузка внутреней конфигурации стратегии
        /// </summary>
        public void loadInternalConfiguration() {
            LoggingHelper.AddInfoLog(this,
                "Загрузка внутреней конфигурации стратегии");

            DateTime lastworkdate = global::Robot.Common.Exchange.Rts.WorkingCalendar.previousTradingDate();
            LoggingHelper.AddDebugLog(this, "Last work day {0}", lastworkdate);
            DateTime dateFrom = lastworkdate + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime dateTo = lastworkdate + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Load candles from date range [{0};{1}]",
                dateFrom, dateTo);
            
            // загружаем свечи предыдущего торгового дня
            IEnumerable<TimeFrameCandle> candles = global::Robot.Storage.Storage.Instance.
                LoadTimeFrameCandles(base.Security, _timeFrame, dateFrom, dateTo);

            StrategyPluginHelper.InsertCandlesInSeries(candles, this.CandleManager, _candleSeries);
            _l = findLevels(candles);

            if (!isLevelsActual()) {
                throw new Exception("Уровни цен не актуалны");
            }
        }  // loadInternalSettingsStorage
        public void saveInternalConfiguration() {
            if (!WorkDayEnd()) {
                return;
            } else {
                // сбрасываем признак разворота
                _cw.P().@internal.Overturn = false;
            }

            DateTime begin = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime end = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Save candle range {0}, {1}",
                begin, end);

            Range<DateTime> range = new Range<DateTime>(begin, end);
            IEnumerable<TimeFrameCandle> candles =
                _candleSeries.GetCandles<TimeFrameCandle>(range);

            global::Robot.Storage.Storage.Instance.SaveCandles(base.Security,
                candles, _timeFrame, CandleStorageType.Min);
            
        }  // saveInternalConfiguration
        /// <summary>
        /// Поиск уровней H,L дня
        /// </summary>
        /// <param name="settings">Свечи</param>
        HLOfDayValue findLevels(IEnumerable<Candle> candles) {
            if (candles.Count() == 0) {
                throw new ArgumentException(String.Format("Нет свечей для вычисления уровней"));
            }
      
            Candle first = candles.First();
            LoggingHelper.AddDebugLog(this, "First candle for HL {0}", first.OpenTime);
    
            Candle last = candles.Last();
            LoggingHelper.AddDebugLog(this, "Last candle for HL {0}", last.OpenTime);

            LoggingHelper.AddDebugLog(this, "Вычисляем H и L за время [{0},{1}]",
                first.OpenTime, last.OpenTime);

            if (first.OpenTime.Date != last.OpenTime.Date) {
                throw new ArgumentException(String.Format(
                    "Диапазон свечей для вычисления уровней содержит свечи разных дней {0}, {1}",
                    first.OpenTime, last.OpenTime));
            }

            HLOfDayValue hlvalues = new HLOfDayValue();
            hlvalues.Date = first.OpenTime.Date;

            foreach (Candle c in candles) {
                if (hlvalues.High < c.HighPrice) hlvalues.High = c.HighPrice;
                if (hlvalues.Low > c.LowPrice) hlvalues.Low = c.LowPrice;
            }

            LoggingHelper.AddDebugLog(this,
                "Вычисленные значения: H({0}), L({1})", hlvalues.High, hlvalues.Low);
            return hlvalues;
        }  // findLevels


        protected override void OnStopOrderFailed(OrderFail fail) {
            base.OnStopOrderFailed(fail);
        }
        public bool WorkDayEnd() {
            return StrategyPluginBase.workDayEnd(_cw.P().@base.EndStrategyDay);
        }  // workDayEnd
        public StrategyStateInfo State() {
            StrategyStateInfo info = new StrategyStateInfo() {
                Name = base.Name,
                Portfolio = base.Portfolio.Name,
                SecurityCode = base.Security.Code,
                MarketState = marketState(),
                ProcessState = base.ProcessState   
            };

            decimal? vol = positionVolume();
            info.Volume = vol == null ? 0 : (decimal)vol;

            Order so = findActiveStop();
            if (so != null) {
                info.StopVolume = so.Volume;
            }

            info.Extended = new Dictionary<string, string>() {
                {"Уровень resist", _resistPriceLevel.ToString()},
                {"Уровень suppor", _supportPriceLevel.ToString()},
                {"Разворот", _cw.P().@internal.Overturn.ToString()}
            };

            info.AliveStatus = _aliveStatus;
            
            return info;
        }  // MarketState
        /// <summary>
        /// Загрузка ордеров стратегии
        /// </summary>
        /// <param name="neworders">Список ордеров</param>
        /// <param name="isstoporders">Флаг признак стопов</param>
        /// <returns></returns>
        protected override IEnumerable<Order> ProcessNewOrders(
            IEnumerable<Order> neworders, bool isstoporders) {
             if (_stopOrdersLoaded && _ordersLoaded) {
                return base.ProcessNewOrders(neworders, isstoporders);
            }
            LoggingHelper.AddDebugLog(this, "Обработка ордеров...");
            List<Order> result = new List<Order>();
            foreach (Order o in neworders) {
                string comment = o.Comment;
                if (comment.EndsWith("/")) {
                    comment = comment.TrimEnd('/');
                }
                if (comment != base.Name) continue;

                if (isstoporders && o.State == OrderStates.Active) {
                    LoggingHelper.AddDebugLog(this, "Обработка стоп ордера: {0}, {1}, {2}",
                        o.Comment, o.Id, o.TransactionId);
                    result.Add(o);

                    var ruleActivated = o.WhenActivated();
                    var ruleCancelFailed = o.WhenCancelFailed();
                    ruleActivated.Do(action_stopOrderActivated).Once().Apply(this)
                        .Exclusive(ruleCancelFailed);
                    ruleCancelFailed.Do(action_cancelFailed)
                        .Once().Apply(this).Exclusive(ruleActivated);
                    continue;
                }
                if (!isstoporders) {
                    LoggingHelper.AddDebugLog(this, "Обработка ордера: {0}, {1}, {2}",
                        o.Comment, o.Id, o.TransactionId);
                    result.Add(o);
                    continue;
                }
            }

            if (isstoporders) _stopOrdersLoaded = true;
            if (!isstoporders) _ordersLoaded = true;

            IEnumerable<Order> myorders = result.AsEnumerable<Order>();
            return myorders.Concat<Order>(base.ProcessNewOrders(neworders, isstoporders));
        }  // ProcessNewOrders
        
    }
}