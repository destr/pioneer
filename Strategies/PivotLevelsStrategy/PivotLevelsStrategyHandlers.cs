﻿namespace StrategyPlugin {
namespace PivotLevelsStrategy {
    using Robot.Sms;
    using Robot.Strategy;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;

    partial class PivotLevelsStrategy {
        /// <summary>
        /// Действие на регистрацию ордера
        /// </summary>
        /// <param name="order">Ордер</param>
        void action_orderRegistered(Order order) {
            // ставим стоп, раз заявка успешно выполнена
            registerStop(order.Direction);

            /// развернулись, смс не нужна
            if (_cw.P().@internal.Overturn == true)
                return;
            var info = new StrategyPositionInfo() {
                Sender = this.Name,
                price = order.Price,
                Direction = order.Direction
            };
            Sms.Invoke(info);
        }  // action_orderRegistered
        /// <summary>
        /// Обработка события активации стоп ордера
        /// </summary>
        /// <param name="order"></param>
        void action_stopOrderActivated(Order order) {

            if (_cw.P().@internal.Overturn == false) {
                LoggingHelper.AddInfoLog(this,
                    "Активация стопа. Переворот позиций");
                _cw.P().@internal.Overturn = true;
                _cw.Save();
                Sms.Invoke(new SmsMessage() {
                    Sender = base.Name,
                    Text = " Overturn"
                });

                

                // Исполненный оред, знает направление сделки которую совершал
                OrderDirections toDirection = order.Direction;
                LoggingHelper.AddDebugLog(this,
                    "Направление сделки при перевороте {0}", toDirection);
                // Разворачиваемся в лонг
                if (toDirection == OrderDirections.Buy) {
                    // Считаем  новое значение стоп-уровня
                    _stopLossLevel -= _stopLossSize;
                    LoggingHelper.AddInfoLog(this,
                        "Разворот short -> long. " +
                        "Стоп уровень _stopLossLevel({0}) -= _stopLossSize({1})", _stopLossLevel, _stopLossSize);
                    //registerStop(OrderDirections.Buy);
                    // объём которым надо выходить в конце дня
                    base.Volume = _overturnVolume;
                } else if (toDirection == OrderDirections.Sell) {
                    // Разворачиваемся в шорт
                    _stopLossLevel += _stopLossSize;
                    LoggingHelper.AddInfoLog(this,
                        "Разворот long -> short." +
                        "Стоп уровень _stopLossLevel({0}) += _stopLossSize({1})", _stopLossLevel, _stopLossSize);
                    //registerStop(OrderDirections.Sell);
                    // объём которым надо выходить в конец дня
                    base.Volume = _overturnVolume;
                    //return;
                }

                /// Закрыли позицию и входим дополнительным объёмо для разворота
                registerEnterOrders(order.Direction);

                //LoggingHelper.AddErrorLog(this,
                //    "Отработал режим переворот, но не удалось понять в какой мы позиции {0}", toDirection);

                return;
            }

            LoggingHelper.AddDebugLog(this, "Выход из позии по стопу");
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.Activated
            });

        }  // action_stopOrderActivated
        /// <summary>
        /// Событие на ошибку регистрации ордера
        /// </summary>
        /// <param name="order"></param>
        void action_orderRegisterFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка регистрации ордера: {0}",
                order.Error.Message);
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = false,
                Action = OrderInfo.OrderAction.Failed
            });
        }  // action_orderRegisterFailed
        /// <summary>
        /// Событие на ошибку регистрации стоп ордера
        /// </summary>
        /// <param name="order"></param>
        void action_stopOrderRegisterFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка регистрации стоп-ордера: {0}",
                order.Error.Message);
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.Failed
            });
        }  // action_stopOrderRegisterFailed

        void action_cancelFailed(OrderFail order) {
            LoggingHelper.AddErrorLog(this, "Ошибка отмены ордера: {0}", 
                order.Error.Message);
            Sms.Invoke(new OrderInfo() {
                Sender = base.Name,
                IsStopOrder = true,
                Action = OrderInfo.OrderAction.CancelFailed
            });
        }  // action_cancelFailed

    }  // class PivotLevelsStrategy
}  // namespace PivotLevelsStrategy
}  // namespace StrategyPlugin