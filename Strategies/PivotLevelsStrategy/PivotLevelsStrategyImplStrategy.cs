﻿namespace StrategyPlugin {
namespace PivotLevelsStrategy {

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Timers;
    using Robot.Common.Exchange.Rts;
    using Robot.Common.Helpers;
    using Robot.Logging;
    using Robot.Strategy;
    using StockSharp.Algo;
    using StockSharp.Algo.Candles;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;


    public partial class PivotLevelsStrategy {

        protected override void OnStarted() {
            _candleSeries.WhenCandlesStarted().Do(NewCandle).Apply(this);
            this.WhenPositionChanged().Do(positionChanged).Apply(this) ;
            setClosePriceDate();

            LoggingHelper.AddInfoLog(this,
                "Информация о стратегии {0}", State());

            // Устанавливаем таймер выхода
            this.basePlugin.ExitTimer.Elapsed += new ElapsedEventHandler(TimeCheck);
            this.basePlugin.ExitTimer.Start();
            // восстанавливаем объём
            restoreVolume();
            //base.Trader.NewStopOrders += Trader_NewStopOrders;
            base.OnStarted();
        } // OnStarting

        public void TimeCheck(object obj, ElapsedEventArgs args) {
            if (_aliveStatus.IsAlive()) return;
            if (isExitTime(DateTime.Now)) {
                this.basePlugin.ExitTimer.Stop();
                exitMarket();
            }
        }

        protected override void OnStopping() {
            base.OnStopping();
            this.basePlugin.ExitTimer.Stop();
            this.CandleManager.Stop(_candleSeries);
        }

        private void NewCandle(Candle candle) {
            _aliveStatus.UpdateByStrategy();
            if (!checkAndCalcLevels()) {
                return;
            }
            tryEnterMarket();
        }
        /// <summary>
        /// Проверка уровней и расчёт при необходимости
        /// </summary>
        bool checkAndCalcLevels() {
            // убедимся, что уровни рассчитаны
            if (levelsCalculated()) return true;
            Candle closecandle = _candleSeries.GetTimeFrameCandle(close_price_date_);
            if (closecandle == null) {
                LoggingHelper.AddInfoLog(this,
                    "Нет свечи для времени: {0}", close_price_date_);
                if (WorkingCalendar.isTradingTime(DateTime.Now.TimeOfDay) &&
                    WorkingCalendar.isTradingDate(DateTime.Now)) {
                    throw new Exception(
                        String.Format("Нет свечи для времени: {0}", close_price_date_));
                }
                return false;
            }
            LoggingHelper.AddInfoLog(this,
                "Уровни не рассчитаны. Производим рассчёт по свече {0}", closecandle.OpenTime);

            calculateLevels(closecandle.ClosePrice);
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                LoggingHelper.AddWarningLog(this,
                    "Стратегия в позиции {0}. Видимо был перезапуск." +
                    " Восстанавливаем внутреннее состояние", state);
                calcInternalValues(state);
            }
            return true;
        }
        private void positionChanged(decimal position) {
        }  // positionChanged

        void tryEnterMarket() {
            // получаем последние свечи
            IEnumerable<TimeFrameCandle> candlelist = candles();
            // проверяем, что самая свежая свеча имеет нужное время
            // т.е. она не старая
            TimeFrameCandle activecandle = candlelist.Last();

            DateTime boundmarkettime = _timeFrame.GetCandleBounds(
                Trader.GetMarketTime(ExchangeBoard.Forts.Exchange)).Min;
            if ((activecandle == null) 
                || (activecandle.OpenTime < boundmarkettime)) {
                return;
            }
            // игнорируем первую свечу
            if (activecandle.OpenTime.TimeOfDay == ignoring_candle_time) {
                return;
            }
            foreach (Candle c in candlelist) {  
                LoggingHelper.AddDebugLog(this, "tryEnterCandles: {0}", c.OpenTime);
            }
            StrategyMarketState state = marketState();
            if (isExitTime(activecandle.OpenTime)) {
                exitMarket();
                return;
            }
            if (candlelist.Count() < number_previous_candles) {
                LoggingHelper.AddErrorLog(this,
                    "Не удалось получить необходимое количество исторических свечей: {0}",
                    number_previous_candles);
                return;
            }
            /// если мы в позиции, то передвигаем стопы
            if ((state & StrategyMarketState.InMarket) != 0) {
                recalcStopLevels(candlelist);
                return;
            }                                          
            // проверяем можем ли мы входить
            if (!canEnter()) {
                return;
            }

            if (haveLongSignal(candlelist)) {
                base.Volume = volume_manager_.maxVolume(activecandle.OpenPrice, _stopLossLevel);
                _overturnVolume = volume_manager_.maxVolumeAfterStop(
                    activecandle.OpenPrice, _stopLossLevel, base.Volume, 
                    _stopLossLevel + _stopLossSize);
                registerEnterOrders(OrderDirections.Buy);

            } else if (haveShortSignal(candlelist)) {
                base.Volume = volume_manager_.maxVolume(activecandle.OpenPrice, _stopLossLevel);
                
                _overturnVolume = volume_manager_.maxVolumeAfterStop(
                    activecandle.OpenPrice, _stopLossLevel, base.Volume,
                    _stopLossLevel - _stopLossSize);
                registerEnterOrders(OrderDirections.Sell);
            }

        }  // tryEnterMarket
        void registerEnterOrders(OrderDirections direction) {
            Order order = createLimitOrder(direction);
            var ruleRegistered = order.WhenRegistered();
            var ruleRegisterFailed = order.WhenRegisterFailed();
            ruleRegistered
                .Do(action_orderRegistered).Once().Apply(this)
                .Exclusive(ruleRegisterFailed);
            ruleRegisterFailed
            .Do(action_orderRegisterFailed).Once().Apply(this).Exclusive(ruleRegistered);
            
            this.RegisterOrder(order);
            LoggingHelper.AddDebugLog(this, "Ордер входа в рынок {0}", order);
        } // registerEnterOrders
        
        /// <summary>
        /// Проверка наличия сигнала в лонг
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveLongSignal(IEnumerable<TimeFrameCandle> candles) {
            Candle active   = candles.ElementAt(4);
            Candle prev_0   = candles.ElementAt(3);
            Candle prev_1   = candles.ElementAt(2);
            Candle prev_2   = candles.ElementAt(1);
            //Candle prev_3   = candles.ElementAt(0);

            decimal stoplosssize = stopLossSize();
            if (((prev_0.HighPrice > _resistPriceLevel)
                && (prev_0.OpenPrice < _resistPriceLevel)
                && (prev_1.ClosePrice < _resistPriceLevel)
                && (prev_2.ClosePrice < _resistPriceLevel)
                && (active.OpenPrice > _resistPriceLevel))
                || this.ManualLongSignal) {
                LoggingHelper.AddInfoLog(this, "LONG!");
                this.ManualLongSignal = false;
                // покупашка
                // если покупаем, то цену выствляем максимальной. т.к. ордер лимитный
                _stopLossSize = stoplosssize;
                _stopLossLevel = _resistPriceLevel - _stopLossSize;

                return (true); 
            }
            
            return (false);
        }  // haveLongSignal
        /// <summary>
        /// Проверка наличия сигнала в шорт
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveShortSignal(IEnumerable<TimeFrameCandle> candles) {
            Candle active = candles.ElementAt(4);
            Candle prev_0 = candles.ElementAt(3);
            Candle prev_1 = candles.ElementAt(2);
            Candle prev_2 = candles.ElementAt(1);
            //Candle prev_3 = candles.ElementAt(0);

           decimal stoplosssize = stopLossSize();

            if (((prev_0.LowPrice < _supportPriceLevel)
                 && (prev_0.OpenPrice > _supportPriceLevel)
                 && (prev_1.ClosePrice > _supportPriceLevel)
                 && (prev_2.ClosePrice > _supportPriceLevel)
                 && (active.OpenPrice < _supportPriceLevel))
                     || this.ManualShortSignal) {
                this.ManualShortSignal = false;
                // продавашка
                LoggingHelper.AddInfoLog(this, "SHORT!");
                _stopLossSize = stoplosssize;
                _stopLossLevel = _supportPriceLevel + _stopLossSize;
                
                return (true);
            }

            return (false);
        }  // haveShortSignal
        /// <summary>
        /// Пересчёт стопов и их перерегистрация в случае изменения уровня
        /// </summary>
        /// <param name="candles">Свечи</param>
        void recalcStopLevels(IEnumerable<TimeFrameCandle> candles) {
            Candle active = candles.ElementAt(4);
            Candle prev_0 = candles.ElementAt(3);
            Candle prev_1 = candles.ElementAt(2);
            //Candle prev_2 = candles.ElementAt(1);
            //Candle prev_3 = candles.ElementAt(0);
            // считаем уровень и округляем в большую сторону
            decimal level = _stopLossSize * _cw.P().@base.StopLossMul;

            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.Long) != 0) {
                if (prev_0.ClosePrice > prev_1.ClosePrice) {
                    if (active.OpenPrice - _stopLossLevel >= level) {
                        _stopLossLevel = _stopLossLevel + (prev_0.ClosePrice - prev_1.ClosePrice);

                        // Перерегистрация стоп ордера с новой ценой
                        reRegisterStop();
                    }
                }
            } else if ((state & StrategyMarketState.Short) != 0) {
                if (prev_0.ClosePrice < prev_1.ClosePrice) {
                    if (_stopLossLevel - active.OpenPrice >= level) {
                        _stopLossLevel = _stopLossLevel - (prev_1.ClosePrice - prev_0.ClosePrice);

                        // Перерегистрация ордера с новой ценой
                        reRegisterStop();
                    }
                }
            }
        }  // recalStopLevels

        void reRegisterStop() {
            decimal stopprice = TradeHelper.RoundPrice(base.Security, _stopLossLevel);
            LoggingHelper.AddDebugLog(this,
                "Попытка перерегистрации стопа с новой ценой: {0}", stopprice);
            Order stop_order = findActiveStop();
            if (stop_order != null) {
                var order = stop_order.Clone();
                
                order.Condition.Parameters["StopPrice"] = stopprice;
                OrderDirections boundDirection = order.Direction == OrderDirections.Sell ? 
                    OrderDirections.Buy : OrderDirections.Sell;
                order.Price = boundPriceByDirectionForStop(boundDirection, stopprice);
                LoggingHelper.AddDebugLog(this, "reRegisterStop::new order: {0}", order);
                LoggingHelper.AddDebugLog(this, "reRegisterStop::old order: {0}", stop_order);

                var ruleActivated = order.WhenActivated();
                var ruleCancelFailed = order.WhenCancelFailed();
                
                ruleActivated.Do(action_stopOrderActivated).Once().Apply(this)
                    .Exclusive(ruleCancelFailed);
                ruleCancelFailed.Do(action_cancelFailed).Once().Apply(this)
                    .Exclusive(ruleActivated);
                base.ReRegisterOrder(stop_order, order);
            } else {
                LoggingHelper.AddWarningLog(this,
                    "reRegisterStop: Нет активного стоп ордера!");
            }
        }  // reRegisterStop
        Order findActiveStop() {
            return this.StopOrders.FirstOrDefault<Order>(o => o.State == OrderStates.Active);
        }
        /// <summary>
        /// Проверка возможности входа
        /// </summary>
        /// <returns>true если можно входить</returns>
        bool canEnter() {
            TimeSpan mtime = Trader.GetMarketTime(
                ExchangeBoard.Forts.Exchange).TimeOfDay;
            if (mtime < _cw.P().@base.StartEnterTime || mtime > _cw.P().@base.EndEnterTime) {
                LoggingHelper.AddDebugLog(this,
                    "Текущее время рынка ({0}) не входит в интервал работы стратегии ({1}, {2})",
                    mtime, _cw.P().@base.StartEnterTime, _cw.P().@base.EndEnterTime);
                return (false);
            }
            if (_cw.P().@internal.Overturn) {
                LoggingHelper.AddDebugLog(this,
                    "Вход не возможен, т.к. уже был разворот");
                return false;
            }
            return true;
        }  // canEnter

        /// <summary>
        /// Получение текущего состояния стратегии на рынке
        /// </summary>
        /// <returns></returns>
        StrategyMarketState marketState() {
            decimal? currVol = positionVolume();
            if (currVol == null) {
                LoggingHelper.AddErrorLog(this,
                    "Не удалось определить текущий объём позиции");
                return StrategyMarketState.Unknown;
            }
            LoggingHelper.AddDebugLog(this,
                "marketState::Текущае значении позиции: {0}", currVol);
            if (currVol > 0) {
                return StrategyMarketState.Long;
            } else if (currVol < 0) {
                return StrategyMarketState.Short;
            }

            return StrategyMarketState.Free;
        }  // marketState

        decimal? positionVolume() {
            Position pos = base.Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                return null;
            }
            LoggingHelper.AddDebugLog(this,
                "positionVolume::Текущае значении объёма позиции позиции: {0}", pos.CurrentValue);
            return pos.CurrentValue;
        }
        /// <summary>
        /// Получение свечей необходмых для анализа сигналов
        /// </summary>
        /// <returns>Список свечей</returns>
        IEnumerable<TimeFrameCandle> candles() {
            IEnumerable<TimeFrameCandle> list =
            _candleSeries.GetCandles<TimeFrameCandle>(number_previous_candles);

            return list;

        }  // candles
        /// <summary>
        /// Проверка времени окончания работы стратегии
        /// </summary>
        /// <param name="datetime">Дата</param>
        /// <returns>true если время окончания работы стратегии</returns>
        bool isExitTime(DateTime datetime) {
            TimeSpan time = datetime.TimeOfDay;
            if (time >= _cw.P().@base.ExitTime) {
                LoggingHelper.AddDebugLog(this,
                    "Рабочий день закончен. Время оконачания дня ({0}) текущее время ({1})",
                    _cw.P().@base.ExitTime, time);
                return (true);
            }
            return (false);
        }  // isExitTime

        /// <summary>
        /// Рассчёт потерь на сделку при выходе по стопу
        /// </summary>
        /// <returns></returns>
        decimal stopLossSize() {
            decimal stoplosssize = (_resistPriceLevel - _supportPriceLevel) * _cw.P().@base.StopLossPercent;
            return stoplosssize;
        }  // stopLossSize

        /// <summary>
        /// Рассчёт внутренних состояние стратегии
        /// </summary>
        /// <param name="state">Состояние стратегии на рынке</param>
        void calcInternalValues(StrategyMarketState state) {
            /// Значение рассчитыватеся по уровням и конфигурационному параметру
            _stopLossSize = stopLossSize();
            if ((state & StrategyMarketState.Short) != 0) {
                decimal level = getLevelFromStop();
                if (level != 0) {
                    _stopLossLevel = level;
                } else {
                    _stopLossLevel = _supportPriceLevel + _stopLossSize;
                }
            }
            if ((state & StrategyMarketState.Long) != 0) {
                decimal level = getLevelFromStop();
                if (level != 0) {
                    _stopLossLevel = level;
                } else {
                    _stopLossLevel =_resistPriceLevel - _stopLossSize;
                }

            }
            Order stopo = findActiveStop();
            if (stopo == null) {
                LoggingHelper.AddWarningLog(this,
                    "Не найден активный стоп. При развороте позиции не будет выставлен корректный стоп");
            } else {

                // Объём в стопе
                decimal stopVol = stopo.Volume;
                // Объём позиции
                decimal? posVol = positionVolume();
                decimal absPosVol = Math.Abs(posVol.GetValueOrDefault(0));
                // Позиции равны
                if (stopVol == absPosVol) {
                    // скорее всего был разворот и объём, которым следует выходить мы уже знаем
                    base.Volume = absPosVol;
                    if (_cw.P().@internal.Overturn != true) {
                        LoggingHelper.AddWarningLog(this,
                            "Объёмы стопа и текущей позиции совпадают, но нет признака разворота. " +
                            "Необходимо исправить в конфиге Internal");
                    }
                } else {
                    base.Volume = Math.Abs(absPosVol);
                    _overturnVolume = stopVol - base.Volume;
                    if (_cw.P().@internal.Overturn != false) {
                        LoggingHelper.AddWarningLog(this,
                            "Объёмы стопа и текущей позици НЕ совпадают, но стратегия думает что был разворот. " +
                            "Необходимо исправить несоответствие в конфиге Internal");
                    }

                }

                LoggingHelper.AddDebugLog(this,
                    "Восстановленные объёмы(_additionStopVolume({0}), base.Volume({1}))",
                    _overturnVolume, base.Volume);
            }
            LoggingHelper.AddDebugLog(this,
                "Рассчитанные значения: _stopLossSize {0}, _stopLossLevel {1}",
                _stopLossSize, _stopLossLevel);
        }  // calcInternalValues

        decimal getLevelFromStop() {

            Order order = findActiveStop();
            
            if (order != null) {
                RobotLogger.Instance.AddDebugLog(
                    "Восстанавливаем уровень по стопу");

                object val = order.Condition.Parameters["StopPrice"];
                _stopLossLevel = Convert.ToDecimal(val);
                return _stopLossLevel;
            }

            return 0;
        }


    } // class PivotLevelsStrategy
}  // namespace PivotLevelsStrategy
}  // namespace StrategyPlugin