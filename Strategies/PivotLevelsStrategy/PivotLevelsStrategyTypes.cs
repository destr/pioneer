﻿using System;
namespace StrategyPlugin {
namespace PivotLevelsStrategy {
    /// <summary>
    /// Значения H, L, C
    /// </summary>
    public sealed class HLCValues {
        public HLCValues() {
            High = 0;
            Low = Decimal.MaxValue;
            Close = 0;
        }  // Ctor
        /// <summary>
        /// Максимальное значение
        /// </summary>
        public decimal High     {get;set;}
        /// <summary>
        /// Минимальное значение
        /// </summary>
        public decimal Low      {get;set;}
        /// <summary>
        /// Закрытие
        /// </summary>
        public decimal Close    {get;set;}
        /// <summary>
        /// Дата актуализации значений
        /// </summary>
        public DateTime Date    {get;set;}
    };
}  // namespace PivotLevelsStrategy
}  // namespace StrategyPlugin