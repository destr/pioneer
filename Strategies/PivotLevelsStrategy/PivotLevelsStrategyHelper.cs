﻿namespace StrategyPlugin.PivotLevelsStrategy {
    using System;
    using Robot.Common.Exchange.Rts;
    using Robot.Common.Helpers;
    using Robot.Strategy;
    using StockSharp.Algo;
    using StockSharp.Algo.Strategies;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    using StockSharp.Quik;

    public partial class PivotLevelsStrategy {
        /// <summary>
        /// Проверка актуальности уровней предыдущего дня
        /// </summary>
        /// <returns>true если уровни актуальны</returns>
        bool isLevelsActual() {
            if (_l == null) throw new Exception("Уровни null. Strategy PANIC!");

            DateTime prevdate = WorkingCalendar.previousTradingDate().Date;
            DateTime levelsday = _l.Date.Date;
            if (levelsday == prevdate) {
                LoggingHelper.AddDebugLog(this, "Market day ({0}) and levels day ({1}) actual.",
                    prevdate, levelsday);
                return (true);
            }
            LoggingHelper.AddErrorLog(this,
                        "Day ago ({0}) is trading day but levels day ({1}). Levels NOT actual",
                        prevdate, levelsday);
            return (false);
            
        } // isLevelActual
        /// <summary>
        /// Выход с рынка
        /// </summary>
        void exitMarket() {
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                // отменяем все ордера
                CancelActiveOrders();
                OrderDirections direction = ((state & StrategyMarketState.Short) != 0) ?
                    OrderDirections.Buy : OrderDirections.Sell;
                Order order = createLimitOrder(direction);
                order.WhenRegisterFailed()
                    .Do(action_orderRegisterFailed).Once().Apply(this);
                LoggingHelper.AddDebugLog(this, "Ордер закрытия позиций", order);
                RegisterOrder(order);
            }
        }
        /// <summary>
        /// Уровни рассчитаны
        /// </summary>
        /// <returns>true если уровни рассчитаны</returns>
        bool levelsCalculated() {
            return (_supportPriceLevel != -1) && (_resistPriceLevel != -1);
        }  // levelsCanculated
        /// <summary>
        /// Установка времени свечи, по которой производится расчёт уровней
        /// </summary>
        void setClosePriceDate(DateTime dt = new DateTime()) {
            if (dt.Equals(DateTime.MinValue)) {
                close_price_date_ = WorkingCalendar.previousTradingDate();
                close_price_date_ = close_price_date_.Add(
                    StockSharp.BusinessEntities.ExchangeBoard.Forts.WorkingTime.Times[2].Max);
                close_price_date_ -= _timeFrame;
                return;
            }
            // передали дату 
            close_price_date_ = dt;
        }
        /// <summary>
        /// Расчёт уровней
        /// </summary>
        /// <param name="close">Цена закрытия дня</param>
        void calculateLevels(decimal close) {
            decimal pivot = (_l.High + _l.Low + close) / 3;
            LoggingHelper.AddDebugLog(this,
                "Расчёт уроня:  pivot({0}) = (l_.High({1}) + l_.Low({2}) + close) / 3",
                pivot, _l.High, _l.Low, close);
            decimal pivotx2 = pivot*2;

            _resistPriceLevel = pivotx2 - _l.Low;
            _supportPriceLevel = pivotx2 - _l.High;
            LoggingHelper.AddDebugLog(this,
                "Рассчитанные уровни: _resistPriceLevel({0}) = pivotx2({1}) - l_.Low({2});",
                _resistPriceLevel, pivotx2, _l.Low);
            LoggingHelper.AddDebugLog(this,
                "Рассчитанные уровни: _supportPriceLevel({0}) = pivotx2({1}) - l_.High({2});",
                _supportPriceLevel, pivotx2, _l.High);

        }  // calculateLeveles
        /// <summary>
        /// Создать лимитный ордер входа в рынок. По направлению выбирается
        /// максимальная или минимальная цена инструмента.
        /// </summary>
        /// <param name="direction">Направление</param>
        /// <returns>Ордер</returns>
        Order createLimitOrder(OrderDirections direction) {
            decimal price = (direction == OrderDirections.Buy) ? base.Security.MaxPrice :
                base.Security.MinPrice;

            var order = this.CreateOrder(direction, price, base.Volume);
            order.Type = OrderTypes.Limit;
            order.Comment = Name;
            return order;
        }  // createLimitOrder
        /// <summary>
        /// Выставления стоп-ордера
        /// </summary>
        /// <param name="direction">Направление оригинальной сделки</param>
        void registerStop(OrderDirections direction) {
            // Округляем цену, в соответствии с инструментом
            decimal stopprice = stopPrice();
            decimal realvolume = 0;// _overturnVolume;
            if (_cw.P().@internal.Overturn == false) {
                realvolume = base.Volume;
            } else {
                realvolume = _overturnVolume;
            }

            LoggingHelper.AddDebugLog(this,
                "Выставляем стоп-ордер на объём {0} направление защищаемой сделки {1} ",
                realvolume, direction);
            var order = new Order {
                Type = OrderTypes.Conditional,
                Volume = realvolume,
                Price = boundPriceByDirectionForStop(direction, stopprice),
                Security = base.Security,
                Portfolio = base.Portfolio,
                Direction = (direction == OrderDirections.Buy) ?
                               OrderDirections.Sell : OrderDirections.Buy,
                Comment = base.Name,               
                Condition = new QuikOrderCondition
                {
                    Type = QuikOrderConditionTypes.StopLimit,
                    StopPrice = stopprice,
                    ActiveTime = null
                }
            };
            LoggingHelper.AddDebugLog(this,
                "Сформированный стоп-ордер Volume {0}, Price {1}, Direction {2},"
                , order.Volume, order.Price, order.Direction);
            LoggingHelper.AddDebugLog(this, "Регистрация стопа {0}", order);
            var ruleActivated = order.WhenActivated();
            var ruleCancelFailed = order.WhenCancelFailed();
            ruleActivated
                .Do(action_stopOrderActivated).Once()
                .Apply(this).Exclusive(ruleCancelFailed);
            ruleCancelFailed
                .Do(action_cancelFailed)
                .Once()
                .Apply(this).Exclusive(ruleActivated);

            base.RegisterOrder(order);
        } // registerStop

        /// <summary>
        /// Получить граничную цену для стопа в зависимости от направления сделки
        /// </summary>
        /// <param name="direction">Направление сделки которую защищем стопом</param>
        /// <param name="stopprice">Цена стопа</param>
        /// <returns>Цена</returns>
        decimal boundPriceByDirectionForStop(OrderDirections direction, decimal stopprice) {

            decimal delta = (direction == OrderDirections.Buy) ? - _cw.P().@base.StopPriceDelta:
                _cw.P().@base.StopPriceDelta;


            decimal price = stopprice + delta;
            return TradeHelper.CheckAndCorrectRangePrice(base.Security, price);
        }
        decimal stopPrice() {
            return TradeHelper.RoundPrice(base.Security, _stopLossLevel);
        }  // stopPrice
        /// <summary>
        /// Восстановление значения объём после перезапуска
        /// </summary>
        void restoreVolume() {

            // если мы не в позиции, то ничего не делаем
            if ((marketState() & StrategyMarketState.InMarket) == 0) {
                return;
            }
            
            Position pos = Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                LoggingHelper.AddWarningLog(this,
                    "Не могу восстановить объём позиции");
                return;
            }
            base.Volume = Math.Abs(pos.CurrentValue);

            LoggingHelper.AddDebugLog(this, "Восстановлен рабочий объём: {0}", base.Volume);
        }  // restoreVolume

    }  // class PivotLevelsStrategy
}  // namespace StrategPivotLevelsHelper