﻿namespace StrategyPlugin {
namespace PivotLevelsStrategy {
    using System;
    using System.Collections.Generic;
    using Robot.Common.Cli;
    using Robot.Strategy;
    using Robot.Strategy.Cli;
    using StockSharp.Logging;
    /// <summary>
    /// Стратегия. содержащая методы cli
    /// </summary>
    partial class PivotLevelsStrategy : ICliStrategy {
        public bool DoCliCommand(CommandDescription cmd) 
        {
            return Helper.InvokeCommand(this, cmd);
        }


        public void ShowSt(IList<string> parameters) {
            Console.WriteLine("ShowSt");
            foreach (string p in parameters) {
                Console.WriteLine(p);
            }
        }

        public void LongCommand(string [] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в long");
            if (!checkAndCalcLevels()) {
                LoggingHelper.AddErrorLog(this,
                    "Уровни не рассчитаны. Команда входа игнорируется");
                return;
            }
            basePlugin.ManualLongSignal = true;
            tryEnterMarket();
        }  // LongCommand
        public void ShortCommand(string [] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в short");
            if (!checkAndCalcLevels()) {
                LoggingHelper.AddErrorLog(this,
                    "Уровни не рассчитаны. Команда входа игнорируется");
                return;
            }
            basePlugin.ManualShortSignal = true;
            tryEnterMarket();
        }  // ShortCommand

        public void manualWorkDayEnd() {
            StrategyPluginBase.manualWorkDayEnd = true;
        }  // manualWorkDayEnd
        public void invokeStop() {
            //action_stopOrderActivated(new Order());
        }
        public void ClosePositionCommand(string [] args) {
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) == 0) {
                throw new Exception(String.Format("Стратегия {0} не в позиции", base.Name));
            }

            exitMarket();

        }  // ClosePositionCommand
        public void SetBaseCandleTime(string datetime) {
            _resistPriceLevel = -1;
            _supportPriceLevel = -1;
            setClosePriceDate(basePlugin.internalSetBaseCandleTime(datetime));
        }
        public void ReRegisterStop(string [] args) {
            if (args.Length == 0) 
            {
                throw new ArgumentNullException("Не задана стоп цена");
            }
            decimal stopprice = decimal.Parse(args[0]);
            LoggingHelper.AddInfoLog(this,
                "Ручная перерегистрация стопа и стоп-уровня({0}) на значение: {1}",
                _stopLossLevel, stopprice);
            _stopLossLevel = stopprice;
            reRegisterStop();
        }  // ReRegisterStop
    }  // class PivotLevelsStrategy
}  // namespace PivotLevelsStrategy
}  // namespace StrategyPlugin