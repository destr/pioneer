﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="412c25f3-d1e1-49e3-a5bd-1a8da310b85b" namespace="StrategyPlugin.PivotLevelsStrategy" xmlSchemaNamespace="urn:StrategyPlugin.PivotLevelsStrategy" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
    <externalType name="Decimal" namespace="System" />
    <externalType name="VolumeUnit" namespace="Robot.Common.Types" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="Parameters" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="parameters">
      <elementProperties>
        <elementProperty name="base" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="base" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/BaseParameters" />
          </type>
        </elementProperty>
        <elementProperty name="internal" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="internal" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/InternalParameters" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="BaseParameters">
      <attributeProperties>
        <attributeProperty name="StopLossPercent" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopLossPercent" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopLossMul" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopLossMul" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopPriceDelta" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopPriceDelta" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="ExitTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ExitTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="StartEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StartEnterTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndEnterTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndStrategyDay" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndStrategyDay" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeBegin" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeBegin" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeEnd" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeEnd" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="TimeFrame" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="TimeFrame" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="Volume" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Volume" isReadOnly="false" documentation="Объем">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Decimal" />
          </type>
        </attributeProperty>
        <attributeProperty name="VolumeUnit" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="VolumeUnit" isReadOnly="false" documentation="Единицы измерения поля Volume">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/VolumeUnit" />
          </type>
        </attributeProperty>
        <attributeProperty name="Portfolio" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Portfolio" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Security" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Security" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="InternalParameters">
      <attributeProperties>
        <attributeProperty name="Overturn" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="Overturn" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/412c25f3-d1e1-49e3-a5bd-1a8da310b85b/Boolean" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>