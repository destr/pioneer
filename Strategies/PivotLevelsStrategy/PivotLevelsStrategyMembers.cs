﻿namespace StrategyPlugin.PivotLevelsStrategy {
    using System;
    using global::Robot.Common.Equity;
    using global::Robot.Configuration;
    using global::Robot.Storage;
    using global::Robot.Strategy;
    using StockSharp.Algo.Candles;

    public sealed class PivotLevelsStrategyAliveStatus : StrategyAliveStatus {
        /// <summary>
        /// Проверка активности стратегии
        /// </summary>
        /// <returns></returns>
        public override bool IsAlive() {
            DateTime dt = UpdateTime.Add(TimeSpan.FromSeconds(TimeFrame.TotalSeconds * 2));
            if (dt < DateTime.Now) {
                return false;
            }
            return true;
        }
        protected override void DoUpdateByStrategy() {
            // в той стратегии делать ничего не надо
        }
    };

    public partial class PivotLevelsStrategy {
        /// <summary>
        /// Интервал свечей
        /// </summary>
        private TimeSpan _timeFrame;
        /// <summary>
        /// Серия свечей
        /// </summary>
        CandleSeries _candleSeries;
        /// <summary>
        /// Значения H, L предыдущего дня
        /// </summary>
        private HLOfDayValue _l;
        /// <summary>
        /// Дата и время свечки закрытия дня, по которой определяются уровни
        /// </summary>
        private DateTime close_price_date_;
        /// <summary>
        /// Нижний уровень цены
        /// </summary>
        private decimal _supportPriceLevel = -1;
        /// <summary>
        /// Верхний уровень цены
        /// </summary>
        private decimal _resistPriceLevel = -1;
        /// <summary>
        /// Уровень стопа
        /// </summary>
        private decimal _stopLossLevel;
        /// <summary>
        /// Размер цены стопа рассчитанное значение
        /// </summary>
        private decimal _stopLossSize;
        /// <summary>
        /// Базовый функционал плагина
        /// </summary>
        private StrategyPluginBase basePlugin = null;
        /// <summary>
        /// Менеджер расчёта объёма входа
        /// </summary>
        private IVolumeManager volume_manager_ = null;
        /// <summary>
        /// Количество предыдущних свечей
        /// </summary>
        private int number_previous_candles = 5;
        /// <summary>
        /// Время свечи, которую надо игнорировать
        /// </summary>
        private TimeSpan ignoring_candle_time = new TimeSpan(10, 0, 0);
        
        /// <summary>
        /// Дополнительный объём на который надо увеличить размер стопа
        /// </summary>
        private decimal _overturnVolume = 0;

        /// <summary>
        /// Признак того, что стратегия уже переворачивала позиции
        /// </summary>
        private bool _stopOrdersLoaded = false;
        /// <summary>
        /// Признак загрузки ордеров
        /// </summary>
        private bool _ordersLoaded = false;
        /// <summary>
        /// Конфигурация стратегии
        /// </summary>
        private StrategyConfigWrapper<Parameters> _cw;
        /// <summary>
        /// Статус живости стратегии
        /// </summary>
        private PivotLevelsStrategyAliveStatus _aliveStatus { get; set; }
        
    }
}