using StockSharp.Algo.Candles;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;

namespace StrategyPlugin {
using StrategyFactory;

namespace TestingStrategy {
    public class TestingStrategy : Strategy, IStrategyPlugin {
        public TestingStrategy() {
        }
        public void setCandleManager(CandleManager manager) {
        }
        public override void Save(SettingsStorage settings) {
        }
        public override void Load(SettingsStorage settings) {
            
        }
        public void loadInternalSettingsStorage(string filename) {
        }  //
        public void saveInternalSettingsStorage(string filename) {
        } // saveInternalSettingsStorage


    }
}  // namespace TestingStrategy
}  // namespace StrategyPlugin