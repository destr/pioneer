﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="acec75ff-294b-4da1-82b0-2cbe2cfd255b" namespace="StrategyPlugin.ExtremumBreakdownStrategy" xmlSchemaNamespace="urn:StrategyPlugin.ExtremumBreakdownStrategy" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
    <externalType name="VolumeUnit" namespace="Robot.Common.Types" />
  </typeDefinitions>
  <configurationElements>
    <configurationSection name="Parameters" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="parameters">
      <elementProperties>
        <elementProperty name="base" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="base" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/BaseParameters" />
          </type>
        </elementProperty>
        <elementProperty name="internal" isRequired="false" isKey="false" isDefaultCollection="false" xmlName="internal" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/InternalParameters" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
    <configurationElement name="BaseParameters">
      <attributeProperties>
        <attributeProperty name="NumBarsForEnter" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="NumBarsForEnter" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="NumBarsForExit1" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="NumBarsForExit1" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="NumBarsForExit2" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="NumBarsForExit2" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="InverseCorrelation" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="InverseCorrelation" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Boolean" />
          </type>
        </attributeProperty>
        <attributeProperty name="ExitTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="ExitTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="StartEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StartEnterTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndEnterTime" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndEnterTime" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="EndStrategyDay" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="EndStrategyDay" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeBegin" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeBegin" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="SaveCandleTimeRangeEnd" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="SaveCandleTimeRangeEnd" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/TimeSpan" />
          </type>
        </attributeProperty>
        <attributeProperty name="TimeFrame" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="TimeFrame" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="Volume" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Volume" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="VolumeUnit" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="VolumeUnit" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/VolumeUnit" />
          </type>
        </attributeProperty>
        <attributeProperty name="StopPriceDelta" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="StopPriceDelta" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/Int32" />
          </type>
        </attributeProperty>
        <attributeProperty name="MasterSecurity" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="MasterSecurity" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="Portfolio" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="Portfolio" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/String" />
          </type>
        </attributeProperty>
        <attributeProperty name="TradeSecurity" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="TradeSecurity" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/acec75ff-294b-4da1-82b0-2cbe2cfd255b/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationElement name="InternalParameters" />
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>