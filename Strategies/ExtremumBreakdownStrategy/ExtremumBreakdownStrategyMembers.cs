﻿namespace StrategyPlugin.ExtremumBreakdownStrategy {
    using System;
    using System.Collections.Generic;
    using global::Robot.Common.Equity;
    using global::Robot.Configuration;
    using global::Robot.Strategy;
    using StockSharp.Algo;
    using StockSharp.Algo.Candles;
    using StockSharp.BusinessEntities;

    public sealed class ExtremumBreakdownStrategyAliveStatus : StrategyAliveStatus {
        /// <summary>
        /// Время обновления ведущего инструмента
        /// </summary>
        public DateTime MasterUpdateTime { get; set; }
        /// <summary>
        /// Проверка активности стратегии
        /// </summary>
        /// <returns></returns>
        public override bool IsAlive() {
            DateTime dt = UpdateTime.Add(TimeSpan.FromSeconds(TimeFrame.TotalSeconds * 2));
            if (dt < DateTime.Now) {
                return false;
            }
            dt = MasterUpdateTime.Add(TimeSpan.FromSeconds(TimeFrame.TotalSeconds * 2));
            if (dt < DateTime.Now) {
                return false;
            }
            return true;
        }
        protected override void DoUpdateByStrategy() {
            // в той стратегии делать ничего не надо
        }
        protected override void DoUpdateByCliring() {
            MasterUpdateTime = DateTime.Now;
        }
        protected override void DoUpdateByTimeout() {
            MasterUpdateTime = DateTime.Now;
        }
        /// <summary>
        /// Обновление ведущего инструмента
        /// </summary>
        public void UpdateByStrategyMaster() {
            MasterUpdateTime = DateTime.Now;
        }
    };

     public partial class ExtremumBreakdownStrategy {
        /// <summary>
        /// Кол-во свечей, необходимых для расчёта
        /// </summary>
        private int NumBars {
                get {
                    int v = 0;
                    v = Math.Max(_cw.P().@base.NumBarsForEnter, _cw.P().@base.NumBarsForExit1);
                    v = Math.Max(v, _cw.P().@base.NumBarsForExit2);
                    /// magic number
                    return v + 2;
                }
        }
        /// <summary>
        /// Параметры стратегии
        /// </summary>
        StrategyConfigWrapper<Parameters> _cw;
        Security _masterSecurity = null;
        MarketRule<CandleSeries, Candle> _ruleTradeCandleChanged;
        IList<TimeFrameCandle> _tradeHistoryCandles = new List<TimeFrameCandle>();
        IList<TimeFrameCandle> _masterHistoryCandles = new List<TimeFrameCandle>();
        /// <summary>
        /// Сигнал на вход от второго инструмента
        /// </summary>
        private int Security2Signal = 0;
        /// <summary>
        /// Свеча по первому инструменту пришла (0 =нет свечи, >=1 =свеча пришла)
        /// </summary>
        private int Candle1Exist = 0;
        /// <summary>
        /// Свеча по второму инструменту пришла (0 =нет свечи, >=1 =свеча пришла)
        /// </summary>
        private int Candle2Exist = 0;
        /// <summary>
        /// Интервал свечей
        /// </summary>
        private TimeSpan _timeFrame;
        /// <summary>
        /// Менеджер свечей
        /// </summary>
        CandleSeries _tradeCandleSeries;
        CandleSeries _masterCandleSeries;

        /// <summary>
        /// Параметры стратегии
        /// </summary>
        //private Parameters _cw.P().@base;
        //Переменные для расчёта индикаторов входа\выхода
        private decimal High1Security1 = -1,
                        High1Security2 = -1,
                        High2Security1 = -1,
                        High2Security2 = -1,
                        Low1Security1 = 1000000000,
                        Low1Security2 = 1000000000,
                        Low2Security1 = 1000000000,
                        Low2Security2 = 1000000000;
        /// <summary>
        /// Уровень входа в лонг по первому инструменту
        /// </summary>
        private decimal HighEnterSecurity1 = -1;
        /// <summary>
        /// Предыдущий уровень входа в лонг по первому инструменту
        /// </summary>
        private decimal PreviousHighEnterSecurity1 = -1;
        /// <summary>
        /// Уровень входа в лонг по второму инструменту
        /// </summary>
        private decimal HighEnterSecurity2 = -1;
        /// <summary>
        /// Предыдущий уровень входа в лонг по второму инструменту
        /// </summary>
        private decimal PreviousHighEnterSecurity2 = -1;
        /// <summary>
        /// Уровень входа в шорт по первому инструменту
        /// </summary>
        private decimal LowEnterSecurity1 = 1000000000;
        /// <summary>
        /// Предыдущий уровень входа в шорт по первому инструменту
        /// </summary>
        private decimal PreviousLowEnterSecurity1 = 1000000000;
        /// <summary>
        /// Уровень входа в шорт по второму инструменту
        /// </summary>
        private decimal LowEnterSecurity2 = 1000000000;
        /// <summary>
        /// Предыдущий уровень входа в шорт по второму инструменту
        /// </summary>
        private decimal PreviousLowEnterSecurity2 = 1000000000;
        /// <summary>
        /// Уровень стопа для лонга по первому инструменту
        /// </summary>
        private decimal HighStopSecurity1 = -1;
        /// <summary>
        /// Предыдущий уровень стопа для лонга по первому инструменту
        /// </summary>
        private decimal PreviousHighStopSecurity1 = -1;
        /// <summary>
        /// Уровень стопа для лонга по второму инструменту
        /// </summary>
        private decimal HighStopSecurity2 = -1;
        /// <summary>
        /// Предыдущий уровень стопа для лонга по второму инструменту
        /// </summary>
        private decimal PreviousHighStopSecurity2 = -1;
        /// <summary>
        /// Уровень стопа для шорта по первому инструменту
        /// </summary>
        private decimal LowStopSecurity1 = 1000000000;
        /// <summary>
        /// Предыдущий уровень стопа для шорта по первому инструменту
        /// </summary>
        private decimal PreviousLowStopSecurity1 = 1000000000;
        /// <summary>
        /// Уровень стопа для шорта по второму инструменту
        /// </summary>
        private decimal LowStopSecurity2 = 1000000000;
        /// <summary>
        /// Предыдущий уровень стопа для шорта по второму инструменту
        /// </summary>
        private decimal PreviousLowStopSecurity2 = 1000000000;
        /// <summary>
        /// Уровень стопа
        /// </summary>
        private decimal stop_loss_level_ = -1;
        /// <summary>
        /// Размер цены стопа рассчитанное значение
        /// </summary>
        private decimal stop_loss_size_ = -1;
        /// <summary>
        /// Базовый функционал плагина
        /// </summary>
        private StrategyPluginBase basePlugin = null;
        /// <summary>
        /// Менеджер расчёта объёма входа
        /// </summary>
        private IVolumeManager volume_manager_ = null;
        /// <summary>
        /// Время свечи, которую надо игнорировать
        /// </summary>
        private TimeSpan ignoring_candle_time = new TimeSpan(10, 0, 0);
        /// <summary>
        /// Признак загрузки стоп-ордеров
        /// </summary>
        private bool _stopOrdersLoaded = false;
        /// <summary>
        /// Признак загрузки ордеров
        /// </summary>
        private bool _ordersLoaded = false;

        private ExtremumBreakdownStrategyAliveStatus _aliveStatus { get; set; }

    }
}