﻿using System;
namespace StrategyPlugin.ExtremumBreakdownStrategy {
    using Robot.Common.Helpers;
    using Robot.Strategy;
    using StockSharp.Algo;
    using StockSharp.Algo.Strategies;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    using StockSharp.Quik;

    public partial class ExtremumBreakdownStrategy {
        void exitMarket() {
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) != 0) {
                // отменяем все ордера
                CancelActiveOrders();
                OrderDirections direction = ((state & StrategyMarketState.Short) != 0) ?
                    OrderDirections.Buy : OrderDirections.Sell;
                Order order = createLimitOrder(direction);
                
                order.WhenRegisterFailed()
                    .Do(action_orderRegisterFailed).Once().Apply(this);
                RegisterOrder(order);
                LoggingHelper.AddInfoLog(this, "Рабочий день закончен");
            }
        }

        /// <summary>
        /// Создать лимитный ордер входа в рынок. По направлению выбирается
        /// максимальная или минимальная цена инструмента.
        /// </summary>
        /// <param name="direction">Направление</param>
        /// <returns>Ордер</returns>
        Order createLimitOrder(OrderDirections direction) {
            decimal price = (direction == OrderDirections.Buy) ? base.Security.MaxPrice :
                base.Security.MinPrice;

            var order = this.CreateOrder(direction, price, base.Volume);
            order.Type = OrderTypes.Limit;
            order.Comment = Name;
            return order;
        }  // createLimitOrder
        /// <summary>
        /// Выставления стоп-ордера
        /// </summary>
        /// <param name="direction">Направление оригинальной сделки</param>
        void registerStop(OrderDirections direction) {
            // Округляем цену, в соответствии с инструментом
            var stopprice = TradeHelper.RoundPrice(base.Security, stop_loss_level_);

            LoggingHelper.AddInfoLog(this,
                "Выставляем стоп-ордер на объём {0}, по цене {1}, направление защищаемой сделки {2} ",
                base.Volume, stopprice, direction);
            var order = new Order {
                Type = OrderTypes.Conditional,
                Volume = base.Volume,
                Price = boundPriceByDirectionForStop(direction, stopprice),
                Security = base.Security,
                Portfolio = base.Portfolio,
                Direction = (direction == OrderDirections.Buy) ?
                               OrderDirections.Sell : OrderDirections.Buy,
                Comment = base.Name,            
                // ExpiryDate = DateTime.MaxValue,
                Condition = new QuikOrderCondition
                {
                    Type = QuikOrderConditionTypes.StopLimit,
                    StopPrice = stopprice,
                    ActiveTime = null,
                    
                }
            };
            //string str = order.StopCondition.ToString;
            LoggingHelper.AddDebugLog(this,
                "Сформированный стоп-ордер Volume: {0}, Price: {1}, Direction: {2},"
                , order.Volume, order.Price, order.Direction);

            var ruleActivated = order.WhenActivated();
            var ruleCancelFailed = order.WhenCancelFailed();

            ruleActivated
                .Do(action_stopOrderActivated)
                .Once().Apply(this)
                .Exclusive(ruleCancelFailed);
            ruleCancelFailed
                .Do(action_cancelFailed)
                .Once().Apply(this)
                .Exclusive(ruleActivated);

            base.RegisterOrder(order);
        } // registerStop
 
        /// <summary>
        /// Получить граничную цену для стопа в зависимости от направления сделки
        /// </summary>
        /// <param name="direction">Направление сделки которую защищем стопом</param>
        /// <param name="stopprice">Цена стопа</param>
        /// <returns>Цена</returns>
        decimal boundPriceByDirectionForStop(OrderDirections direction, decimal stopprice) {

            decimal delta = (direction == OrderDirections.Buy) ? - _cw.P().@base.StopPriceDelta :
                _cw.P().@base.StopPriceDelta;
            decimal price = stopprice + delta;
            price = TradeHelper.CheckAndCorrectRangePrice(base.Security, price);
            return price;
        }
        /// <summary>
        /// Восстановление значения объём после перезапуска
        /// </summary>
        void restoreVolume() {
            LoggingHelper.AddDebugLog(this, "Восстанавливаю объём...");
            // если мы не в позиции, то ничего не делаем
            if ((marketState() & StrategyMarketState.InMarket) == 0) {
                return;
            }
            
            Position pos = Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                LoggingHelper.AddWarningLog(this,
                    "Не могу восстановить объём позиции");
                return;
            }
            base.Volume = Math.Abs(pos.CurrentValue);

            LoggingHelper.AddInfoLog(this, "Восстановлен рабочий объём: {0}", base.Volume);
        }  // restoreVolume
        
        void subscribeStopOrderRules(Order o) {
            var ruleActivated = o.WhenActivated();
            var ruleCancelFailed = o.WhenCancelFailed();

            ruleActivated
                .Do(action_stopOrderActivated).Once()
                .Apply(this).Exclusive(ruleCancelFailed);
            ruleCancelFailed
                .Do(action_cancelFailed).Once()
                .Apply(this).Exclusive(ruleActivated);
        }

    }  // class ExtremumBreakdownStrategy
}  // namespace ExtremumBreakdownStrategyHelper