﻿namespace StrategyPlugin.ExtremumBreakdownStrategy {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Timers;
    using Ecng.ComponentModel;
    using Robot.Common.Helpers;
    using Robot.Storage;
    using Robot.Strategy;
    using Robot.Strategy.Helper;
    using StockSharp.Algo;
    using StockSharp.Algo.Candles;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;

        public partial class ExtremumBreakdownStrategy {

        Timer timer = new Timer();

        protected override void OnStarted() {
            
            var ruleNewTradeCandle = _tradeCandleSeries.WhenCandlesStarted();
            var ruleNewMasterCandle = _masterCandleSeries.WhenCandlesStarted();
            ruleNewMasterCandle.Do(NewCandle2).Apply(this);
            ruleNewTradeCandle.Do(NewCandle1).Apply(this);

            this.WhenPositionChanged().Do(positionChanged).Apply(this);
            
            //запускаем таймер проверки наступления времени выхода из позиции
            timer.Elapsed += new ElapsedEventHandler(TimeCheck);
            timer.Interval = 1000;
            timer.Start();

            restoreVolume();

            base.OnStarted();
            LoggingHelper.AddInfoLog(this, "Информация о стратегии {0}: ", State());

        } // OnStarted

        protected override void OnStopping() {
            base.OnStopping();
            timer.Stop();
            this.CandleManager.Stop(_tradeCandleSeries);
            this.CandleManager.Stop(_masterCandleSeries);
        }
        
        //каждую минуту по таймеру проверяем текущее время, если настало время выхода - выходим
        private void TimeCheck(object source, ElapsedEventArgs e)  //static?
        {
            if (DateTime.Now.TimeOfDay >= _cw.P().@base.ExitTime)
            {
                LoggingHelper.AddDebugLog(this,
                    "Настало время выхода из позиции ({0}), текущее время ({1})",
                    _cw.P().@base.ExitTime, DateTime.Now.TimeOfDay);
                exitMarket();
                timer.Stop();
            }
        }
   
        private void NewCandle1(Candle candle) {
            _aliveStatus.UpdateByStrategy();
            Candle LastCandle = candle;

            //проверяем время свечи, нужно убедиться что пришла нужная свеча
            DateTime boundmarkettime = _timeFrame.GetCandleBounds(
                Trader.GetMarketTime(ExchangeBoard.Forts.Exchange)).Min;

            if ((LastCandle == null)
                || (LastCandle.OpenTime < boundmarkettime)) {
                LoggingHelper.AddDebugLog(this,
                    "Есть свеча по {0}, но не та ({1}). Ждём нужную свечу...",
                    base.Security, LastCandle.OpenTime);
                return;
            }

            Candle1Exist = Candle1Exist + 1;
            LoggingHelper.AddDebugLog(this, "Новая свеча: {0}, O:{1}, H:{2}, L:{3}, C:{4}",
                LastCandle.OpenTime, LastCandle.OpenPrice, LastCandle.HighPrice, LastCandle.LowPrice, LastCandle.ClosePrice);
            LoggingHelper.AddDebugLog(this,
                "На этот момент Candle1Exist:{0}, Candle2Exist:{1}", Candle1Exist, Candle2Exist);
            NewCandles();
        }

        private void NewCandle2(Candle candle) {
            _aliveStatus.UpdateByStrategyMaster();
            Candle LastCandle = candle;

            //проверяем время свечи, нужно убедиться что пришла нужная свеча
            DateTime boundmarkettime = _timeFrame.GetCandleBounds(
                Trader.GetMarketTime(ExchangeBoard.Forts.Exchange)).Min;

            if ((LastCandle == null)
                || (LastCandle.OpenTime < boundmarkettime)) {
                LoggingHelper.AddDebugLog(this,
                    "Есть свеча по {0}, но не та ({1}). Ждём нужную свечу...",
                    _masterSecurity.Code, LastCandle.OpenTime);
                return;
            }

            Candle2Exist = Candle2Exist + 1;
            LoggingHelper.AddDebugLog(this, "Новая свеча: {0}, O:{1}, H:{2}, L:{3}, C:{4}",
                LastCandle.OpenTime, LastCandle.OpenPrice, LastCandle.HighPrice, 
                LastCandle.LowPrice, LastCandle.ClosePrice);
            LoggingHelper.AddDebugLog(this, "На этот момент Candle1Exist:{0}, Candle2Exist:{1}",
                Candle1Exist, Candle2Exist);
            NewCandles();
        }

        private void NewCandles() {
            //убедимся что обе свечи пришли
            if (Candle1Exist < 1 || Candle2Exist < 1) return;
            
            Candle1Exist = 0;
            Candle2Exist = 0;
            //если уже 11 часов, а свечей по второму инстр. всё нет, то видимо не торговый день, пишем об этом
            //соотв. торговли в такой день не будет
            IEnumerable<TimeFrameCandle> candlelist = candles(_masterCandleSeries);
            TimeFrameCandle LastCandle = candlelist.Last();
            if (_masterSecurity.LastTrade == null && 
                LastCandle.OpenTime.Hour == 23 && 
                DateTime.Now.TimeOfDay.Hours >= 11)
            {
                LoggingHelper.AddDebugLog(this, "Нет свечей по {0}, видимо, не торговый день", _masterSecurity);
                return;
            }

            // останавливаем получение события проверки изменения свечи (это на тот случай,
            // если входа на предыдущей свече не было)
            if (_ruleTradeCandleChanged != null &&_ruleTradeCandleChanged.IsSuspended == false) {
                suspendRuleCandleChanged();
            }
            //если мы не в позиции и при этом есть ордера - значит это не исполненные ордера на вход 
            // на предыдущей свече, удаляем их
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.Long) == 0 &&
                (state & StrategyMarketState.Short) == 0) {
                LoggingHelper.AddInfoLog(this, "Отмена всех заявок и стопов");
                CancelActiveOrders();
            }

            tryEnterMarket();
        }

        private void positionChanged(decimal position) {
        }  // positionChanged

        void insertCandlesInSeries(IList<TimeFrameCandle> candles, CandleSeries series) {
            // Добавляем исторические свечи, если они ещё не добавлены
            if (candles.Count() != 0) {
                StrategyPluginHelper.InsertCandlesInSeries(
                    candles, this.CandleManager, series);

                candles.Clear();
            }
        }
        class TimeFrameCandleComparer : IEqualityComparer<TimeFrameCandle> {
            public bool Equals(TimeFrameCandle lhs, TimeFrameCandle rhs) {
                return lhs.OpenTime == rhs.OpenTime;
            }
            public int GetHashCode(TimeFrameCandle candle) {
                return candle.GetHashCode();
            }
        };
        /// <summary>
        /// Проверка синхронности свечей
        /// </summary>
        bool checkCandlesIsSync(List<TimeFrameCandle> tradeCandles,
            List<TimeFrameCandle> masterCandles) {
            
            DateTime masterBeginDate = masterCandles.First().OpenTime;
            DateTime tradeBeginDate = tradeCandles.First().OpenTime;
            DateTime minBeginDate = new DateTime(Math.Min(masterBeginDate.Ticks, tradeBeginDate.Ticks));
            int mCount = masterCandles.Count;
            int tCount = tradeCandles.Count;
#if true
            LoggingHelper.AddDebugLog(this, "Список свечей по ведущему инструменту перед синхронизацией");
            foreach (TimeFrameCandle c in masterCandles) {
                printCandle(c, "Master before sync:");
            }
            LoggingHelper.AddDebugLog(this, "Список свечей по торгуемому инструменту перед синхронизацией");
            foreach (TimeFrameCandle c in tradeCandles) {
                printCandle(c, "Trade before sync:" );
            }
#endif

            // Очищаем все свечи которых нет в ведущем инструменте
            IEnumerable<TimeFrameCandle> m = masterCandles.AsEnumerable();
            //foreach (TimeFrameCandle c in tradeCandles) {
            //    if (!m.Contains(c, new TimeFrameCandleComparer())) {
            //        printCandle(c, "THIS WILL BE DEL from Trade");
            //    }
            //}
            tradeCandles.RemoveAll(c => !m.Contains(c, new TimeFrameCandleComparer()));
            // Очищаем все свечи, которых нет в торгуемомо инструменте
            IEnumerable<TimeFrameCandle> t = tradeCandles.AsEnumerable();
            //foreach (TimeFrameCandle c in masterCandles) {
            //    if (!t.Contains(c, new TimeFrameCandleComparer())) {
            //        printCandle(c, "THIS WILL BE DEL from MASTER");
            //    }
            //}
            
            masterCandles.RemoveAll(c => !t.Contains(c, new TimeFrameCandleComparer()));
            
            // догружаем свечи
            int deletedTradeCandleCount = tCount - tradeCandles.Count;
            loadAdditionalCandles(minBeginDate, tradeCandles, _tradeCandleSeries, base.Security,
                   deletedTradeCandleCount);

            int deletedMasterCandleCount = mCount - masterCandles.Count;
            loadAdditionalCandles(minBeginDate, masterCandles, _masterCandleSeries, _masterSecurity,
                    deletedMasterCandleCount);

            bool deleted = false;
            if (deletedMasterCandleCount != 0 || deletedTradeCandleCount != 0) {
                LoggingHelper.AddWarningLog(this,
                    "При синхронизации удалено свечей по ведущему инструменту: {0}. По торгуемому {1}",
                    deletedMasterCandleCount, deletedTradeCandleCount);
                deleted = true;
            }

            if (deleted && checkCandlesIsSync(tradeCandles, masterCandles)) {
                return true;
            }
            // Проверка на нехватку свечей осуществляется при входе
            // Поэтому проверяем только соответствие дат
            // по идее теперь размеры дожны совпасть
            int count = Math.Min(tradeCandles.Count, masterCandles.Count);
            for (int i = 0; i < count; ++i) {
                
                TimeFrameCandle tradeCandle = tradeCandles.ElementAt(i);
                TimeFrameCandle masterCandle = masterCandles.ElementAt(i);
                if (tradeCandle.OpenTime != masterCandle.OpenTime) {
                    throw new Exception(String.Format(
                        "Рассинхронизированы свечи ведущий инструмент {0}, торгуемый инструмент {1}",
                        masterCandle.OpenTime, tradeCandle.OpenTime));
                }
            }
            return true;
        }  // checkCandlesIsSync

        void loadAdditionalCandles(DateTime lastCandleDate, List<TimeFrameCandle> candles, CandleSeries series, Security security,
            int count) {
            Range<TimeSpan> candleTimeRange = new Range<TimeSpan>(
                ExchangeBoard.Forts.WorkingTime.Times[0].Min,
                ExchangeBoard.Forts.WorkingTime.Times[2].Max);

            // дата последней загруженной свечи
            //DateTime lastCandleDate = candles.First().OpenTime;
            IEnumerable<TimeFrameCandle> addCandles = Storage.Instance
                .LoadTimeFrameCandles(security, _timeFrame, lastCandleDate, count, candleTimeRange);
            // вставляем свечи в список
            foreach (TimeFrameCandle c in addCandles) {
                c.Series = _masterCandleSeries;
                candles.Add(c);
            }
            candles.Sort((a, b) => a.OpenTime.CompareTo(b.OpenTime));

        }  // loadAdditionalCandles

        void tryEnterMarket() {
            insertCandlesInSeries(_tradeHistoryCandles, _tradeCandleSeries);
            insertCandlesInSeries(_masterHistoryCandles, _masterCandleSeries);

            List<TimeFrameCandle> tradeCandles;
            List<TimeFrameCandle> masterCandles;

            // Первоначальная загрузка свечей
            tradeCandles = candles(_tradeCandleSeries).ToList();
            masterCandles = candles(_masterCandleSeries).ToList();
            checkCandlesIsSync(tradeCandles, masterCandles);

            // получаем последние свечи по первому инструменту
            IEnumerable<TimeFrameCandle> candlelist = tradeCandles.AsEnumerable();
            // проверяем, что самая свежая свеча имеет нужное время
            // т.е. она не старая
            TimeFrameCandle activecandle = candlelist.Last();
            
            DateTime boundmarkettime = _timeFrame.GetCandleBounds(Trader.GetMarketTime(
                ExchangeBoard.Forts.Exchange)).Min;

            if ((activecandle == null)
                || (activecandle.OpenTime < boundmarkettime)) {
                    LoggingHelper.AddDebugLog(this, "Пока нет нужных свечей. Ждём...");
                return;
            }
            // получаем последние свечи по второму инструменту
            IEnumerable<TimeFrameCandle> candlelist2 = masterCandles.AsEnumerable();
            // проверяем, что самая свежая свеча имеет нужное время
            // т.е. она не старая
            TimeFrameCandle activecandle2 = candlelist2.Last();

            if ((activecandle2 == null) 
                || (activecandle2.OpenTime < boundmarkettime)) {
                    LoggingHelper.AddDebugLog(this, "Пока нет нужных свечей. Ждём...");
                return;
            }
            // игнорируем первую свечу
            if (activecandle.OpenTime.TimeOfDay == ignoring_candle_time) {
                return;
            }

            LoggingHelper.AddDebugLog(this, "Свечи по {0}:", base.Security);
            foreach (Candle c in candlelist) {  
                LoggingHelper.AddDebugLog(this, "{0}, O:{1}, H:{2}, L:{3}, C:{4}",
                    c.OpenTime, c.OpenPrice, c.HighPrice, c.LowPrice, c.ClosePrice);
            }
            LoggingHelper.AddDebugLog(this, "Свечи по {0}:", _masterSecurity);
            foreach (Candle c in candlelist2) {
                LoggingHelper.AddDebugLog(this, "{0}, O:{1}, H:{2}, L:{3}, C:{4}",
                    c.OpenTime, c.OpenPrice, c.HighPrice, c.LowPrice, c.ClosePrice);
            }
            

            if (candlelist.Count() < NumBars) {
                LoggingHelper.AddErrorLog(this,
                    "Не удалось получить {0} исторических свечей по {1}",
                    NumBars, _tradeCandleSeries.Security.Code);
                return;
            }
            if (candlelist2.Count() < NumBars)
            {
                LoggingHelper.AddErrorLog(this,
                    "Не удалось получить {0} исторических свечей по {1}", 
                    NumBars, _masterCandleSeries.Security.Code);
                return;
            }

            //расчитываем индикаторы входа\выхода
            CalculateIndicators(candlelist, candlelist2);
            StrategyMarketState state = marketState();
            /// если мы в позиции, то передвигаем стопы
            if ((state & StrategyMarketState.InMarket) != 0) {
                recalcStopLevels(candlelist);
                return;
            }                                          
            // проверяем можем ли мы входить
            if (!canEnter()) {
                return;
            }

            //проверяем есть ли сигнал от второго инструмента
            CalculateSecurity2Signal(candlelist2);
            if (haveLongSignal(candlelist)) {
                //сразу считаем объём на вход, при условии входа по цене HighEnterSecurity1
                LoggingHelper.AddDebugLog(this, 
                    "Перед расчётом объёма на вход, убедимся что у нас есть данные."
                    + " HighEnterSecurity1: {0}, stop_loss_level_: {1} ",
                    HighEnterSecurity1, stop_loss_level_);

                base.Volume = volume_manager_.maxVolume(HighEnterSecurity1, stop_loss_level_);
                LoggingHelper.AddInfoLog(this, "Посчитанный объём на вход: {0}", base.Volume);

                LoggingHelper.AddDebugLog(this,
                    "Подписываемся на событие изменения текущей свечи, ждём пробоя вверх уровня {0}",
                    HighEnterSecurity1);   

                if (_ruleTradeCandleChanged != null) {
                    Rules.Remove(_ruleTradeCandleChanged);
                }
                _ruleTradeCandleChanged = _tradeCandleSeries.WhenCandlesChanged();
                _ruleTradeCandleChanged.Do(CheckCandleToLong).Apply(this);

            } else if (haveShortSignal(candlelist)) {
                //сразу считаем объём на вход, при условии входа по цене LowEnterSecurity1
                LoggingHelper.AddDebugLog(this, 
                    "Перед расчётом объёма на вход, убедимся что у нас есть данные."
                    + " LowEnterSecurity1: {0}, stop_loss_level_: {1} ",
                    LowEnterSecurity1, stop_loss_level_);
                
                base.Volume = volume_manager_.maxVolume(LowEnterSecurity1, stop_loss_level_);
                LoggingHelper.AddInfoLog(this, "Посчитанный объём на вход: {0}", base.Volume);

                // если сигнал на вход есть, то подписываемся на проверку свечи и ждём когда 
                // цена пробьёт нужный уровень
                LoggingHelper.AddDebugLog(this,
                    "Подписываемся на событие изменения текущей свечи, ждём пробоя вниз уровня {0}", 
                    LowEnterSecurity1);
                if (_ruleTradeCandleChanged != null) {
                    Rules.Remove(_ruleTradeCandleChanged);
                }
                _ruleTradeCandleChanged = _tradeCandleSeries.WhenCandlesChanged();
                _ruleTradeCandleChanged.Do(CheckCandleToShort).Apply(this);
                
            }

        }  // tryEnterMarket

        private void CheckCandleToLong(Candle candle)
        {
            Candle LastCandle = candle;

            printCandle(LastCandle);
            LoggingHelper.AddDebugLog(this,
                "Проверяем истинность условия для входа LastCandle.HighPrice={0} >= HighEnterSecurity1={1}",
                LastCandle.HighPrice, HighEnterSecurity1);

            if (LastCandle.HighPrice >= HighEnterSecurity1 || this.ManualLongSignal)
            {
                this.ManualLongSignal = false;
                LoggingHelper.AddInfoLog(this, ">>>>> Уровень {0} пробит, входим в LONG <<<<<", HighEnterSecurity1);
                registerEnterOrders(OrderDirections.Buy);  //поменять цены, чтобы иметь гарантированный вход
                //отписываемся от события проверки текущей свечи
                suspendRuleCandleChanged();
            }
        }

        private void CheckCandleToShort(Candle candle)
        {
            Candle LastCandle = candle;
            printCandle(LastCandle);
            
            LoggingHelper.AddDebugLog(this,
                "Проверяем истинность условия для входа LastCandle.LowPrice={0} <= LowEnterSecurity1={1}",
                LastCandle.LowPrice, LowEnterSecurity1);

            if (LastCandle.LowPrice <= LowEnterSecurity1 || this.ManualShortSignal)
            {
                this.ManualShortSignal = false;
                LoggingHelper.AddInfoLog(this, ">>>>> Уровень {0} пробит, входим в SHORT <<<<<", LowEnterSecurity1);
                registerEnterOrders(OrderDirections.Sell);  //поменять цены, чтобы иметь гарантированный вход
                //отписываемся от события проверки текущей свечи
                suspendRuleCandleChanged();
            }
        }
        void suspendRuleCandleChanged() {
            //Rules.RemoveRulesByToken(_ruleTradeCandleChanged.Token, _ruleTradeCandleChanged);
            //Rules.Remove(_ruleTradeCandleChanged);
            _ruleTradeCandleChanged.Suspend(true);
            //_ruleTradeCandleChanged.Dispose();
           // _ruleTradeCandleChanged = null;
        }

        void printCandle(Candle candle, string suffix = null) {
            if (String.IsNullOrEmpty(suffix)) {
                suffix = "Изменилась свеча:";
            }
            LoggingHelper.AddDebugLog(this, 
                "{0} {1} O:{2} H:{3} L:{4} C:{5} V:{6}",
                suffix,
                candle.OpenTime.ToString(),
                candle.OpenPrice,
                candle.HighPrice,
                candle.LowPrice,
                candle.ClosePrice,
                candle.TotalVolume);
        }

        void registerEnterOrders(OrderDirections direction) {
            Order order = createLimitOrder(direction);
            var ruleRegistered = order.WhenRegistered();
            var ruleRegisterFailed = order.WhenRegisterFailed();

            ruleRegistered.Do(action_orderRegistered)
                .Once()
                .Apply(this).Exclusive(ruleRegisterFailed);
            
            ruleRegisterFailed.Do(action_orderRegisterFailed)
                .Once()
                .Apply(this).Exclusive(ruleRegistered);
            
            this.RegisterOrder(order);
        } // registerEnterOrders
        
        /// <summary>
        /// Расчёт индикаторов
        /// </summary>
        void CalculateIndicators(IEnumerable<TimeFrameCandle> candles, IEnumerable<TimeFrameCandle> candles2)
        {
            if (HighStopSecurity1 == -1 ||
                HighStopSecurity2 == -1 ||
                LowStopSecurity1 == 1000000000 ||
                LowStopSecurity2 == 1000000000 ||
                HighEnterSecurity1 == -1 ||
                HighEnterSecurity2 == -1 ||
                LowEnterSecurity1 == 1000000000 ||
                LowEnterSecurity2 == 1000000000
                )
            {
                //если по каким-то причинам нет предыдущих значений (например первый запуск или запуск после сбоя), то мы их рассчитываем заново (соответственно дополнительно смещаемся назад на 1 свечу)

                //обнуляем на всякий случай
                HighStopSecurity1 = -1;
                HighStopSecurity2 = -1;
                LowStopSecurity1 = 1000000000;
                LowStopSecurity2 = 1000000000;
                HighEnterSecurity1 = -1;
                HighEnterSecurity2 = -1;
                LowEnterSecurity1 = 1000000000;
                LowEnterSecurity2 = 1000000000;
                High1Security1 = -1;
                High1Security2 = -1;
                High2Security1 = -1;
                High2Security2 = -1;
                Low1Security1 = 1000000000;
                Low1Security2 = 1000000000;
                Low2Security1 = 1000000000;
                Low2Security2 = 1000000000;

                //далее просто копия рассчёта расположенного ниже, но смещённая дополнительно назад на 1 свечу
                //рассчитываем High1 = Highest.Series(High, NumBarsForExit1)>>1; //наибольший хай из NumBarsForExit1 баров, начиная с бара -1
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 2); i--)
                {
                    if (candles.ElementAt(i).HighPrice > High1Security1) High1Security1 = candles.ElementAt(i).HighPrice;
                }

                //рассчитываем Low1 = Lowest.Series(Low,   NumBarsForExit1)>>1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 2); i--)
                {
                    if (candles.ElementAt(i).LowPrice < Low1Security1) Low1Security1 = candles.ElementAt(i).LowPrice;
                }

                //рассчитываем High1Micex = Highest.Series(Micex.High, NumBarsForExit1)>>1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 2); i--)
                {
                    if (candles2.ElementAt(i).HighPrice > High1Security2) High1Security2 = candles2.ElementAt(i).HighPrice;
                }

                //рассчитываем Low1Micex = Lowest.Series(Micex.Low,   NumBarsForExit1)>>1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 2); i--)
                {
                    if (candles2.ElementAt(i).LowPrice < Low1Security2) Low1Security2 = candles2.ElementAt(i).LowPrice;
                }

                //рассчитываем High2 = (High >> NumBarsForExit2); //хай бара -NumBarsForExit2
                High2Security1 = candles.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 2).HighPrice;

                //рассчитываем Low2 = (Low >> NumBarsForExit2);
                Low2Security1 = candles.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 2).LowPrice;

                //рассчитываем High2Micex = (Micex.High >> NumBarsForExit2);
                High2Security2 = candles2.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 2).HighPrice;

                //рассчитываем Low2Micex = (Micex.Low >> NumBarsForExit2);
                Low2Security2 = candles2.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 2).LowPrice;

                //HighStop = (High2+High1)/2;
                HighStopSecurity1 = (High1Security1 + High2Security1) / 2;
                //LowStop = (Low2 + Low1) / 2;
                LowStopSecurity1 = (Low1Security1 + Low2Security1) / 2;

                //HighStopMicex = (High2Micex + High1Micex) / 2;
                HighStopSecurity2 = (High1Security2 + High2Security2) / 2;
                //LowStopMicex = (Low2Micex + Low1Micex) / 2;
                LowStopSecurity2 = (Low1Security2 + Low2Security2) / 2;

                // Индикаторы для входа на РТС
                //HighEnter = Highest.Series(High, NumBarsForEnter) >> 1; //наибольший хай из NumBarsForEnter баров, начиная с бара -1
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 2); i--)
                {
                    if (candles.ElementAt(i).HighPrice > HighEnterSecurity1) HighEnterSecurity1 = candles.ElementAt(i).HighPrice;
                }
                //LowEnter = Lowest.Series(Low, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 2); i--)
                {
                    if (candles.ElementAt(i).LowPrice < LowEnterSecurity1) LowEnterSecurity1 = candles.ElementAt(i).LowPrice;
                }

                // Индикаторы для входа на ММВБ
                //HighEnterMicex = Highest.Series(Micex.High, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 2); i--)
                {
                    if (candles2.ElementAt(i).HighPrice > HighEnterSecurity2) HighEnterSecurity2 = candles2.ElementAt(i).HighPrice;
                }
                //LowEnterMicex = Lowest.Series(Micex.Low, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 3); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 2); i--)
                {
                    if (candles2.ElementAt(i).LowPrice < LowEnterSecurity2) LowEnterSecurity2 = candles2.ElementAt(i).LowPrice;
                }

                PreviousHighStopSecurity1 = HighStopSecurity1;
                PreviousHighStopSecurity2 = HighStopSecurity2;
                PreviousLowStopSecurity1 = LowStopSecurity1;
                PreviousLowStopSecurity2 = LowStopSecurity2;
                PreviousHighEnterSecurity1 = HighEnterSecurity1;
                PreviousHighEnterSecurity2 = HighEnterSecurity2;
                PreviousLowEnterSecurity1 = LowEnterSecurity1;
                PreviousLowEnterSecurity2 = LowEnterSecurity2;

                LoggingHelper.AddDebugLog(this, "PreviousHighStopSecurity1: {0} (предыдущий стоп для лонга по инструменту 1)", PreviousHighStopSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousHighStopSecurity2: {0} (предыдущий стоп для лонга по инструменту 2)", PreviousHighStopSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousLowStopSecurity1: {0} (предыдущий стоп для шорта по инструменту 1)", PreviousLowStopSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousLowStopSecurity2: {0} (предыдущий стоп для шорта по инструменту 2)", PreviousLowStopSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousHighEnterSecurity1: {0} (предыдущий уровень входа в лонг по инструменту 1)", PreviousHighEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousHighEnterSecurity2: {0} (предыдущий уровень входа в лонг по инструменту 2)", PreviousHighEnterSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousLowEnterSecurity1: {0} (предыдущий уровень входа в шорт по инструменту 1)", PreviousLowEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousLowEnterSecurity2: {0} (предыдущий уровень входа в шорт по инструменту 2)", PreviousLowEnterSecurity2);
            }
            else  //если предыдущие значения есть, то просто записываем их
            {
                PreviousHighStopSecurity1 = HighStopSecurity1;
                PreviousHighStopSecurity2 = HighStopSecurity2;
                PreviousLowStopSecurity1 = LowStopSecurity1;
                PreviousLowStopSecurity2 = LowStopSecurity2;
                PreviousHighEnterSecurity1 = HighEnterSecurity1;
                PreviousHighEnterSecurity2 = HighEnterSecurity2;
                PreviousLowEnterSecurity1 = LowEnterSecurity1;
                PreviousLowEnterSecurity2 = LowEnterSecurity2;

                LoggingHelper.AddDebugLog(this, "PreviousHighStopSecurity1: {0} (предыдущий стоп для лонга по инструменту 1)", PreviousHighStopSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousHighStopSecurity2: {0} (предыдущий стоп для лонга по инструменту 2)", PreviousHighStopSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousLowStopSecurity1: {0} (предыдущий стоп для шорта по инструменту 1)", PreviousLowStopSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousLowStopSecurity2: {0} (предыдущий стоп для шорта по инструменту 2)", PreviousLowStopSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousHighEnterSecurity1: {0} (предыдущий уровень входа в лонг по инструменту 1)", PreviousHighEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousHighEnterSecurity2: {0} (предыдущий уровень входа в лонг по инструменту 2)", PreviousHighEnterSecurity2);
                LoggingHelper.AddDebugLog(this, "PreviousLowEnterSecurity1: {0} (предыдущий уровень входа в шорт по инструменту 1)", PreviousLowEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "PreviousLowEnterSecurity2: {0} (предыдущий уровень входа в шорт по инструменту 2)", PreviousLowEnterSecurity2);
            }
            
            //обнуляем текущие значения
            HighStopSecurity1 = -1;
            HighStopSecurity2 = -1;
            LowStopSecurity1 = 1000000000;
            LowStopSecurity2 = 1000000000;
            HighEnterSecurity1 = -1;
            HighEnterSecurity2 = -1;
            LowEnterSecurity1 = 1000000000;
            LowEnterSecurity2 = 1000000000;
            High1Security1 = -1;
            High1Security2 = -1;
            High2Security1 = -1;
            High2Security2 = -1;
            Low1Security1 = 1000000000;
            Low1Security2 = 1000000000;
            Low2Security1 = 1000000000;
            Low2Security2 = 1000000000;

                //рассчитываем High1 = Highest.Series(High, NumBarsForExit1)>>1; //наибольший хай из NumBarsForExit1 баров, начиная с бара -1
            for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 1); i--)
                {
                    if (candles.ElementAt(i).HighPrice > High1Security1) High1Security1 = candles.ElementAt(i).HighPrice;
                }

                //рассчитываем Low1 = Lowest.Series(Low,   NumBarsForExit1)>>1;
            for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 1); i--)
                {
                    if (candles.ElementAt(i).LowPrice < Low1Security1) Low1Security1 = candles.ElementAt(i).LowPrice;
                }

                //рассчитываем High1Micex = Highest.Series(Micex.High, NumBarsForExit1)>>1;
            for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 1); i--)
                {
                    if (candles2.ElementAt(i).HighPrice > High1Security2) High1Security2 = candles2.ElementAt(i).HighPrice;
                }

                //рассчитываем Low1Micex = Lowest.Series(Micex.Low,   NumBarsForExit1)>>1;
            for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForExit1 - 1); i--)
                {
                    if (candles2.ElementAt(i).LowPrice < Low1Security2) Low1Security2 = candles2.ElementAt(i).LowPrice;
                }

                //рассчитываем High2 = (High >> NumBarsForExit2); //хай бара -NumBarsForExit2
            High2Security1 = candles.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 1).HighPrice;

                //рассчитываем Low2 = (Low >> NumBarsForExit2);
            Low2Security1 = candles.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 1).LowPrice;

                //рассчитываем High2Micex = (Micex.High >> NumBarsForExit2);
            High2Security2 = candles2.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 1).HighPrice;

                //рассчитываем Low2Micex = (Micex.Low >> NumBarsForExit2);
            Low2Security2 = candles2.ElementAt(NumBars - _cw.P().@base.NumBarsForExit2 - 1).LowPrice;

            LoggingHelper.AddDebugLog(this, "High1Security1: {0}", High1Security1);
            LoggingHelper.AddDebugLog(this, "High1Security2: {0}", High1Security2);
            LoggingHelper.AddDebugLog(this, "Low1Security1: {0}", Low1Security1);
            LoggingHelper.AddDebugLog(this, "Low1Security2: {0}", Low1Security2);
            LoggingHelper.AddDebugLog(this, "High2Security1: {0}", High2Security1);
            LoggingHelper.AddDebugLog(this, "High2Security2: {0}", High2Security2);
            LoggingHelper.AddDebugLog(this, "Low2Security1: {0}", Low2Security1);
            LoggingHelper.AddDebugLog(this, "Low2Security2: {0}", Low2Security2);


                //HighStop = (High2+High1)/2;
                HighStopSecurity1 = (High1Security1 + High2Security1) / 2;
                //LowStop = (Low2 + Low1) / 2;
                LowStopSecurity1 = (Low1Security1 + Low2Security1) / 2;

                //HighStopMicex = (High2Micex + High1Micex) / 2;
                HighStopSecurity2 = (High1Security2 + High2Security2) / 2;
                //LowStopMicex = (Low2Micex + Low1Micex) / 2;
                LowStopSecurity2 = (Low1Security2 + Low2Security2) / 2;

                // Индикаторы для входа на РТС
                //HighEnter = Highest.Series(High, NumBarsForEnter) >> 1; //наибольший хай из NumBarsForEnter баров, начиная с бара -1
                for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 1); i--)
                {
                    if (candles.ElementAt(i).HighPrice > HighEnterSecurity1) HighEnterSecurity1 = candles.ElementAt(i).HighPrice;
                }
                //LowEnter = Lowest.Series(Low, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 1); i--)
                {
                    if (candles.ElementAt(i).LowPrice < LowEnterSecurity1) LowEnterSecurity1 = candles.ElementAt(i).LowPrice;
                }

                // Индикаторы для входа на ММВБ
                //HighEnterMicex = Highest.Series(Micex.High, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 1); i--)
                {
                    if (candles2.ElementAt(i).HighPrice > HighEnterSecurity2) HighEnterSecurity2 = candles2.ElementAt(i).HighPrice;
                }
                //LowEnterMicex = Lowest.Series(Micex.Low, NumBarsForEnter) >> 1;
                for (int i = (NumBars - 2); i >= (NumBars - _cw.P().@base.NumBarsForEnter - 1); i--)
                {
                    if (candles2.ElementAt(i).LowPrice < LowEnterSecurity2) LowEnterSecurity2 = candles2.ElementAt(i).LowPrice;
                }

                LoggingHelper.AddDebugLog(this, "HighStopSecurity1: {0} (текущий стоп для лонга по инструменту 1)", HighStopSecurity1);
                LoggingHelper.AddDebugLog(this, "HighStopSecurity2: {0} (текущий стоп для лонга по инструменту 2)", HighStopSecurity2);
                LoggingHelper.AddDebugLog(this, "LowStopSecurity1: {0} (текущий стоп для шорта по инструменту 1)", LowStopSecurity1);
                LoggingHelper.AddDebugLog(this, "LowStopSecurity2: {0} (текущий стоп для шорта по инструменту 2)", LowStopSecurity2);
                LoggingHelper.AddDebugLog(this, "HighEnterSecurity1: {0} (текущий уровень входа в лонг по инструменту 1)", HighEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "HighEnterSecurity2: {0} (текущий уровень входа в лонг по инструменту 2)", HighEnterSecurity2);
                LoggingHelper.AddDebugLog(this, "LowEnterSecurity1: {0} (текущий уровень входа в шорт по инструменту 1)", LowEnterSecurity1);
                LoggingHelper.AddDebugLog(this, "LowEnterSecurity2: {0} (текущий уровень входа в шорт по инструменту 2)", LowEnterSecurity2);

        } //CalculateIndicators

        void CalculateSecurity2Signal(IEnumerable<TimeFrameCandle> candles2)
        {
            Security2Signal = 0;

            if (candles2.ElementAt(NumBars - 2).HighPrice > PreviousHighEnterSecurity2 &&
                PreviousHighStopSecurity2 < candles2.ElementAt(NumBars - 2).LowPrice)
            {
                Security2Signal = 1;
                if (_cw.P().@base.InverseCorrelation) Security2Signal = -1;
            }
            if (candles2.ElementAt(NumBars - 2).LowPrice < PreviousLowEnterSecurity2 &&
                PreviousLowStopSecurity2 > candles2.ElementAt(NumBars - 2).HighPrice)
            {
                Security2Signal = -1;
                if (_cw.P().@base.InverseCorrelation) Security2Signal = 1;
            }
            if (Security2Signal == 1) LoggingHelper.AddDebugLog(this, "Разрешение на long от {0}", _masterSecurity);
            if (Security2Signal == -1) LoggingHelper.AddDebugLog(this, "Разрешение на short от {0}", _masterSecurity);
        }

        /// <summary>
        /// Проверка наличия сигнала в лонг
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveLongSignal(IEnumerable<TimeFrameCandle> candles) {
            if (PreviousHighStopSecurity1 < candles.ElementAt(NumBars - 2).LowPrice &&
                Security2Signal == 1
                || this.ManualLongSignal)
            {
                LoggingHelper.AddDebugLog(this, "На текущей свече по {0} возможен вход в Long при пробитии уровня {1} вверх", base.Security, HighEnterSecurity1);
                //basePlugin.LongCommand = false;
                stop_loss_size_ = HighEnterSecurity1 - PreviousHighStopSecurity1;
                stop_loss_level_ = PreviousHighStopSecurity1;
                LoggingHelper.AddDebugLog(this, "Уровень стопа: {0}, размер стопа в пунктах: {1}", stop_loss_level_, stop_loss_size_);
                return(true);
            }

            return (false);
        }  // haveLongSignal
        /// <summary>
        /// Проверка наличия сигнала в шорт
        /// </summary>
        /// <returns>true если есть сигнал для входа</returns>
        bool haveShortSignal(IEnumerable<TimeFrameCandle> candles) {


            if (PreviousLowStopSecurity1 > candles.ElementAt(NumBars - 2).HighPrice &&
                Security2Signal == -1
                || this.ManualShortSignal)
            {
                LoggingHelper.AddDebugLog(this, "На текущей свече по {0} возможен вход в Short при пробитии уровня {1} вниз", base.Security, LowEnterSecurity1);
                //basePlugin.ShortCommand = false;
                stop_loss_size_ = PreviousLowStopSecurity1 - LowEnterSecurity1;    //здесь ранее был HighEnterSecurity1
                stop_loss_level_ = PreviousLowStopSecurity1;
                LoggingHelper.AddDebugLog(this, "Уровень стопа: {0}, размер стопа в пунктах: {1}", stop_loss_level_, stop_loss_size_);
                return (true);
            }

            return (false);
        }  // haveShortSignal
        /// <summary>
        /// Пересчёт стопов и их перерегистрация в случае изменения уровня
        /// </summary>
        /// <param name="candles">Свечи</param>
        void recalcStopLevels(IEnumerable<TimeFrameCandle> candles) {
            // StopForLong = Math.Max(StopForLong, HighStop[bar]); // Передвигаем стоп

            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.Long) != 0)
            {
                if (HighStopSecurity1 > stop_loss_level_)
                {
                    stop_loss_level_ = HighStopSecurity1;
                    reRegisterStop();
                }
            }
            else if ((state & StrategyMarketState.Short) != 0)
            {
                if (LowStopSecurity1 < stop_loss_level_)
                {
                    stop_loss_level_ = LowStopSecurity1;
                    reRegisterStop();
                }
            }


        }  // recalStopLevels

        void reRegisterStop() {
            decimal stopprice = TradeHelper.RoundPrice(base.Security, stop_loss_level_);
            LoggingHelper.AddInfoLog(this,
                "Попытка перерегистрации стопа с новой ценой: {0}", stopprice); 
            Order stop_order = findActiveStop();
            if (stop_order != null) {
                var order = stop_order.Clone();
                
                order.Condition.Parameters["StopPrice"] = stopprice;
                OrderDirections boundDirection = order.Direction == OrderDirections.Sell ?
                    OrderDirections.Buy : OrderDirections.Sell;
                order.Price = boundPriceByDirectionForStop(boundDirection, stopprice);
                LoggingHelper.AddDebugLog(this, "reRegisterStop::new order: {0}", order);
                LoggingHelper.AddDebugLog(this, "reRegisterStop::old order: {0}", stop_order);


                subscribeStopOrderRules(order);
                base.ReRegisterOrder(stop_order, order);
                order.WhenRegistered()
                    .Do(action_StopOrderOk)
                    .Once().Apply(this);
            } else {
                LoggingHelper.AddWarningLog(this,
                    "reRegisterStop: Нет активного стоп ордера!");
            }
        }  // reRegisterStop
        Order findActiveStop() {
            return this.StopOrders.FirstOrDefault<Order>(o => o.State == OrderStates.Active);
        }
        /// <summary>
        /// Проверка возможности входа
        /// </summary>
        /// <returns>true если можно входить</returns>
        bool canEnter() {

            TimeSpan mtime = Trader.GetMarketTime(
                ExchangeBoard.Forts.Exchange).TimeOfDay;
            if (mtime < _cw.P().@base.StartEnterTime || mtime > _cw.P().@base.EndEnterTime) {
                LoggingHelper.AddDebugLog(this,
                    "Текущее время рынка ({0}) не входит в интервал работы стратегии ({1}, {2})",
                    mtime, _cw.P().@base.StartEnterTime, _cw.P().@base.EndEnterTime);
                return (false);
            }                  
            return (true);
        }  // canEnter

        /// <summary>
        /// Получение текущего состояния стратегии на рынке
        /// </summary>
        /// <returns></returns>
        StrategyMarketState marketState() {
            Position pos = base.Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                return StrategyMarketState.Unknown;
            }
            LoggingHelper.AddDebugLog(this,
                "Текущее значение позиции: {0}", pos.CurrentValue);
            if (pos.CurrentValue > 0) {
                return StrategyMarketState.Long;
            } else if (pos.CurrentValue < 0) {
                return StrategyMarketState.Short;
            }

            return StrategyMarketState.Free;
        }  // marketState
        /// <summary>
        /// Получение свечей необходмых для анализа сигналов
        /// </summary>
        /// <returns>Список свечей</returns>
        IEnumerable<TimeFrameCandle> candles(CandleSeries series) {
            LoggingHelper.AddDebugLog(this,
                "Загрузка предыдущих свечей в количестве {0}", NumBars);
            return series.GetCandles<TimeFrameCandle>(NumBars);
        }

        void changeSecurityExchange() {
            if (_masterSecurity == null) return;
            LoggingHelper.AddDebugLog(this, "Security.Id= {0}", Security.Id);
            LoggingHelper.AddDebugLog(this, "Security.Name= {0}", Security.Name);
            LoggingHelper.AddDebugLog(this, "Security.Type= {0}", Security.Type);
            LoggingHelper.AddDebugLog(this, "Security.Class= {0}", Security.Class);
            LoggingHelper.AddDebugLog(this, "Security.Exchange= {0}", Security.ExchangeBoard.Exchange);
            _masterSecurity.ExchangeBoard = new Robot.Common.Exchange.ExchangeNyse();
            
            LoggingHelper.AddDebugLog(this, "_masterSecurity.Id= {0}", _masterSecurity.Id);
            LoggingHelper.AddDebugLog(this, "_masterSecurity.Name= {0}", _masterSecurity.Name);
            LoggingHelper.AddDebugLog(this, "_masterSecurity.Type= {0}", _masterSecurity.Type);
            LoggingHelper.AddDebugLog(this, "_masterSecurity.Class= {0}", _masterSecurity.Class);
            LoggingHelper.AddDebugLog(this, "_masterSecurity.Exchange= {0}", _masterSecurity.ExchangeBoard.Exchange);

            LoggingHelper.AddDebugLog(this,
                "Меняем биржу инструмента _masterSecurity.Exchange = {0}",
                _masterSecurity.ExchangeBoard.Exchange);
        }  // changeSecurityExchange

    } // class ExtremumBreakdownStrategy
}
