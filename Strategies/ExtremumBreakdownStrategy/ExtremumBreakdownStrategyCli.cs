﻿namespace StrategyPlugin.ExtremumBreakdownStrategy {
    using System;
    using Robot.Common.Cli;
    using Robot.Strategy;
    using Robot.Strategy.Cli;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    /// <summary>
    /// Стратегия, содержащая методы cli
    /// </summary>
    partial class ExtremumBreakdownStrategy : ICliStrategy {

        public bool DoCliCommand(CommandDescription command) 
        {
            return Helper.InvokeCommand(this, command);
        }

        public void LongCommand(string [] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в long");
            this.ManualLongSignal = true;
            tryEnterMarket();
        }  // LongCommand
        public void ShortCommand(string[] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного входа в short");
            this.ManualShortSignal = true;
            tryEnterMarket();
        }  // ShortCommand
        public void manualWorkDayEnd() {
            StrategyPluginBase.manualWorkDayEnd = true;
        }  // manualWorkDayEnd
        
        public void invokeStop() {
            action_stopOrderActivated(new Order());
        }
        public void ClosePositionCommand(string[] args) {
            LoggingHelper.AddInfoLog(this, "Сигнал ручного выхода из позиции");
            StrategyMarketState state = marketState();
            if ((state & StrategyMarketState.InMarket) == 0) {
                throw new Exception(String.Format("Стратегия {0} не в позиции", base.Name));
            }
            exitMarket();
        }  // ClosePositionCommand
        public void SetBaseCandleTime(string datetime) {


        //    удалить этот метод не даёт возникающая ошибка, поэтому оставляю пустым
        }  // SetBaseCandleTime
        public void ReRegisterStop(string[] args) {
            if (args.Length == 0) {
                throw new ArgumentNullException("Не задана стоп цена");
            }
            decimal stopprice = decimal.Parse(args[0]);
            LoggingHelper.AddInfoLog(this,
                "Ручная перерегистрация стопа и стоп-уровня {0} на значение: {1}", stop_loss_level_, stopprice);
            stop_loss_level_ = stopprice;
            reRegisterStop();
        }  // ReRegisterStop
    }  // class ExtremumBreakdownStrategy
}
