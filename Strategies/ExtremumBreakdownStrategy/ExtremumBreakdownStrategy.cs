﻿namespace StrategyPlugin.ExtremumBreakdownStrategy {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Ecng.ComponentModel;
    using global::Robot.Common.Equity;
    using global::Robot.Common.Exchange.Rts;
    using global::Robot.Configuration;
    using global::Robot.Sms;
    using global::Robot.Storage;
    using global::Robot.Strategy;
    using global::Robot.Strategy.Helper;
    using StockSharp.Algo.Candles;
    using StockSharp.Algo.Strategies;
    using StockSharp.BusinessEntities;
    using StockSharp.Logging;
    
    public partial class ExtremumBreakdownStrategy : Strategy, IStrategyPlugin, ISecurityDownload {
        /// <summary>
        /// Конструктор
        /// </summary>
        public event Action<SmsMessage>   Sms;

        public ExtremumBreakdownStrategy() {
            basePlugin = new StrategyPluginBase(this);
            _aliveStatus = new ExtremumBreakdownStrategyAliveStatus();
            CancelOrdersWhenStopping = false;
        }

        public CandleManager CandleManager {
            get {
                return this.basePlugin.CandleManager;
            }
            set {
                this.basePlugin.CandleManager = value;
            }
        }

        public bool ManualLongSignal { get; set; }
        public bool ManualShortSignal { get; set; }
        
        public void RegisterAsLogSource(ConsoleLogListener console) {
            basePlugin.RegisterAsLogSource(console);
        } 
        public void UnregisterAsLogSource() {
            basePlugin.UnregisterAsLogSource();
        }

        public IEnumerable<DownloadSecurity> DownloadSecurityList(System.Configuration.Configuration config) {
            var cw = new StrategyConfigWrapper<StrategyPlugin.ExtremumBreakdownStrategy.Parameters>(config);

            IList<DownloadSecurity> result = new List<DownloadSecurity>();

            result.Add(new DownloadSecurity() {
                Code = cw.P().@base.MasterSecurity,
                TimeFrame = TimeSpan.FromMinutes(cw.P().@base.TimeFrame),
                DaysToDownload = 5
            });

            result.Add(new DownloadSecurity() {
                Code = cw.P().@base.TradeSecurity,
                TimeFrame = TimeSpan.FromMinutes(cw.P().@base.TimeFrame),
                DaysToDownload = 5
            });

            return result.AsEnumerable();
        }  // DownloadSecurityList
     
        public void Save() {
            LoggingHelper.AddInfoLog(this, "Сохранение параметров стратегии");
            saveInternalSettingsStorage();
        }
        public void Load(System.Configuration.Configuration config) {
            LoggingHelper.AddInfoLog(this, "Установка параметров стратегии {0}...", Name);
            _cw = new StrategyConfigWrapper<StrategyPlugin.ExtremumBreakdownStrategy.Parameters>(config);

            base.Security = StrategyPluginHelper.LoadSecurity(base.Trader,
                _cw.P().@base.TradeSecurity);
            base.Portfolio = StrategyPluginHelper.LoadPortfolio(base.Trader,
                _cw.P().@base.Portfolio);

            LoggingHelper.AddDebugLog(this, String.Format("Торгуемый инструмент {0}", base.Security));

            string masterSecCode = _cw.P().@base.MasterSecurity;
            LoggingHelper.AddDebugLog(this, "Ведущий инструмент - {0}", masterSecCode);
            _masterSecurity = StrategyPluginHelper.LoadSecurity(base.Trader, masterSecCode);
            if (_masterSecurity == null) {
                throw new Exception(String.Format("Не найден ведущий инструмен: {0}", masterSecCode));
            }

            changeSecurityExchange();

            // при переподключении меняем биржу инструмента
            base.Trader.Connected += new Action(changeSecurityExchange);
            
            _timeFrame = TimeSpan.FromMinutes(_cw.P().@base.TimeFrame);
            _aliveStatus.TimeFrame = _timeFrame;
            // проверим что ещё не зарегистрированы свечи для торгуемого инструмента 
            _tradeCandleSeries = StrategyPluginHelper.CheckAndRegisterSeries(
                this.CandleManager, base.Security, _timeFrame);
            _masterCandleSeries = StrategyPluginHelper.CheckAndRegisterSeries(
                this.CandleManager, _masterSecurity, _timeFrame);
            
            /// создаем менеджер объёма
            volume_manager_ = VolumeFactory.create(base.Portfolio, base.Security,
                this, _cw.P().@base.VolumeUnit, _cw.P().@base.Volume);
            loadInternalConfiguration();
 
        }  // Load


        IEnumerable<TimeFrameCandle> checkHistoryCandle(string secCode, IList<TimeFrameCandle> candles) {
            int candleCount = candles.Count();
            if (candleCount < NumBars) {
                throw new Exception(String.Format(
                    "Не хватает исторических свечей {0} для работы стратегии: необходимо {1} загружено {2}",
                    secCode, NumBars, candleCount));
            }
            return candles;
        }

        IEnumerable<TimeFrameCandle> loadCandleFromStorage(Security security, DateTime baseDate) {
            DateTime dateFrom = baseDate + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime dateTo = baseDate + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Load candles from date range [{0};{1}] for {2}",
                dateFrom, dateTo, security.Code);
            // загружаем свечи предыдущего торгового дня
            return global::Robot.Storage.Storage.Instance.
                LoadTimeFrameCandles(security, _timeFrame, dateFrom, dateTo);
        }

        public void loadInternalConfiguration() {
            LoggingHelper.AddInfoLog(this,
                "Загрузка внутреней конфигурации стратегии...");

            DateTime baseDate = DateTime.Now.Date;
            for (int i = 0; i < 5; ++i) {
                baseDate = WorkingCalendar.previousTradingDate(baseDate);
                // Грузим свечи торгуемого инструмента    
                //loadCandles(_tradeCandleSeries, base.Security, baseDate);
                foreach (TimeFrameCandle c in loadCandleFromStorage(base.Security, baseDate)) {
                    _tradeHistoryCandles.Add(c);
                }
                //грузим свечи второго (ведущего) инструмента
                //loadCandles(_masterCandleSeries, _masterSecurity, baseDate);
                foreach (TimeFrameCandle c in loadCandleFromStorage(_masterSecurity, baseDate)) {
                    _masterHistoryCandles.Add(c);
                }
            }
            checkHistoryCandle(base.Security.Code, _tradeHistoryCandles);
            checkHistoryCandle(_masterSecurity.Code, _masterHistoryCandles);

        }  // loadInternalConfiguration
        public void saveInternalSettingsStorage() {

            DateTime begin = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeBegin;
            DateTime end = DateTime.Now.Date + _cw.P().@base.SaveCandleTimeRangeEnd;

            LoggingHelper.AddDebugLog(this, "Save candle range {0}, {1}",
                begin, end);

            Range<DateTime> range = new Range<DateTime>(begin, end);
            IEnumerable<TimeFrameCandle> candles =
                _tradeCandleSeries.GetCandles<TimeFrameCandle>(range);

            global::Robot.Storage.Storage.Instance.SaveCandles(base.Security,
                candles, _timeFrame, CandleStorageType.Min);
            
            candles = _masterCandleSeries.GetCandles<TimeFrameCandle>(range);
            global::Robot.Storage.Storage.Instance.SaveCandles(_masterSecurity,
                candles, _timeFrame, CandleStorageType.Min);
            
        }  // saveInternalSettingsStorage

         protected override void OnStopOrderFailed(OrderFail fail) {
            base.OnStopOrderFailed(fail);
        }
        public bool WorkDayEnd() {
            return StrategyPluginBase.workDayEnd(_cw.P().@base.EndStrategyDay);
        }  // workDayEnd

        public StrategyStateInfo State() {
            StrategyStateInfo info = new StrategyStateInfo() {
                Name = base.Name,
                Portfolio = base.Portfolio.Name,
                SecurityCode = base.Security.Code,
                MarketState = marketState(),
                ProcessState = base.ProcessState,
                Volume = base.Volume,
                Extended = new Dictionary<string, string>() {
                    {"Ведущий инструмент", _masterSecurity.Code}
                }
            };

            info.Extended = new Dictionary<string, string>() {
                {"Уровень входа в long", HighEnterSecurity1.ToString()},
                {"Уровень входа в short", LowEnterSecurity1.ToString()}
            };

            info.AliveStatus = _aliveStatus;

            Order o = findActiveStop();
            if (o != null) {
                info.StopVolume = o.Volume;
            }
            decimal? vol = positionVolume();
            info.Volume = vol == null ? 0 : (decimal)vol;
            return info;
 
        }  // State

        decimal? positionVolume() {
            Position pos = base.Trader.GetPosition(base.Portfolio, base.Security);
            if (pos == null) {
                return null;
            }
            LoggingHelper.AddDebugLog(this,
                "positionVolume::Текущае значении объёма позиции позиции: {0}", pos.CurrentValue);
            return pos.CurrentValue;
        }

        /// <summary>
        /// Загрузка ордеров стратегии
        /// </summary>
        /// <param name="neworders">Список ордеров</param>
        /// <param name="isstoporders">Флаг признак стопов</param>
        /// <returns></returns>
        protected override IEnumerable<Order> ProcessNewOrders(
            IEnumerable<Order> neworders, bool isstoporders) {
 
            if (_stopOrdersLoaded && _ordersLoaded) {
                return base.ProcessNewOrders(neworders, isstoporders);
            }
            
            LoggingHelper.AddDebugLog(this, "Обработка {0}ордеров", isstoporders ? "стоп-" : "");
            List<Order> result = new List<Order>();
            foreach (Order o in neworders) {
                
                string comment = o.Comment;
                if (comment.EndsWith("/")) {
                    comment = comment.TrimEnd('/');
                }
                if (comment != base.Name) continue;
                if (isstoporders && o.State == OrderStates.Active) {
                    LoggingHelper.AddDebugLog(this,
                    "Обработка стоп ордера: {0}, {1}, {2}",
                    o.Comment, o.Id, o.TransactionId);
                    result.Add(o);

                    if (o.Direction == OrderDirections.Buy) {
                        stop_loss_level_ = o.Price - _cw.P().@base.StopPriceDelta;
                        LoggingHelper.AddInfoLog(this, "Найден стоп на покупку {0} контрактов при касании цены {1}", o.Volume, stop_loss_level_);
                        LoggingHelper.AddInfoLog(this, "Задаём stop_loss_level_ = {0}", stop_loss_level_);
                    }

                    if (o.Direction == OrderDirections.Sell) {
                        stop_loss_level_ = o.Price + _cw.P().@base.StopPriceDelta;
                        LoggingHelper.AddInfoLog(this, "Найден стоп на продажу {0} контрактов при касании цены {1}", o.Volume, stop_loss_level_);
                        LoggingHelper.AddInfoLog(this, "Задаём stop_loss_level_ = {0}", stop_loss_level_);
                    }

                    subscribeStopOrderRules(o);
                    continue;
                }
                if (!isstoporders) {
                    LoggingHelper.AddDebugLog(this,
                    "Обработка ордера: {0}, {1}, {2}",
                    o.Comment, o.Id, o.TransactionId);
                    result.Add(o);
                    continue;
                }
            }

            if (isstoporders) _stopOrdersLoaded = true;
            if (!isstoporders) _ordersLoaded = true;

            IEnumerable<Order> myorders = result.AsEnumerable<Order>();
            return myorders.Concat<Order>(base.ProcessNewOrders(neworders, isstoporders));

        }  // ProcessNewOrders
        
    }
}