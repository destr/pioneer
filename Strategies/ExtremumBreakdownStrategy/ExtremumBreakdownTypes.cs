﻿using System;
namespace StrategyPlugin {
    namespace ExtremumBreakdownStrategy {
    /// <summary>
    /// Параметры стратегии
    /// </summary>
    public sealed class Parameters {
        /// <summary>
        /// Кол-во баров которое смотрим для входа
        /// </summary>
        public int NumBarsForEnter { get; set; }
        /// <summary>
        /// Кол-во баров которое смотрим для первого индикатора выхода
        /// </summary>
        public int NumBarsForExit1 { get; set; }
        /// <summary>
        /// Кол-во баров которое смотрим для второго индикатора выхода
        /// </summary>
        public int NumBarsForExit2 { get; set; }
        /// <summary>
        /// Обратная корреляция (для сочетания Si-SP500, например)
        /// </summary>
        public bool InverseCorrelation { get; set; }
        /// <summary>
        /// Защитный отступ в пунктах при выставлении ордера, чтобы он гарантированно исполнился
        /// </summary>
        public decimal stoppricedelta { get; set; }
        /// <summary>
        /// Время завершения стратегии: закрытие позиций
        /// </summary>
        public TimeSpan exittime { get; set; }
        /// <summary>
        /// Время с которого стратегия пытается входить
        /// </summary>
        public TimeSpan startentertime { get; set; }
        /// <summary>
        /// Время до которого стратегия ищет сигналы входа
        /// </summary>
        public TimeSpan endentertime { get; set; }
        /// <summary>
        /// Время конца дня для стратегии. При достижении этого времени
        /// статегия считается успешно завершённо. Если при завершении
        /// стратегии это время не достигнуто, то считается что стратегия
        /// ещё не завершена, поэтому не сохраняются отчёты и уровни, не
        /// обнуляется счётчик входов
        /// </summary>
        public TimeSpan endstrategyday { get; set; }
        /// <summary>
        /// Начало диапазона сохранения свеч предыдущего дня
        /// </summary>
        public TimeSpan savecandletimerangebegin { get; set; }
        /// <summary>
        /// Окончание диапазона сохранения свеч предыдущего дня
        /// </summary>
        public TimeSpan savecandletimerangeend { get; set; }
        public override string ToString() {
            return String.Format(
                "InverseCorrelation: {0}, NumBarsForEnter: {1}, NumBarsForExit1: {2}, NumBarsForExit2: {3}, startentertime: {4}, endentertime: {5}, exittime: {6}, savecandletimerangebegin: {7}, savecandletimerangeend: {8}",
              //  levelfactor, stoplosspercent, stoplossmul, maxentertomarket,
              //  exittime, startentertime, endentertime, savecandletimerangebegin, 
              //  savecandletimerangeend, DaysKeepPosition
              InverseCorrelation, NumBarsForEnter, NumBarsForExit1, NumBarsForExit2,
              startentertime, endentertime, exittime, savecandletimerangebegin, savecandletimerangeend);
        }
    };
    /// <summary>
    /// Состояние стратегии относительно рынка
    /// </summary>
    [Flags]
    public enum StrategyMarketState {
        /// <summary>
        /// Не в позиции
        /// </summary>
        Free = 0,
        /// <summary>
        /// Длинная позиция
        /// </summary>
        Long = 0x1,
        /// <summary>
        /// Короткая позиция
        /// </summary>
        Short = 0x2,
        /// <summary>
        /// В позиции
        /// </summary>
        InMarket = Long | Short,
        /// <summary>
        /// Неизвестная позиция. Данных нет
        /// </summary>
        Unknown = 0x4,
        /// <summary>
        /// Сохраняем позиции
        /// </summary>
        KeepPosition = 0x10
    }

}  // namespace BreakVolatilityStrategy
}  // namespace StrategyPlugin